### 版权许可证

#### 1. 使用JDK自带的 keytool 工具生成公私钥证书库

```
 # 生成命令
 keytool -genkeypair -keyalg DSA -keysize 2048 -validity 3650 -alias "privateKey" -keystore "privateKeys.keystore" -storepass "aster.vip@2025" -keypass "aster.vip@2025" -dname "CN=localhost, OU=localhost, O=aster, L=BEIJING, ST=BEIJING, C=CN"
 
 # 导出命令
 keytool -exportcert -alias "privateKey" -keystore "privateKeys.keystore" -storepass "aster.vip@2025" -file "certfile.cer"
 
 # 导入命令
 keytool -import -alias "publicCert" -file "certfile.cer" -keystore "publicCerts.keystore" -storepass "aster.vip@2025"
```
执行上述命令后，会生成3个文件，其中certfile.cer使用不到可以删除。
- privateKeys.keystore用来为客户生成License文件，不可泄漏。
- publicCerts.keystore需要提供给客户，用其解密License文件并校验其许可信息。

命令解释：
- -genkeypair	生成一对非对称密钥
- -keysize	密钥长度
- -validity	有效期，单位天，一般将有效期设置的很长，这样公钥私钥不用更换，再通过证书来控制应用有效期。
- -alias	私钥别名
- -keystore	私钥文件名
- -storepass	访问整个密码库的密码
- -keypass	别名对应私钥的密码
- -dname	证书持有者详细信息
- CN=localhost：CN是Common Name的缩写，通常用于指定域名或IP地址
- OU=localhost：OU是Organizational Unit的缩写，表示组织单位
- O=localhost：O是Organization的缩写，表示组织名
- L=SH：L是Locality的缩写，表示城市或地区
- ST=SH：ST是State的缩写，表示州或省
- C=CN：C是Country的缩写，表示国家代码

#### 2. 配置文件

配置文件中有以下代码，可根据实际业务更改。
```
license:
  # 证书subject
  subject: aster-license
  # 公钥别称
  publicAlias: publicCert
  # 访问公钥库的密码
  storePass: aster.vip@2025
  # 证书路径 linux: /data/license/license.lic
  licensePath: D://license//license.lic
  # 公钥路径 linux: /data/license/publicCerts.keystore
  publicKeysStorePath: D://license//publicCerts.keystore
```

#### 3. 生成license证书

生成license证书前的准备：
- 获取客户的服务器硬件信息：IP地址、Mac地址、CPU序列号、主板序列号。<br />
    也可以将应用部署到客户服务器上，通过访问http://localhost:8080/aster-boot/license/serverInfo接口获取。
- 在aster-system模块下的pom.xml中添加依赖aster-module-license
- 运行项目，访问http://localhost:8080/aster-boot/license/generateLicense

generateLicense接口的参数：
```json
{
	"subject": "aster-license",
	"storePass": "aster.vip@2025",
	"keyPass": "aster.vip@2025",
	"privateAlias": "privateKey",
	"privateKeysStorePath": "D://license//privateKeys.keystore",
	"consumerType": "user",
	"consumerAmount": 1,
	"issuedTime": "2025-01-01 00:00:00",
	"expiryTime": "2025-01-02 00:00:00",
	"licensePath": "D://license//license.lic",
	"licenseCheckModel": {
		"ipAddress": [
			"10.11.14.121"
		],
		"macAddress": [
			"80-38-FB-1A-8C-68"
		],
		"cpuSerial": "BFEBFBFF000806C1",
		"mainBoardSerial": "49I0PM21B8005456"
	},
	"description": ""
}
```

通过以上步骤可以生成license.lic文件，该文件是加密后的license证书。

#### 4. 使用

- 给客户打包时，需在ResourceConfiguration中开启证书校验拦截器，否则不生效
- publicCerts.keystore和license.lic需提供给客户，放置到固定位置
- 私钥证书库不能泄露给客户，否则客户可以解密license证书，导致授权许可失效；
- 
