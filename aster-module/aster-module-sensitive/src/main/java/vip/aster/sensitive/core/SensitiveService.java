package vip.aster.sensitive.core;

/**
 * 脱敏服务
 * 默认管理员不过滤
 * 需自行根据业务重写实现
 *
 * @author Aster
 * @since 2024/2/7 18:12
 */
public interface SensitiveService {

    /**
     * 是否脱敏
     * @param roleKey 角色
     * @param perms 权限
     * @return true-脱敏 false-不脱敏
     */
    boolean isSensitive(String roleKey, String perms);
}
