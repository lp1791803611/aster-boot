package ${package}.${moduleName}.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ${package}.common.utils.PageInfo;
import ${package}.${moduleName}.entity.${ClassName};
import ${package}.${moduleName}.mapper.${ClassName}Mapper;
import ${package}.${moduleName}.query.${ClassName}Query;
import ${package}.${moduleName}.service.${ClassName}Service;
import ${package}.${moduleName}.vo.${ClassName}VO;

/**
 * <P>
 * ${tableComment} 服务实现类
 * </P>
 *
 * @author ${author} ${email}
 * @since ${datetime}
 */
@Service
@AllArgsConstructor
public class ${ClassName}ServiceImpl extends ServiceImpl<${ClassName}Mapper, ${ClassName}> implements ${ClassName}Service {
    private ${ClassName}Mapper ${className}Mapper;

    @Override
    public PageInfo<${ClassName}VO> pageList(${ClassName}Query query) {
        Page<${ClassName}> page = new Page<>(query.getPageNum(), query.getPageSize());
        Page<${ClassName}> pageList = ${className}Mapper.selectPage(page, getWrapper(query));
        return new PageInfo<>(${ClassName}VO.convertList(pageList.getRecords()), pageList.getTotal());
    }

    @Override
    public void save(${ClassName}VO vo) {
        ${ClassName} entity = vo.reconvert();

        if (StrUtil.isNotBlank(entity.getId())) {
            ${className}Mapper.updateById(entity);
        } else {
            ${className}Mapper.insert(entity);
        }
    }

    private LambdaQueryWrapper<${ClassName}> getWrapper(${ClassName}Query query){
        LambdaQueryWrapper<${ClassName}> wrapper = Wrappers.lambdaQuery();
        <#list queryList as field>
            <#if field.queryFormType == 'date' || field.queryFormType == 'datetime'>
        wrapper.between(ArrayUtil.isNotEmpty(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, ArrayUtils.isNotEmpty(query.get${field.attrName?cap_first}()) ? query.get${field.attrName?cap_first}()[0] : null, ArrayUtils.isNotEmpty(query.get${field.attrName?cap_first}()) ? query.get${field.attrName?cap_first}()[1] : null);
            <#elseif field.queryType == '='>
        wrapper.eq(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == '!='>
        wrapper.ne(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == '>'>
        wrapper.gt(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == '>='>
        wrapper.ge(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == '<'>
        wrapper.lt(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == '<='>
        wrapper.le(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == 'like'>
        wrapper.like(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == 'left like'>
        wrapper.likeLeft(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            <#elseif field.queryType == 'right like'>
        wrapper.likeRight(StrUtil.isNotBlank(query.get${field.attrName?cap_first}()), ${ClassName}::get${field.attrName?cap_first}, query.get${field.attrName?cap_first}());
            </#if>
        </#list>
        return wrapper;
    }

}