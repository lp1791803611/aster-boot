package ${package}.${moduleName}.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ${package}.common.utils.PageInfo;
import ${package}.common.utils.ResultInfo;
import ${package}.framework.log.annotation.Log;
import ${package}.framework.log.enums.BusinessTypeEnum;
import ${package}.${moduleName}.entity.${ClassName};
import ${package}.${moduleName}.query.${ClassName}Query;
import ${package}.${moduleName}.service.${ClassName}Service;
import ${package}.${moduleName}.vo.${ClassName}VO;

import java.util.List;

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${datetime}
 */
@Tag(name = "${tableComment}")
@RestController
@RequestMapping("/${moduleSimple}/${functionName}")
@AllArgsConstructor
public class ${ClassName}Controller {
    private final ${ClassName}Service ${className}Service;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('${moduleSimple}:${functionName}:list')")
    public ResultInfo<PageInfo<${ClassName}VO>> page(@ParameterObject @Valid ${ClassName}Query query){
        PageInfo<${ClassName}VO> page = ${className}Service.pageList(query);

        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情") 
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('${moduleSimple}:${functionName}:info')")
    public ResultInfo<${ClassName}VO> get(@PathVariable("id") String id){
        ${ClassName} entity = ${className}Service.getById(id);

        return ResultInfo.success(new ${ClassName}VO(entity));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('${moduleSimple}:${functionName}:add','${moduleSimple}:${functionName}:edit')")
    public ResultInfo<String> save(@RequestBody ${ClassName}VO vo){
        ${className}Service.save(vo);

        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('${moduleSimple}:${functionName}:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList){
        ${className}Service.removeBatchByIds(idList);

        return ResultInfo.success();
    }
}