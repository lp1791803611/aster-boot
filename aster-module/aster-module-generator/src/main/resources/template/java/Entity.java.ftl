package ${package}.${moduleName}.entity;

import com.baomidou.mybatisplus.annotation.*;
<#if hasDate>
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
</#if>
import lombok.Data;
import lombok.EqualsAndHashCode;
<#list importList as i>
import ${i!};
</#list>

import java.io.Serial;

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${datetime}
 */
<#if baseClass??>@EqualsAndHashCode(callSuper=false)</#if>
@Data
@TableName("${tableName}")
public class ${ClassName}<#if baseClass??> extends ${baseClass.code}<${ClassName}></#if> {

	@Serial
	private static final long serialVersionUID = 1L;

<#list fieldList as field>
<#if baseClass??>
	<#if !field.baseField>
		<#if field.fieldComment!?length gt 0>
	/**
	 * ${field.fieldComment}
	 */
		</#if>
		<#if field.autoFill == "INSERT">
	@TableField(value="${field.fieldName}", fill = FieldFill.INSERT)
		</#if>
		<#if field.autoFill == "INSERT_UPDATE">
	@TableField(value="${field.fieldName}", fill = FieldFill.INSERT_UPDATE)
		</#if>
		<#if field.autoFill == "UPDATE">
	@TableField(value="${field.fieldName}", fill = FieldFill.UPDATE)
		</#if>
		<#if field.primaryPk>
	@TableId("${field.fieldName}")
		<#else>
	@TableField("${field.fieldName}")
		</#if>
		<#if field.attrType == "LocalDateTime">
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
		</#if>
	private ${field.attrType} ${field.attrName};
	</#if>
<#else>
	<#if field.fieldComment!?length gt 0>
	/**
	* ${field.fieldComment}
	*/
	</#if>
	<#if field.primaryPk>
	@TableId("${field.fieldName}")
	<#elseif field.autoFill == "INSERT">
	@TableField(value="${field.fieldName}", fill = FieldFill.INSERT)
	<#elseif field.autoFill == "INSERT_UPDATE">
	@TableField(value="${field.fieldName}", fill = FieldFill.INSERT_UPDATE)
	<#elseif field.autoFill == "UPDATE">
	@TableField(value="${field.fieldName}", fill = FieldFill.UPDATE)
	<#else>
	@TableField("${field.fieldName}")
	</#if>
	<#if field.attrType == "LocalDateTime">
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	</#if>
	<#if field.fieldName == "is_deleted">
	@TableLogic
	</#if>
	private ${field.attrType} ${field.attrName};
</#if>
</#list>
}