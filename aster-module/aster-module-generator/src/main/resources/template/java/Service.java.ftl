package ${package}.${moduleName}.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ${package}.common.utils.PageInfo;
import ${package}.${moduleName}.entity.${ClassName};
import ${package}.${moduleName}.query.${ClassName}Query;
import ${package}.${moduleName}.vo.${ClassName}VO;

/**
 * <P>
 * ${tableComment} 服务接口
 * </P>
 *
 * @author ${author} ${email}
 * @since ${datetime}
 */
public interface ${ClassName}Service extends IService<${ClassName}> {
    /**
     * 分页
     * @param query 查询条件
     * @return 查询结果
     */
    PageInfo<${ClassName}VO> pageList(${ClassName}Query query);

    /**
     * 保存
     * @param vo ${tableComment}
     */
    void save(${ClassName}VO vo);
}