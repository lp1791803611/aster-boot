package ${package}.${moduleName}.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ${package}.${moduleName}.entity.${ClassName};

/**
 * ${tableComment} Mapper 接口
 *
 * @author ${author} ${email}
 * @since ${datetime}
 */
public interface ${ClassName}Mapper extends BaseMapper<${ClassName}> {
	
}