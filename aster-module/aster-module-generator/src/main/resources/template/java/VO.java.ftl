package ${package}.${moduleName}.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ${package}.${moduleName}.entity.${ClassName};

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${datetime}
 */
@Data
@Schema(description = "${tableComment}")
public class ${ClassName}VO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

<#list fieldList as field>
	<#if field.fieldComment!?length gt 0>
	@Schema(description = "${field.fieldComment}")
	</#if>
	private ${field.attrType} ${field.attrName};
</#list>

	public ${ClassName}VO() {
		super();
	}

	public ${ClassName}VO(${ClassName} entity) {
	<#list fieldList as field>
		this.${field.attrName} = entity.get${field.attrName?cap_first}();
	</#list>
	}

	public static List<${ClassName}VO> convertList(List<${ClassName}> source) {
		if (CollUtil.isEmpty(source)) {
			return ListUtil.empty();
		}
		List<${ClassName}VO> list = new ArrayList<>();
		for (${ClassName} entity : source) {
			list.add(new ${ClassName}VO(entity));
		}
		return list;
	}

	public ${ClassName} reconvert() {
		${ClassName} entity = new ${ClassName}();
		<#list fieldList as field>
		entity.set${field.attrName?cap_first}(this.${field.attrName});
		</#list>
		return entity;
	}

}