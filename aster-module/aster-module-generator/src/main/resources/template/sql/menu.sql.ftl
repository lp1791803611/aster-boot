<#assign dbTime = "now()">
<#if dbType=="SQLServer">
    <#assign dbTime = "getDate()">
</#if>
<#if dbType=="Oracle">
    <#assign dbTime = "sysdate">
</#if>

-- 初始化菜单
INSERT INTO sys_menu(id, pid, name, en_name, menu_path, component, perms, menu_type, open_style, icon, is_keep_alive, is_affix, is_full, is_hide, sort, version, create_time, create_by, update_time, update_by, status, is_deleted, remark)
VALUES ('${menuId}', '0', '${tableComment!}', '${functionName}', '/${moduleName}/${functionName}', '/${moduleName}/${functionName}/index', NULL, '1', 0, 'iconfont icon-shezhi', '1', '1', '1', '1', 1, 0, ${dbTime}, NULL, ${dbTime}, NULL, '0', '0', NULL);

INSERT INTO sys_menu(id, pid, name, en_name, menu_path, component, perms, menu_type, open_style, icon, is_keep_alive, is_affix, is_full, is_hide, sort, version, create_time, create_by, update_time, update_by, status, is_deleted, remark)
VALUES ('${searchId}', '${menuId}', '查询', NULL, NULL, NULL, '${moduleSimple}:${functionName}:list', '2', 0, NULL, '1', '1', '1', '1', 1, 0, ${dbTime}, NULL, ${dbTime}, NULL, '0', '0', NULL);

INSERT INTO sys_menu(id, pid, name, en_name, menu_path, component, perms, menu_type, open_style, icon, is_keep_alive, is_affix, is_full, is_hide, sort, version, create_time, create_by, update_time, update_by, status, is_deleted, remark)
VALUES ('${infoId}', '${menuId}', '详情', NULL, NULL, NULL, '${moduleSimple}:${functionName}:info', '2', 0, NULL, '1', '1', '1', '1', 2, 0, ${dbTime}, NULL, ${dbTime}, NULL, '0', '0', NULL);

INSERT INTO sys_menu(id, pid, name, en_name, menu_path, component, perms, menu_type, open_style, icon, is_keep_alive, is_affix, is_full, is_hide, sort, version, create_time, create_by, update_time, update_by, status, is_deleted, remark)
VALUES ('${addId}', '${menuId}', '新增', NULL, NULL, NULL, '${moduleSimple}:${functionName}:add', '2', 0, NULL, '1', '1', '1', '1', 3, 0, ${dbTime}, NULL, ${dbTime}, NULL, '0', '0', NULL);

INSERT INTO sys_menu(id, pid, name, en_name, menu_path, component, perms, menu_type, open_style, icon, is_keep_alive, is_affix, is_full, is_hide, sort, version, create_time, create_by, update_time, update_by, status, is_deleted, remark)
VALUES ('${editId}', '${menuId}', '编辑', NULL, NULL, NULL, '${moduleSimple}:${functionName}:edit', '2', 0, NULL, '1', '1', '1', '1', 4, 0, ${dbTime}, NULL, ${dbTime}, NULL, '0', '0', NULL);

INSERT INTO sys_menu(id, pid, name, en_name, menu_path, component, perms, menu_type, open_style, icon, is_keep_alive, is_affix, is_full, is_hide, sort, version, create_time, create_by, update_time, update_by, status, is_deleted, remark)
VALUES ('${deleteId}', '${menuId}', '删除', NULL, NULL, NULL, '${moduleSimple}:${functionName}:delete', '2', 0, NULL, '1', '1', '1', '1', 5, 0, ${dbTime}, NULL, ${dbTime}, NULL, '0', '0', NULL);
