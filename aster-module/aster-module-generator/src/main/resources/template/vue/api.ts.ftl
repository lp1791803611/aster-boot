import request from '@/config/axios';
import { PORT1 } from '../config';

/**
 * @description: 分页
 * @return {*}
 */
export const ${functionName}PageApi = (params: ${FunctionName}.${FunctionName}Params) => {
  return request.get<Page.ResPage<${FunctionName}.${FunctionName}Info>>(PORT1 + `/${functionName}/page`, params, {
	noLoading: true,
  });
};

/**
 * @description: 单条信息
 * @return {*}
 */
export const ${functionName}InfoApi = (id: string) => {
  return request.get<${FunctionName}.${FunctionName}Info>(PORT1 + `/${functionName}/info/` + id, { noLoading: true });
};

/**
 * @description: 保存
 * @param {${FunctionName}.${FunctionName}Info} info
 * @return {*}
 */
export const ${functionName}SaveApi = (info: ${FunctionName}.${FunctionName}Info) => {
  return request.post<string>(PORT1 + `/${functionName}/save`, info, { noLoading: true });
};

/**
 * @description: 删除
 * @param {any} ids
 * @return {*}
 */
export const ${functionName}DeleteApi = (ids: any) => {
  return request.post<string>(PORT1 + `/${functionName}/delete`, ids, { noLoading: true });
};