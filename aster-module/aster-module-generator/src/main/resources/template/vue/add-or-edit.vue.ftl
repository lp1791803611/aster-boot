<template>
  <el-dialog
	v-model="visible"
	:title="title"
	:close-on-click-modal="false"
	:lock-scroll="false"
	width="60%"
	draggable
  >
	<el-form
	  ref="formDataRef"
	  :model="formData"
	  :rules="formRules"
	  label-width="100px"
	  @keyup.enter="submit()"
	>
	  <el-row>
			<#list formList as field>
		<el-col :span="<#if formLayout == 1>24<#else>12</#if>">
		  <el-form-item prop="${field.attrName}" label="${field.fieldComment!}">
						<#if field.formType == 'text'>
			<el-input v-model="formData.${field.attrName}" placeholder="${field.fieldComment!}" />
						<#elseif field.formType == 'textarea'>
			<el-input type="textarea" v-model="formData.${field.attrName}" />
						<#elseif field.formType == 'editor'>
			<el-input type="textarea" v-model="formData.${field.attrName}" />
						<#elseif field.formType == 'select'>
							<#if field.formDict??>
			<dict-select
			  v-model="formData.${field.attrName}"
			  dict-type="${field.formDict}"
			  clearable
			  placeholder="${field.fieldComment!}"
			/>
							<#else>
			<el-select v-model="formData.${field.attrName}" placeholder="请选择">
			  <el-option label="请选择" value="0"></el-option>
			</el-select>
							</#if>
						<#elseif field.formType == 'radio'>
							<#if field.formDict??>
			<dict-radio v-model="formData.${field.attrName}" dict-type="${field.formDict}" />
							<#else>
			<el-radio-group v-model="formData.${field.attrName}">
			  <el-radio :label="0">启用</el-radio>
			  <el-radio :label="1">禁用</el-radio>
			</el-radio-group>
							</#if>
						<#elseif field.formType == 'checkbox'>
							<#if field.formDict??>
			<dict-checkbox v-model="formData.${field.attrName}" dict-type="${field.formDict}" />
							<#else>
			<el-checkbox-group v-model="formData.${field.attrName}">
				<el-checkbox label="启用" name="type"></el-checkbox>
				<el-checkbox label="禁用" name="type"></el-checkbox>
			</el-checkbox-group>
							</#if>
						<#elseif field.formType == 'date'>
			<el-date-picker type="date" placeholder="${field.fieldComment!}" v-model="formData.${field.attrName}" />
						<#elseif field.formType == 'datetime'>
			<el-date-picker type="datetime" placeholder="${field.fieldComment!}" v-model="formData.${field.attrName}" />
						<#else>
			<el-input v-model="formData.${field.attrName}" placeholder="${field.fieldComment!}" />
						</#if>
		  </el-form-item>
		</el-col>
			</#list>
	  </el-row>
	</el-form>
	<template #footer>
	  <el-button type="primary" @click="submit">{{ $t('button.confirm') }}</el-button>
	  <el-button @click="visible = false">{{ $t('button.cancel') }}</el-button>
	</template>
  </el-dialog>
</template>
<script setup lang="ts">
  import { computed, reactive, ref } from 'vue';
  import { ${functionName}InfoApi, ${functionName}SaveApi } from '@/api/${moduleSimple}/${functionName}';
  import { ResultEnum } from '@/enums/httpEnum';
  import { ElMessage } from 'element-plus/es';
  import { isNotEmpty } from '@/utils';
  import DictRadio from '@/components/dict/dict-radio.vue';
  import { useI18n } from 'vue-i18n';

  const { t } = useI18n();

  const emits = defineEmits(['refresh']);

  /** 是否显示 */
  const visible = ref(false);
  /** 标题 */
  const title = computed(() => {
	return !formData.id ? t('button.add') : t('button.edit');
  });
  /** 注册组件 */
  const formDataRef = ref();
  /** 表单内容 */
  const formData = reactive<${FunctionName}.${FunctionName}Info>({
	id: '',
	<#list formList as field>
	${field.attrName}: ''<#sep>,
	</#list>
  });
  /** 表单规则 */
  const formRules = computed(() => {
	return {
		<#list formList as field>
		<#if field.formRequired>
	  ${field.attrName}: [{ required: true, message: t('common.required'), trigger: 'blur' }]<#sep>,
		</#if>
		</#list>
	};
  });

  /** 初始化 */
  const init = async (key?: string) => {
	visible.value = true;
	formData.id = '';
	// 重置表单
	if (formDataRef.value) {
	  formDataRef.value.resetFields();
	}
	// 判断新增还是编辑
	if (key && isNotEmpty(key)) {
	  getInfo(key);
    }
  };

  /**
   * @description: 获取信息
   * @param {*} key
   * @return {*}
   */
  const getInfo = (key: string) => {
	${functionName}InfoApi(key).then(({ data }) => {
	  Object.assign(formData, data);
	});
  };

  /**
   * @description: 保存
   * @return {*}
   */
  const submit = () => {
	formDataRef.value.validate((valid: boolean) => {
	  if (!valid) {
		return false;
	  }

	  ${functionName}SaveApi(formData).then((res) => {
		if (res.code == ResultEnum.SUCCESS) {
		  ElMessage.success({
			message: t('common.success'),
			duration: 500,
			onClose: () => {
			  visible.value = false;
			  emits('refresh');
			},
		  });
		}
	  });
	});
  };

  defineExpose({
	init,
  });
</script>
<style lang="scss" scoped></style>
