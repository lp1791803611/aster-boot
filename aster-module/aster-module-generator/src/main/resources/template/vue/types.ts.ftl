declare namespace ${FunctionName} {
  export interface ${FunctionName}Params extends Page.ReqPage {
<#list queryList as field>
  <#if field.attrType == "Integer" || field.attrType == "Long" || field.attrType == "Double">
    ${field.attrName}: number;
  <#else>
    ${field.attrName}: string;
  </#if>
</#list>
  }

  export interface ${FunctionName}Info {
<#list formList as field>
  <#if (field_index == 0 && field.fieldName != 'id')>
    id?: string;
  </#if>
  <#if field.attrType == "Integer" || field.attrType == "Long" || field.attrType == "Float" || field.attrType == "Double">
    ${field.attrName}: number;
  <#else>
    ${field.attrName}: string;
  </#if>
</#list>
  }
}