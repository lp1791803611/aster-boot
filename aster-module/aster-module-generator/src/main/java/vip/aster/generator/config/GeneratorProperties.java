package vip.aster.generator.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * GeneratorProperties
 *
 * @author Aster
 * @since 2024/2/26 16:05
 */
@Data
@ConfigurationProperties("generator")
public class GeneratorProperties {
    /**
     * 模板路径
     */
    private String template;
}
