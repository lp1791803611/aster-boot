package vip.aster.generator.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.common.utils.PageInfo;
import vip.aster.generator.entity.GenFieldType;
import vip.aster.generator.mapper.GenFieldTypeMapper;
import vip.aster.generator.query.Query;
import vip.aster.generator.service.GenFieldTypeService;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * GenFieldTypeServiceImpl
 *
 * @author Aster
 * @since 2024/2/26 10:41
 */
@Service
@AllArgsConstructor
public class GenFieldTypeServiceImpl extends ServiceImpl<GenFieldTypeMapper, GenFieldType> implements GenFieldTypeService {
    private final GenFieldTypeMapper genFieldTypeMapper;

    @Override
    public PageInfo<GenFieldType> page(Query query) {
        LambdaQueryWrapper<GenFieldType> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StrUtil.isNotBlank(query.getColumnType()), GenFieldType::getColumnType, query.getColumnType());
        queryWrapper.like(StrUtil.isNotBlank(query.getAttrType()), GenFieldType::getAttrType, query.getAttrType());

        Page<GenFieldType> page = new Page<>(query.getPageNum(), query.getPageSize());
        Page<GenFieldType> pageList = genFieldTypeMapper.selectPage(page, queryWrapper);

        return new PageInfo<>(pageList.getRecords(), pageList.getTotal());
    }

    @Override
    public Map<String, GenFieldType> getMap() {
        List<GenFieldType> list = genFieldTypeMapper.selectList(null);
        Map<String, GenFieldType> map = new LinkedHashMap<>(list.size());
        for (GenFieldType entity : list) {
            map.put(entity.getColumnType().toLowerCase(), entity);
        }
        return map;
    }

    @Override
    public Set<String> getPackageByTableId(String tableId) {
        Set<String> importList = genFieldTypeMapper.getPackageByTableId(tableId);

        return importList.stream().filter(StrUtil::isNotBlank).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getList() {
        return genFieldTypeMapper.list();
    }
}
