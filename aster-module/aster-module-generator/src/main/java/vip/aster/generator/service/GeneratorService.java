package vip.aster.generator.service;

import vip.aster.generator.vo.PreviewVO;

import java.util.List;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成
 *
 * @author Aster
 * @since 2024/2/26 11:53
 */
public interface GeneratorService {
    /**
     * 下载代码
     * @param tableId 表id
     * @param zip zip流
     */
    void downloadCode(String tableId, ZipOutputStream zip);

    /**
     * 生成代码
     * @param tableId 表id
     */
    void generatorCode(String tableId);

    /**
     * 预览
     * @param tableId 表id
     * @return 表信息
     */
    List<PreviewVO> preview(String tableId);
}
