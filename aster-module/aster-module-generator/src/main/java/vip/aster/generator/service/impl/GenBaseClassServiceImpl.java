package vip.aster.generator.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vip.aster.common.utils.PageInfo;
import vip.aster.generator.entity.GenBaseClass;
import vip.aster.generator.entity.GenTable;
import vip.aster.generator.mapper.GenBaseClassMapper;
import vip.aster.generator.query.Query;
import vip.aster.generator.service.GenBaseClassService;

/**
 * GenBaseClassServiceImpl
 *
 * @author Aster
 * @since 2024/2/26 14:54
 */
@Slf4j
@Service
@AllArgsConstructor
public class GenBaseClassServiceImpl extends ServiceImpl<GenBaseClassMapper, GenBaseClass> implements GenBaseClassService {
    private final GenBaseClassMapper baseClassMapper;

    @Override
    public PageInfo<GenBaseClass> page(Query query) {
        LambdaQueryWrapper<GenBaseClass> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StrUtil.isNotBlank(query.getCode()), GenBaseClass::getCode, query.getCode());

        Page<GenBaseClass> page = new Page<>(query.getPageNum(), query.getPageSize());
        Page<GenBaseClass> pageList = baseClassMapper.selectPage(page, queryWrapper);

        return new PageInfo<>(pageList.getRecords(), pageList.getTotal());
    }

    @Override
    public void saveBaseClass(GenBaseClass baseClass) {
        if (StrUtil.isNotBlank(baseClass.getId())) {
            baseClassMapper.updateById(baseClass);
        } else {
            baseClassMapper.insert(baseClass);
        }
    }
}
