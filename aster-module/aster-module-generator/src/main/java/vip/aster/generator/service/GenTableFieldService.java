package vip.aster.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.generator.entity.GenTableField;

import java.util.List;

/**
 * GenTableFieldService
 *
 * @author Aster
 * @since 2024/2/26 10:38
 */
public interface GenTableFieldService extends IService<GenTableField> {
    /**
     * 查询表字段
     * @param tableId 表id
     * @return 字段
     */
    List<GenTableField> getByTableId(String tableId);

    /**
     * 批量删除
     * @param tableIds 表id
     */
    void deleteBatchTableIds(String[] tableIds);

    /**
     * 修改表字段数据
     *
     * @param tableId        表ID
     * @param tableFieldList 字段列表
     */
    void updateTableField(String tableId, List<GenTableField> tableFieldList);

    /**
     * 初始化字段数据
     */
    void initFieldList(List<GenTableField> tableFieldList);
}
