package vip.aster.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.generator.query.Query;
import vip.aster.common.utils.PageInfo;
import vip.aster.generator.entity.GenTable;

/**
 * GenTableService
 *
 * @author Aster
 * @since 2024/2/26 10:39
 */
public interface GenTableService extends IService<GenTable> {
    /**
     * 分页
     * @param query 查询条件
     * @return 分页结果
     */
    PageInfo<GenTable> page(Query query);

    /**
     * 通过表名称查询表信息
     * @param tableName 表名
     * @return 表信息
     */
    GenTable getByTableName(String tableName);

    /**
     * 批量删除
     * @param ids 表id
     */
    void deleteBatchIds(String[] ids);

    /**
     * 导入表
     *
     * @param datasourceId 数据源ID
     * @param tableName    表名
     */
    void tableImport(String datasourceId, String tableName);

    /**
     * 同步数据库表
     *
     * @param id 表ID
     */
    void sync(String id);
}
