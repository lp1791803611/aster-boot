package vip.aster.generator.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.generator.entity.GenTable;
import vip.aster.generator.entity.GenTableField;
import vip.aster.generator.query.Query;
import vip.aster.generator.service.GenTableFieldService;
import vip.aster.generator.service.GenTableService;

import java.util.List;

/**
 * 数据表管理
 *
 * @author Aster
 * @since 2024/2/26 16:23
 */
@RestController
@RequestMapping("/gen/table")
@AllArgsConstructor
@Tag(name = "数据表管理")
public class TableController {
    private final GenTableService tableService;
    private final GenTableFieldService tableFieldService;

    /**
     * 分页
     *
     * @param query 查询参数
     * @return 分页
     */
    @GetMapping("/page")
    public ResultInfo<PageInfo<GenTable>> page(@ParameterObject @Valid Query query) {
        PageInfo<GenTable> page = tableService.page(query);

        return ResultInfo.success(page);
    }

    /**
     * 获取表信息
     *
     * @param id 表id
     * @return 表信息
     */
    @GetMapping("/{id}")
    public ResultInfo<GenTable> get(@PathVariable("id") String id) {
        GenTable table = tableService.getById(id);

        // 获取表的字段
        List<GenTableField> fieldList = tableFieldService.getByTableId(table.getId());
        table.setFieldList(fieldList);

        return ResultInfo.success(table);
    }

    /**
     * 修改
     *
     * @param table 表
     * @return 状态
     */
    @PostMapping("/edit")
    public ResultInfo<String> update(@RequestBody GenTable table) {
        tableService.updateById(table);

        return ResultInfo.success();
    }

    /**
     * 删除
     *
     * @param ids 表id数组
     * @return 删除状态
     */
    @PostMapping("/delete")
    public ResultInfo<String> delete(@RequestBody String[] ids) {
        tableService.deleteBatchIds(ids);

        return ResultInfo.success();
    }

    /**
     * 同步表结构
     *
     * @param id 表id
     * @return 同步状态
     */
    @PostMapping("/sync/{id}")
    public ResultInfo<String> sync(@PathVariable("id") String id) {
        tableService.sync(id);

        return ResultInfo.success();
    }

    /**
     * 导入数据源中的表
     *
     * @param datasourceId  数据源ID
     * @param tableNameList 表名列表
     */
    @PostMapping("/import/{datasourceId}")
    public ResultInfo<String> tableImport(@PathVariable("datasourceId") String datasourceId, @RequestBody List<String> tableNameList) {
        for (String tableName : tableNameList) {
            tableService.tableImport(datasourceId, tableName);
        }

        return ResultInfo.success();
    }

    /**
     * 修改表字段数据
     *
     * @param tableId        表ID
     * @param tableFieldList 字段列表
     */
    @PostMapping("/field/{tableId}")
    public ResultInfo<String> updateTableField(@PathVariable("tableId") String tableId, @RequestBody List<GenTableField> tableFieldList) {
        tableFieldService.updateTableField(tableId, tableFieldList);

        return ResultInfo.success();
    }

}
