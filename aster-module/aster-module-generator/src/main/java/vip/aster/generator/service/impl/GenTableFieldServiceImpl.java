package vip.aster.generator.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.aster.generator.entity.GenFieldType;
import vip.aster.generator.entity.GenTableField;
import vip.aster.generator.enums.AutoFillEnum;
import vip.aster.generator.mapper.GenTableFieldMapper;
import vip.aster.generator.service.GenFieldTypeService;
import vip.aster.generator.service.GenTableFieldService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * GenTableFieldServiceImpl
 *
 * @author Aster
 * @since 2024/2/26 10:43
 */
@Service
@AllArgsConstructor
public class GenTableFieldServiceImpl extends ServiceImpl<GenTableFieldMapper, GenTableField> implements GenTableFieldService {
    private final GenTableFieldMapper genTableFieldMapper;
    private final GenFieldTypeService genFieldTypeService;

    @Override
    public List<GenTableField> getByTableId(String tableId) {
        return genTableFieldMapper.getByTableId(tableId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatchTableIds(String[] tableIds) {
        List<String> list = Arrays.stream(tableIds).toList();
        this.removeBatchByIds(list);
    }

    @Override
    public void updateTableField(String tableId, List<GenTableField> tableFieldList) {
        // 更新字段数据
        int sort = 0;
        for (GenTableField tableField : tableFieldList) {
            tableField.setSort(sort++);
            this.updateById(tableField);
        }
    }

    @Override
    public void initFieldList(List<GenTableField> tableFieldList) {
        // 字段类型、属性类型映射
        Map<String, GenFieldType> fieldTypeMap = genFieldTypeService.getMap();
        int index = 0;
        for (GenTableField field : tableFieldList) {
            field.setAttrName(StringUtils.underlineToCamel(field.getFieldName()));
            // 获取字段对应的类型
            GenFieldType fieldTypeMapping = fieldTypeMap.get(field.getFieldType().toLowerCase());
            if (fieldTypeMapping == null) {
                // 没找到对应的类型，则为Object类型
                field.setAttrType("Object");
            } else {
                field.setAttrType(fieldTypeMapping.getAttrType());
                field.setPackageName(fieldTypeMapping.getPackageName());
            }

            field.setAutoFill(AutoFillEnum.DEFAULT.name());
            field.setFormItem(true);
            field.setGridItem(true);
            field.setQueryType("=");
            field.setQueryFormType("text");
            field.setFormType("text");
            field.setSort(index++);
        }
    }
}
