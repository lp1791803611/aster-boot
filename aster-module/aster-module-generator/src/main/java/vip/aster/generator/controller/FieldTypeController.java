package vip.aster.generator.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.generator.entity.GenFieldType;
import vip.aster.generator.query.Query;
import vip.aster.generator.service.GenFieldTypeService;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * 字段类型管理
 *
 * @author Aster
 * @since 2024/2/26 16:38
 */
@Slf4j
@RestController
@RequestMapping("/gen/fieldType")
@AllArgsConstructor
@Tag(name = "字段类型管理")
public class FieldTypeController {
    private final GenFieldTypeService fieldTypeService;

    @GetMapping("/page")
    public ResultInfo<PageInfo<GenFieldType>> page(Query query) {
        PageInfo<GenFieldType> page = fieldTypeService.page(query);

        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    public ResultInfo<GenFieldType> get(@PathVariable("id") String id) {
        GenFieldType data = fieldTypeService.getById(id);

        return ResultInfo.success(data);
    }

    @GetMapping("/list")
    public ResultInfo<Set<String>> list() {
        Set<String> set = fieldTypeService.getList();

        return ResultInfo.success(set);
    }

    @PostMapping("/add")
    public ResultInfo<String> save(@RequestBody GenFieldType entity) {
        fieldTypeService.save(entity);

        return ResultInfo.success();
    }

    @PostMapping("/edit")
    public ResultInfo<String> update(@RequestBody GenFieldType entity) {
        fieldTypeService.updateById(entity);

        return ResultInfo.success();
    }

    @PostMapping("/delete")
    public ResultInfo<String> delete(@RequestBody List<String> ids) {
        fieldTypeService.removeBatchByIds(ids);

        return ResultInfo.success();
    }
}
