package vip.aster.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.generator.entity.GenBaseClass;
import vip.aster.generator.query.Query;

/**
 * 基类 服务类
 *
 * @author Aster
 * @since 2024/2/26 14:54
 */
public interface GenBaseClassService extends IService<GenBaseClass> {
    /**
     * 分页
     * @param query 条件
     * @return data
     */
    PageInfo<GenBaseClass> page(Query query);

    /**
     * 保存
     * @param baseClass 基类
     */
    void saveBaseClass(GenBaseClass baseClass);
}
