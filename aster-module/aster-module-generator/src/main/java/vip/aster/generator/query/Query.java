package vip.aster.generator.query;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

/**
 * 查询公共参数
 *
 * @author Aster
 * @since 2024/3/4 10:06
 */
@Data
public class Query {
    String code;
    String tableName;
    String attrType;
    String columnType;
    String connName;
    String dbType;
    String projectName;

    @Schema(description = "当前页码")
    @Min(value = 1, message = "页码最小值为 1")
    Integer pageNum;

    @Schema(description = "每页条数")
    @Range(min = 1, max = 1000, message = "每页条数，取值范围 1-1000")
    Integer pageSize;
}
