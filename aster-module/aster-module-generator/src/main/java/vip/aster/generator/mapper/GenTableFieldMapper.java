package vip.aster.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.generator.entity.GenTableField;

import java.util.List;

/**
 * 表字段 Mapper 接口
 *
 * @author Aster
 * @since 2024/2/26 10:04
 */
public interface GenTableFieldMapper extends BaseMapper<GenTableField> {
    /**
     * 通过表id查询表字段
     * @param tableId 表id
     * @return 表字段
     */
    List<GenTableField> getByTableId(String tableId);

    /**
     * 批量删除
     * @param tableIds 表id
     */
    void deleteBatchTableIds(String[] tableIds);
}
