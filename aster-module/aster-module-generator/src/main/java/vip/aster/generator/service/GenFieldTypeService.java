package vip.aster.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.generator.entity.GenFieldType;
import vip.aster.generator.query.Query;

import java.util.Map;
import java.util.Set;

/**
 * GenFieldTypeService
 *
 * @author Aster
 * @since 2024/2/26 10:37
 */
public interface GenFieldTypeService extends IService<GenFieldType> {
    /**
     * 分页
     * @param query 查询条件
     * @return 分页结果
     */
    PageInfo<GenFieldType> page(Query query);

    /**
     * 获取所有字段类型
     * @return 字段类型
     */
    Map<String, GenFieldType> getMap();

    /**
     * 根据tableId，获取包列表
     *
     * @param tableId 表ID
     * @return 返回包列表
     */
    Set<String> getPackageByTableId(String tableId);

    /**
     * 获取所有字段类型
     * @return 字段类型
     */
    Set<String> getList();
}
