package vip.aster.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.aster.generator.entity.GenFieldType;

import java.util.Set;

/**
 * 字段类型 Mapper 接口
 *
 * @author Aster
 * @since 2024/2/26 10:01
 */
public interface GenFieldTypeMapper extends BaseMapper<GenFieldType> {
    /**
     * 根据tableId，获取包列表
     */
    Set<String> getPackageByTableId(@Param("tableId") String tableId);

    /**
     * 获取全部字段类型
     */
    Set<String> list();
}
