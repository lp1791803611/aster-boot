package vip.aster.generator.config.template;

import lombok.Data;

/**
 * 开发者信息
 *
 * @author Aster
 * @since 2024/2/23 15:13
 */
@Data
public class DeveloperInfo {
    /**
     * 作者
     */
    private String author;
    /**
     * 邮箱
     */
    private String email;
}
