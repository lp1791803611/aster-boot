package vip.aster.generator.enums;

/**
 * 字段自动填充 枚举
 *
 * @author Aster
 * @since 2024/2/23 15:09
 */
public enum AutoFillEnum {
    DEFAULT, INSERT, UPDATE, INSERT_UPDATE;
}
