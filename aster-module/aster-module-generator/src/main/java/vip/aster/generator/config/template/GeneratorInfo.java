package vip.aster.generator.config.template;

import lombok.Data;

import java.util.List;

/**
 * 代码生成信息
 *
 * @author Aster
 * @since 2024/2/23 15:13
 */
@Data
public class GeneratorInfo {
    private ProjectInfo project;
    private DeveloperInfo developer;
    private List<TemplateInfo> templates;
}
