package vip.aster.generator.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.generator.entity.GenBaseClass;
import vip.aster.generator.query.Query;
import vip.aster.generator.service.GenBaseClassService;

import java.util.List;

/**
 * 基类管理 控制器
 *
 * @author Aster
 * @since 2024/2/29 11:40
 */
@RestController
@RequestMapping("/gen/baseClass")
@AllArgsConstructor
@Tag(name = "代码生成")
public class BaseClassController {
    private GenBaseClassService baseClassService;

    @GetMapping("/list")
    public ResultInfo<List<GenBaseClass>> list() {
        List<GenBaseClass> list = baseClassService.list();
        return ResultInfo.success(list);
    }

    @GetMapping("/page")
    public ResultInfo<PageInfo<GenBaseClass>> page(@ParameterObject @Valid Query query) {
        PageInfo<GenBaseClass> page = baseClassService.page(query);
        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    public ResultInfo<GenBaseClass> info(@PathVariable("id") String id) {
        GenBaseClass baseClass = baseClassService.getById(id);
        return ResultInfo.success(baseClass);
    }

    @PostMapping("/save")
    public ResultInfo<String> save(@RequestBody GenBaseClass baseClass) {
        baseClassService.saveBaseClass(baseClass);
        return ResultInfo.success();
    }

    @PostMapping("/delete")
    public ResultInfo<String> delete(@RequestBody List<String> ids) {
        baseClassService.removeBatchByIds(ids);
        return ResultInfo.success();
    }
}
