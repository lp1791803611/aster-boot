package vip.aster.generator.config;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import vip.aster.generator.config.template.GeneratorConfig;

/**
 * GeneratorAutoConfiguration
 *
 * @author Aster
 * @since 2024/2/26 16:10
 */
@Configuration
@AllArgsConstructor
@ComponentScan(basePackages = {"vip.aster.generator"})
@EnableConfigurationProperties(GeneratorProperties.class)
public class GeneratorAutoConfiguration {
    private final GeneratorProperties properties;

    @Bean
    GeneratorConfig generatorConfig() {
        return new GeneratorConfig(properties.getTemplate());
    }

}
