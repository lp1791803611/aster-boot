package vip.aster.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.generator.entity.GenDataSource;

/**
 * 数据源 Mapper 接口
 *
 * @author Aster
 * @since 2024/2/26 9:58
 */
public interface GenDataSourceMapper extends BaseMapper<GenDataSource> {
}
