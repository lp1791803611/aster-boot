package vip.aster.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.generator.config.DataSourceConfig;
import vip.aster.generator.entity.GenDataSource;
import vip.aster.generator.query.Query;

import java.util.List;

/**
 * GenDataSourceService
 *
 * @author Aster
 * @since 2024/2/26 10:35
 */
public interface GenDataSourceService extends IService<GenDataSource> {
    /**
     * 分页
     * @param query 查询条件
     * @return 分页结果
     */
    PageInfo<GenDataSource> page(Query query);

    /**
     * 查询所有数据源
     * @return 数据源
     */
    List<GenDataSource> getList();

    /**
     * 获取数据库产品名，如：MySQL
     *
     * @param datasourceId 数据源ID
     * @return 返回产品名
     */
    String getDatabaseProductName(String datasourceId);

    /**
     * 根据数据源ID，获取数据源
     *
     * @param datasourceId 数据源ID
     */
    DataSourceConfig get(String datasourceId);


    /**
     * 保存
     * @param entity 数据源
     */
    void saveDataSource(GenDataSource entity);
}
