package vip.aster.generator.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * PreviewVO
 *
 * @author Aster
 * @since 2024/2/26 11:54
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class PreviewVO {
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件内容
     */
    private String content;


}
