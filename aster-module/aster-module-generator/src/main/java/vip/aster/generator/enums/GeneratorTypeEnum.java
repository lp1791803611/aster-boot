package vip.aster.generator.enums;

/**
 * 代码生成方式 枚举
 *
 * @author Aster
 * @since 2024/2/23 15:10
 */
public enum GeneratorTypeEnum {
    /**
     * zip压缩包
     */
    ZIP_DOWNLOAD,
    /**
     * 自定义目录
     */
    CUSTOM_DIRECTORY;
}
