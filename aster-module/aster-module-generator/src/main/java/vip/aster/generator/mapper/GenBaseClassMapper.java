package vip.aster.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.generator.entity.GenBaseClass;

/**
 * 基类 Mapper 接口
 *
 * @author Aster
 * @since 2024/2/26 14:52
 */
public interface GenBaseClassMapper extends BaseMapper<GenBaseClass> {
}
