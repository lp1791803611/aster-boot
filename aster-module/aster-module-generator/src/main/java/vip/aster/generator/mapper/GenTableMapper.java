package vip.aster.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.generator.entity.GenTable;

/**
 * 数据表 Mapper 接口
 *
 * @author Aster
 * @since 2024/2/26 10:03
 */
public interface GenTableMapper extends BaseMapper<GenTable> {
}
