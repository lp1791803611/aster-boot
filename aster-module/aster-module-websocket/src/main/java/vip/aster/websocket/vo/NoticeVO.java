package vip.aster.websocket.vo;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * NotifyVO
 *
 * @author Aster
 * @since 2024/3/13 11:03
 */
@Data
public class NoticeVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -4377655564683385041L;

    private String key;

    private String content;

    private String type;

    private String time;

    public NoticeVO() {
        super();
    }

    public NoticeVO(String key, String content, String type) {
        this.key = key;
        this.content = content;
        this.type = type;
    }
}
