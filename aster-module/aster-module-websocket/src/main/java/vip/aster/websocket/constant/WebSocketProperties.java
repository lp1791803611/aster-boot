package vip.aster.websocket.constant;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * WebSocket 配置项
 *
 * @author Aster
 * @since 2024/3/11 16:24
 */
@Data
@ConfigurationProperties("websocket")
public class WebSocketProperties {
    /**
     * 是否开启websocket
     */
    private Boolean enabled;

    /**
     * 路径
     */
    private String path;

    /**
     *  设置访问源地址
     */
    private String allowedOrigins;
}
