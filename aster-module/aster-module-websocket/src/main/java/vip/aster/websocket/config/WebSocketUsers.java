package vip.aster.websocket.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用于保存当前所有在线的会话信息
 *
 * @author Aster
 * @since 2024/3/11 16:37
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WebSocketUsers {
    /**
     * 用户集
     */
    private static final Map<String, WebSocketSession> USERS = new ConcurrentHashMap<>();

    /**
     * 添加session
     * @param sessionKey 会话key
     * @param session 会话
     */
    public static void addSession(String sessionKey, WebSocketSession session) {
        USERS.put(sessionKey, session);
    }

    /**
     * 移除session
     * @param sessionKey 会话key
     */
    public static void removeSession(String sessionKey) {
        USERS.remove(sessionKey);
    }

    /**
     * 通过key获取会话
     * @param sessionKey 会话key
     * @return 会话
     */
    public static WebSocketSession getSessions(String sessionKey) {
        return USERS.get(sessionKey);
    }

    /**
     * 所有session的key
     * @return key
     */
    public static Set<String> getSessionsAll() {
        return USERS.keySet();
    }

    /**
     * 是否存在session
     * @param sessionKey 会话key
     * @return 结果
     */
    public static Boolean existSession(String sessionKey) {
        return USERS.containsKey(sessionKey);
    }

}
