package vip.aster.websocket.vo;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * WebSocketMessageDto
 *
 * @author Aster
 * @since 2024/3/11 16:28
 */
@Data
public class WebSocketMessageVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -6512905186878148525L;

    /**
     * 需要推送到的session key 列表
     */
    private List<String> sessionKeys;

    /**
     * 需要发送的消息
     */
    private String message;
}
