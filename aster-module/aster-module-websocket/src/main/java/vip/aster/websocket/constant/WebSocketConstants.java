package vip.aster.websocket.constant;

/**
 * WebSocket 常量
 *
 * @author Aster
 * @since 2024/3/11 16:27
 */
public class WebSocketConstants {
    /**
     * websocketSession中的参数的key
     */
    public static final String LOGIN_USER_KEY = "loginUser";

    /**
     * 订阅的频道
     */
    public static final String WEB_SOCKET_TOPIC = "global:websocket";

    /**
     * 前端心跳检查的命令
     */
    public static final String PING = "ping";

    /**
     * 服务端心跳恢复的字符串
     */
    public static final String PONG = "pong";
}
