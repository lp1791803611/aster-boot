package vip.aster.websocket.constant;

import lombok.Getter;

/**
 * 消息类型 枚举
 *
 * @author Aster
 * @since 2024/3/13 14:26
 */
@Getter
public enum NoticeTypeEnum {

    /**
     * 通知
     */
    NOTICE("1", "通知"),
    /**
     * 公告
     */
    Announcement("2", "公告"),
    /**
     * 消息
     */
    MESSAGE("3", "消息");


    private final String value;
    private final String label;

    NoticeTypeEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public static String getLabel(String value) {
        NoticeTypeEnum[] enums = NoticeTypeEnum.values();
        for (NoticeTypeEnum bEnum : enums) {
            if (bEnum.getValue().equals(value)) {
                return bEnum.getLabel();
            }
        }
        return null;
    }

    public static String getValue(String label) {
        NoticeTypeEnum[] enums = NoticeTypeEnum.values();
        for (NoticeTypeEnum bEnum : enums) {
            if (bEnum.getLabel().equals(label)) {
                return bEnum.getValue();
            }
        }
        return null;
    }


}
