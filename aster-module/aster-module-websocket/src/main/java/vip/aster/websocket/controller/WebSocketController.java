package vip.aster.websocket.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.aster.websocket.utils.WebSocketUtils;
import vip.aster.websocket.vo.WebSocketMessageVO;

import java.util.List;

/**
 * WebSocketController
 *
 * @author Aster
 * @since 2024/3/18 17:26
 */
@RestController
@RequestMapping("/demo/socket")
public class WebSocketController {

    @GetMapping("/publish")
    public void publish(String key, String message) {
//        WebSocketMessageVO messageVO = new WebSocketMessageVO();
//        messageVO.setMessage(message);
//        messageVO.setSessionKeys(List.of(key));
//        WebSocketUtils.publishMessage(messageVO);
        WebSocketUtils.publishAll(message);
    }
}
