package vip.aster.monitor.vo;

import cn.hutool.core.util.StrUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 缓存信息
 *
 * @author Aster
 * @since 2024/2/5 18:11
 */
@Data
@Schema(description = "缓存信息")
public class CacheVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -7696144881777628854L;

    @Schema(description = "缓存名称")
    private String cacheName;

    @Schema(description = "缓存key")
    private String cacheKey;

    @Schema(description = "缓存value")
    private String cacheValue;

    @Schema(description = "备注")
    private String remark;

    public CacheVO() {
        super();
    }

    public CacheVO(String cacheName, String remark) {
        this.cacheName = cacheName;
        this.remark = remark;
    }

    public CacheVO(String cacheName, String cacheKey, String cacheValue) {
        this.cacheName = StrUtil.replace(cacheName, ":", "");
        this.cacheKey = StrUtil.replace(cacheKey, cacheName, "");
        this.cacheValue = cacheValue;
    }
}
