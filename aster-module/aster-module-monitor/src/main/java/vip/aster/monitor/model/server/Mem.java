package vip.aster.monitor.model.server;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 内存相关信息
 *
 * @author Aster
 * @since 2023/11/16 17:28
 */
@Data
@Schema(description = "内存")
public class Mem implements Serializable {

    @Serial
    private static final long serialVersionUID = 7075125303791096445L;

    @Schema(description = "内存总量")
    private double total;

    @Schema(description = "已用内存")
    private double used;

    @Schema(description = "剩余内存")
    private double free;

    @Schema(description = "资源的使用率")
    private double usage;

    @Schema(description = "交换区总量")
    private double swapTotal;

    @Schema(description = "交换区已使用量")
    private double swapUsed;

    @Schema(description = "交换区未使用量")
    private double swapFree;

    @Schema(description = "资源的使用率")
    private double swapUsage;
}
