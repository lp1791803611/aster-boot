package vip.aster.monitor.model.server;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * CPU相关信息
 *
 * @author Aster
 * @since 2023/11/16 17:14
 */
@Data
@Schema(description = "CPU")
public class Cpu implements Serializable {

    @Serial
    private static final long serialVersionUID = 2595820742884228441L;

    @Schema(description = "CPU型号")
    private String name;

    @Schema(description = "核心数")
    private int cpuNum;

    @Schema(description = "CPU总的使用率")
    private double total;

    @Schema(description = "CPU系统使用率")
    private double sys;

    @Schema(description = "CPU用户使用率")
    private double used;

    @Schema(description = "CPU当前等待率")
    private double wait;

    @Schema(description = "CPU当前空闲率")
    private double free;

    @Schema(description = "资源的使用率")
    private double usage;
}
