package vip.aster.monitor.controller;

import cn.hutool.core.collection.CollUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.aster.common.utils.ResultInfo;
import vip.aster.monitor.utils.ServerUtils;
import vip.aster.monitor.vo.ServerVO;

import java.util.List;

/**
 * 服务监控
 *
 * @author Aster
 * @since 2024/2/2 14:31
 */
@RestController
@RequestMapping("/monitor/server")
@AllArgsConstructor
@Tag(name = "服务监控")
public class ServerController {

    /**
     * 服务器相关信息
     */
    @GetMapping("/all")
    @Operation(summary = "服务器信息")
    @PreAuthorize("hasAuthority('monitor:server:all')")
    public ResultInfo<ServerVO> getServerInfo() {
        ServerVO server = new ServerVO();
        server.setCpu(ServerUtils.getCpuInfo());
        server.setMem(ServerUtils.getMemInfo());
        server.setJvm(ServerUtils.getJvmInfo());
        server.setSys(ServerUtils.getSysInfo());
        server.setDisks(ServerUtils.getDiskInfo());
        return ResultInfo.success(server);
    }

}
