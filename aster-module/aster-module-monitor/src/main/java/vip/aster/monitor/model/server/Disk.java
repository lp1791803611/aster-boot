package vip.aster.monitor.model.server;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 磁盘相关信息
 *
 * @author Aster
 * @since 2023/11/16 18:35
 */
@Data
@Schema(description = "磁盘")
public class Disk implements Serializable {

    @Serial
    private static final long serialVersionUID = 6182515858170421345L;

    @Schema(description = "磁盘路径")
    private String dirName;

    @Schema(description = "磁盘类型")
    private String sysTypeName;

    @Schema(description = "磁盘名称")
    private String typeName;

    @Schema(description = "总大小")
    private String total;

    @Schema(description = "剩余大小")
    private String free;

    @Schema(description = "已经使用量")
    private String used;

    @Schema(description = "空间使用率")
    private double usage;
}
