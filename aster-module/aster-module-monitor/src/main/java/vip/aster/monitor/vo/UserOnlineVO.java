package vip.aster.monitor.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.framework.security.entity.UserDetail;

/**
 * 在线用户
 *
 * @author Aster
 * @since 2024/2/2 9:45
 */
@Data
@Schema(description = "在线用户")
public class UserOnlineVO {

    @Schema(description = "会话编号")
    private String tokenId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "姓名")
    private String realName;

    @Schema(description = "性别")
    private String gender;

    @Schema(description = "邮箱")
    private String email;

    @Schema(description = "手机")
    private String mobile;

    public UserOnlineVO() {
        super();
    }

    public UserOnlineVO(UserDetail userDetail) {
        this.username = userDetail.getUsername();
        this.realName = userDetail.getRealName();
        this.gender = userDetail.getGender();
        this.email = userDetail.getEmail();
        this.mobile = userDetail.getMobile();
    }
}
