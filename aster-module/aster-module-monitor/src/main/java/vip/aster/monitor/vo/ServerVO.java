package vip.aster.monitor.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.monitor.model.server.*;

import java.util.LinkedList;
import java.util.List;

/**
 * 服务器信息
 *
 * @author Aster
 * @since 2024/2/2 14:41
 */
@Data
@Schema(description = "服务器信息")
public class ServerVO {

    @Schema(description = "CPU信息")
    private Cpu cpu;

    @Schema(description = "内存信息")
    private Mem mem;

    @Schema(description = "JVM信息")
    private Jvm jvm;

    @Schema(description = "系统信息")
    private Sys sys;

    @Schema(description = "系统文件信息")
    private List<Disk> disks = new LinkedList<>();
}
