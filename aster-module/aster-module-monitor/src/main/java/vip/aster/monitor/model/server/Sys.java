package vip.aster.monitor.model.server;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 系统相关信息
 *
 * @author Aster
 * @since 2023/11/16 16:44
 */
@Data
@Schema(description = "系统")
public class Sys implements Serializable {

    @Serial
    private static final long serialVersionUID = -3131004220385850690L;

    @Schema(description = "系统名称")
    private String computerName;

    @Schema(description = "系统Ip")
    private String computerIp;

    @Schema(description = "项目路径")
    private String userDir;

    @Schema(description = "操作系统")
    private String osName;

    @Schema(description = "系统架构")
    private String osArch;
}
