package vip.aster.monitor.model.server;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * JVM相关信息
 *
 * @author Aster
 * @since 2023/11/16 17:10
 */
@Data
@Schema(description = "JVM")
public class Jvm implements Serializable {

    @Serial
    private static final long serialVersionUID = -7007713139529685269L;

    @Schema(description = "当前JVM占用的内存总数(M)")
    private double total;

    @Schema(description = "JVM最大可用内存总数(M)")
    private double max;

    @Schema(description = "JVM空闲内存(M)")
    private double free;

    @Schema(description = "JVM已使用内存(M)")
    private double used;

    @Schema(description = "JVM使用率")
    private double usage;

    @Schema(description = "JDK名称")
    private String name;

    @Schema(description = "JDK版本")
    private String version;

    @Schema(description = "JDK路径")
    private String home;

    @Schema(description = "JDK启动时间")
    private Date startTime;

    @Schema(description = "JDK运行时间")
    private String runTime;

}
