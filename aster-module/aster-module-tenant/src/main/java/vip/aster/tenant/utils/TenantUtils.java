package vip.aster.tenant.utils;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.plugins.IgnoreStrategy;
import com.baomidou.mybatisplus.core.plugins.InterceptorIgnoreHelper;
import lombok.extern.slf4j.Slf4j;
import vip.aster.common.constant.CacheConstants;
import vip.aster.common.utils.CacheUtils;
import vip.aster.framework.security.entity.SecurityUser;

import java.util.function.Supplier;

/**
 * TenantUtils
 *
 * @author Aster
 * @since 2024/3/25 10:19
 */
@Slf4j
public class TenantUtils {
    /**
     * 租户功能是否启用
     */
    public static boolean isEnable() {
        return Convert.toBool(SpringUtil.getProperty("tenant.enable"), false);
    }

    /**
     * 开启忽略租户(开启后需手动调用 {@link #disableIgnore()} 关闭)
     */
    public static void enableIgnore() {
        InterceptorIgnoreHelper.handle(IgnoreStrategy.builder().tenantLine(true).build());
    }

    /**
     * 关闭忽略租户
     */
    public static void disableIgnore() {
        InterceptorIgnoreHelper.clearIgnoreStrategy();
    }

    /**
     * 在忽略租户中执行
     *
     * @param handle 处理执行方法
     */
    public static void ignore(Runnable handle) {
        enableIgnore();
        try {
            handle.run();
        } finally {
            disableIgnore();
        }
    }

    /**
     * 在忽略租户中执行
     *
     * @param handle 处理执行方法
     */
    public static <T> T ignore(Supplier<T> handle) {
        enableIgnore();
        try {
            return handle.get();
        } finally {
            disableIgnore();
        }
    }

    /**
     * 设置动态租户(一直有效 需要手动清理)
     * <p>
     * 如果为未登录状态下 那么只在当前线程内生效
     */
    public static void setDynamic(String tenantId) {
        if (!isEnable()) {
            return;
        }
        if (!isLogin()) {
            return;
        }
        String userId = SecurityUser.getUserId();
        CacheUtils.set(CacheConstants.TENANT_KEY, userId, tenantId);
    }

    /**
     * 获取动态租户(一直有效 需要手动清理)
     * <p>
     * 如果为未登录状态下 那么只在当前线程内生效
     */
    public static String getDynamic() {
        if (!isEnable()) {
            return null;
        }
        if (!isLogin()) {
            return null;
        }
        String userId = SecurityUser.getUserId();
        return CacheUtils.get(CacheConstants.TENANT_KEY, userId, String.class);
    }

    /**
     * 清除动态租户
     */
    public static void clearDynamic() {
        if (!isEnable()) {
            return;
        }
        if (!isLogin()) {
            return;
        }
        String userId = SecurityUser.getUserId();
        CacheUtils.delete(CacheConstants.TENANT_KEY, userId);
    }

    /**
     * 在动态租户中执行
     *
     * @param handle 处理执行方法
     */
    public static void dynamic(String tenantId, Runnable handle) {
        setDynamic(tenantId);
        try {
            handle.run();
        } finally {
            clearDynamic();
        }
    }

    /**
     * 在动态租户中执行
     *
     * @param handle 处理执行方法
     */
    public static <T> T dynamic(String tenantId, Supplier<T> handle) {
        setDynamic(tenantId);
        try {
            return handle.get();
        } finally {
            clearDynamic();
        }
    }

    /**
     * 获取当前租户id(动态租户优先)
     */
    public static String getTenantId() {
        if (!isEnable()) {
            return null;
        }
        String tenantId = TenantUtils.getDynamic();
        if (StrUtil.isBlank(tenantId)) {
            tenantId = SecurityUser.getTenantId();
        }
        return tenantId;
    }

    private static boolean isLogin() {
        try {
            return SecurityUser.isLogin();
        } catch (Exception e) {
            return false;
        }
    }
}
