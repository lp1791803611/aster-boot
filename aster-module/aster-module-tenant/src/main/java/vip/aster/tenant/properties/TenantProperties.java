package vip.aster.tenant.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 租户 配置属性
 *
 * @author Aster
 * @since 2024/3/22 17:33
 */
@Data
@ConfigurationProperties(prefix = "tenant")
public class TenantProperties {
    /**
     * 是否启用
     */
    private Boolean enable;
    /**
     * 租户字段
     */
    private String column;
    /**
     * 忽略的表名
     */
    private List<String> ignoreTables;
}
