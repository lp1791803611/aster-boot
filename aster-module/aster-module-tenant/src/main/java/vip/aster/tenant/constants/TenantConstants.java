package vip.aster.tenant.constants;

/**
 * 租户常量信息
 *
 * @author Aster
 * @since 2024/3/25 11:12
 */
public interface TenantConstants {

    /**
     * 超级管理员ID
     */
    String SUPER_ADMIN_ID = "1";

    /**
     * 超级管理员角色 roleKey
     */
    String SUPER_ADMIN_ROLE_KEY = "super_admin";

    /**
     * 租户管理员角色 roleKey
     */
    String TENANT_ADMIN_ROLE_KEY = "admin";

    /**
     * 租户管理员角色名称
     */
    String TENANT_ADMIN_ROLE_NAME = "管理员";

    /**
     * 默认租户ID
     */
    String DEFAULT_TENANT_ID = "000000";

    /**
     * 默认用户限制数量
     */
    Integer USER_LIMIT_NUM = -1;
}
