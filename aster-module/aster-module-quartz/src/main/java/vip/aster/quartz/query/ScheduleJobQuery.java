package vip.aster.quartz.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

/**
 * 定时任务调度查询
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 10:24:20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "定时任务调度查询")
public class ScheduleJobQuery extends Query {
    @Schema(description = "任务名称")
    private String jobName;

    @Schema(description = "任务组名")
    private String jobGroup;

    @Schema(description = "状态")
    private String status;
}