package vip.aster.quartz.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 定时任务调度日志
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 11:10:07
 */

@Data
@TableName("schedule_job_log")
public class ScheduleJobLog implements Serializable {

	@Serial
	private static final long serialVersionUID = 1895997965475326578L;

	/**
	* 任务日志ID
	*/
	@TableId("id")
	private String id;
	/**
	 * 任务ID
	 */
	@TableField("job_id")
	private String jobId;
	/**
	* 任务名称
	*/
	@TableField("job_name")
	private String jobName;
	/**
	* 任务组名
	*/
	@TableField("job_group")
	private String jobGroup;
	/**
	* 调用目标字符串
	*/
	@TableField("invoke_target")
	private String invokeTarget;
	/**
	* 日志信息
	*/
	@TableField("job_message")
	private String jobMessage;
	/**
	* 执行状态（0正常 1失败）
	*/
	@TableField("status")
	private String status;
	/**
	* 异常信息
	*/
	@TableField("exception_info")
	private String exceptionInfo;
	/**
	* 创建时间
	*/
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime createTime;
}