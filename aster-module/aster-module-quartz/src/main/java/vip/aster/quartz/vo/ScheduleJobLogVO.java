package vip.aster.quartz.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.quartz.entity.ScheduleJobLog;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 定时任务调度日志
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 11:10:07
 */
@Data
@Schema(description = "定时任务调度日志")
public class ScheduleJobLogVO implements Serializable {

	@Serial
	private static final long serialVersionUID = 4381209824604307251L;

	@Schema(description = "任务日志ID")
	private String id;
	@Schema(description = "任务ID")
	private String jobId;
	@Schema(description = "任务名称")
	private String jobName;
	@Schema(description = "任务组名")
	private String jobGroup;
	@Schema(description = "调用目标字符串")
	private String invokeTarget;
	@Schema(description = "日志信息")
	private String jobMessage;
	@Schema(description = "执行状态（0正常 1失败）")
	private String status;
	@Schema(description = "异常信息")
	private String exceptionInfo;
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	public ScheduleJobLogVO(ScheduleJobLog entity) {
		this.id = entity.getId();
		this.jobId = entity.getJobId();
		this.jobName = entity.getJobName();
		this.jobGroup = entity.getJobGroup();
		this.invokeTarget = entity.getInvokeTarget();
		this.jobMessage = entity.getJobMessage();
		this.status = entity.getStatus();
		this.exceptionInfo = entity.getExceptionInfo();
		this.createTime = entity.getCreateTime();
	}

	public static List<ScheduleJobLogVO> convertList(List<ScheduleJobLog> source) {
		if (CollUtil.isEmpty(source)) {
			return ListUtil.empty();
		}
		List<ScheduleJobLogVO> list = new ArrayList<>();
		for (ScheduleJobLog entity : source) {
			list.add(new ScheduleJobLogVO(entity));
		}
		return list;
	}

	public ScheduleJobLog reconvert() {
		ScheduleJobLog entity = new ScheduleJobLog();
		entity.setId(this.id);
		entity.setJobId(this.jobId);
		entity.setJobName(this.jobName);
		entity.setJobGroup(this.jobGroup);
		entity.setInvokeTarget(this.invokeTarget);
		entity.setJobMessage(this.jobMessage);
		entity.setStatus(this.status);
		entity.setExceptionInfo(this.exceptionInfo);
		entity.setCreateTime(this.createTime);
		return entity;
	}

}