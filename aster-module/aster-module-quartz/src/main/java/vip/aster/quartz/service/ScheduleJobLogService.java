package vip.aster.quartz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.quartz.entity.ScheduleJobLog;
import vip.aster.quartz.query.ScheduleJobLogQuery;
import vip.aster.quartz.vo.ScheduleJobLogVO;

/**
 * <P>
 * 定时任务调度日志 服务接口
 * </P>
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 11:10:07
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLog> {

    /**
     * 分页
     * @param query 查询条件
     * @return 分页结果
     */
    PageInfo<ScheduleJobLogVO> pageList(ScheduleJobLogQuery query);

    /**
     * 保存
     * @param vo 日志信息
     */
    void save(ScheduleJobLogVO vo);

    /**
     * 清空日志
     */
    void clear();

}