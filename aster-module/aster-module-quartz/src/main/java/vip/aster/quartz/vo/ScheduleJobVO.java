package vip.aster.quartz.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.quartz.entity.ScheduleJob;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 定时任务调度
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 10:24:20
 */
@Data
@Schema(description = "定时任务调度")
public class ScheduleJobVO implements Serializable {

	@Serial
	private static final long serialVersionUID = -3670774366255142208L;

	@Schema(description = "任务ID")
	private String id;
	@Schema(description = "任务名称")
	private String jobName;
	@Schema(description = "任务组名")
	private String jobGroup;
	@Schema(description = "调用目标字符串")
	private String invokeTarget;
	@Schema(description = "cron执行表达式")
	private String cronExpression;
	@Schema(description = "计划执行错误策略（1立即执行 2执行一次 3放弃执行）")
	private String misfirePolicy;
	@Schema(description = "是否并发执行（0允许 1禁止）")
	private String concurrent;
	@Schema(description = "版本号")
	private Integer version;
	@Schema(description = "创建时间")
	private LocalDateTime createTime;
	@Schema(description = "创建者")
	private String createBy;
	@Schema(description = "更新时间")
	private LocalDateTime updateTime;
	@Schema(description = "修改者")
	private String updateBy;
	@Schema(description = "启用状态")
	private String status;
	@Schema(description = "删除状态")
	private String isDeleted;
	@Schema(description = "备注信息")
	private String remark;

	public ScheduleJobVO() {
		super();
	}

	public ScheduleJobVO(ScheduleJob entity) {
		this.id = entity.getId();
		this.jobName = entity.getJobName();
		this.jobGroup = entity.getJobGroup();
		this.invokeTarget = entity.getInvokeTarget();
		this.cronExpression = entity.getCronExpression();
		this.misfirePolicy = entity.getMisfirePolicy();
		this.concurrent = entity.getConcurrent();
		this.version = entity.getVersion();
		this.createTime = entity.getCreateTime();
		this.createBy = entity.getCreateBy();
		this.updateTime = entity.getUpdateTime();
		this.updateBy = entity.getUpdateBy();
		this.status = entity.getStatus();
		this.isDeleted = entity.getIsDeleted();
		this.remark = entity.getRemark();
	}

	public static List<ScheduleJobVO> convertList(List<ScheduleJob> source) {
		if (CollUtil.isEmpty(source)) {
			return ListUtil.empty();
		}
		List<ScheduleJobVO> list = new ArrayList<>();
		for (ScheduleJob entity : source) {
			list.add(new ScheduleJobVO(entity));
		}
		return list;
	}

	public ScheduleJob reconvert() {
		ScheduleJob entity = new ScheduleJob();
		entity.setId(this.id);
		entity.setJobName(this.jobName);
		entity.setJobGroup(this.jobGroup);
		entity.setInvokeTarget(this.invokeTarget);
		entity.setCronExpression(this.cronExpression);
		entity.setMisfirePolicy(this.misfirePolicy);
		entity.setConcurrent(this.concurrent);
		entity.setVersion(this.version);
		entity.setCreateTime(this.createTime);
		entity.setCreateBy(this.createBy);
		entity.setUpdateTime(this.updateTime);
		entity.setUpdateBy(this.updateBy);
		entity.setStatus(this.status);
		entity.setIsDeleted(this.isDeleted);
		entity.setRemark(this.remark);
		return entity;
	}

}