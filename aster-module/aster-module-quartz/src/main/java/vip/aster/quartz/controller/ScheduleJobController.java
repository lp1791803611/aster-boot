package vip.aster.quartz.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.constant.Constants;
import vip.aster.common.exception.BusinessException;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.quartz.entity.ScheduleJob;
import vip.aster.quartz.query.ScheduleJobQuery;
import vip.aster.quartz.service.ScheduleJobService;
import vip.aster.quartz.utils.CronUtils;
import vip.aster.quartz.utils.ScheduleUtils;
import vip.aster.quartz.vo.ScheduleJobVO;

import java.util.List;

/**
 * 定时任务调度
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 10:24:20
 */
@Tag(name = "定时任务调度")
@RestController
@RequestMapping("/quartz/job")
@AllArgsConstructor
public class ScheduleJobController {
    private final ScheduleJobService scheduleJobService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('quartz:job:list')")
    public ResultInfo<PageInfo<ScheduleJobVO>> page(@ParameterObject @Valid ScheduleJobQuery query) {
        PageInfo<ScheduleJobVO> page = scheduleJobService.pageList(query);

        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('quartz:job:info')")
    public ResultInfo<ScheduleJobVO> get(@PathVariable("id") String id) {
        ScheduleJob entity = scheduleJobService.getById(id);

        return ResultInfo.success(new ScheduleJobVO(entity));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('quartz:job:add','quartz:job:edit')")
    public ResultInfo<String> save(@RequestBody ScheduleJobVO vo) throws SchedulerException {
        if (!CronUtils.isValid(vo.getCronExpression())) {
            return ResultInfo.failed("操作失败，Cron表达式不正确");
        }
        // 检查Bean的合法性
        checkBean(vo);

        scheduleJobService.save(vo);

        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('quartz:job:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList) throws SchedulerException {
        scheduleJobService.deleteJobByIds(idList);

        return ResultInfo.success();
    }

    @PostMapping("/run")
    @Operation(summary = "立即执行")
    @Log(type = BusinessTypeEnum.OTHER)
    @PreAuthorize("hasAuthority('quartz:job:run')")
    public ResultInfo<String> run(@RequestBody ScheduleJobVO vo) throws SchedulerException {
        scheduleJobService.run(vo);

        return ResultInfo.success();
    }

    @PostMapping("/change-status")
    @Operation(summary = "修改状态")
    @Log(type = BusinessTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('quartz:job:edit')")
    public ResultInfo<String> changeStatus(@RequestBody ScheduleJobVO vo) throws SchedulerException {
        scheduleJobService.changeStatus(vo);

        return ResultInfo.success();
    }

    private void checkBean(ScheduleJobVO job) {
        if (!CronUtils.isValid(job.getCronExpression())) {
            throw new BusinessException("新增任务'" + job.getJobName() + "'失败，Cron表达式不正确");
        } else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_RMI)) {
            throw new BusinessException("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'rmi'调用");
        } else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(), new String[]{Constants.LOOKUP_LDAP, Constants.LOOKUP_LDAPS})) {
            throw new BusinessException("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'ldap(s)'调用");
        } else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(), new String[]{Constants.HTTP, Constants.HTTPS})) {
            throw new BusinessException("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)'调用");
        } else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(), Constants.JOB_ERROR_STR)) {
            throw new BusinessException("新增任务'" + job.getJobName() + "'失败，目标字符串存在违规");
        } else if (!ScheduleUtils.whiteList(job.getInvokeTarget())) {
            throw new BusinessException("新增任务'" + job.getJobName() + "'失败，目标字符串不在白名单内");
        }
    }
}