package vip.aster.quartz.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.quartz.entity.ScheduleJobLog;
import vip.aster.quartz.query.ScheduleJobLogQuery;
import vip.aster.quartz.service.ScheduleJobLogService;
import vip.aster.quartz.vo.ScheduleJobLogVO;

import java.util.List;

/**
 * 定时任务调度日志
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 11:10:07
 */
@Tag(name = "定时任务调度日志")
@RestController
@RequestMapping("/quartz/log")
@AllArgsConstructor
public class ScheduleJobLogController {
    private final ScheduleJobLogService scheduleJobLogService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('quartz:log:list')")
    public ResultInfo<PageInfo<ScheduleJobLogVO>> page(@ParameterObject @Valid ScheduleJobLogQuery query){
        PageInfo<ScheduleJobLogVO> page = scheduleJobLogService.pageList(query);

        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情") 
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('quartz:log:info')")
    public ResultInfo<ScheduleJobLogVO> get(@PathVariable("id") String id){
        ScheduleJobLog entity = scheduleJobLogService.getById(id);

        return ResultInfo.success(new ScheduleJobLogVO(entity));
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('quartz:log:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList){
        scheduleJobLogService.removeBatchByIds(idList);

        return ResultInfo.success();
    }

    @PostMapping("/clear")
    @Operation(summary = "清空")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('quartz:log:clear')")
    public ResultInfo<String> clear(){
        scheduleJobLogService.clear();

        return ResultInfo.success();
    }
}