package vip.aster.quartz.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.common.utils.PageInfo;
import vip.aster.quartz.entity.ScheduleJobLog;
import vip.aster.quartz.mapper.ScheduleJobLogMapper;
import vip.aster.quartz.query.ScheduleJobLogQuery;
import vip.aster.quartz.service.ScheduleJobLogService;
import vip.aster.quartz.vo.ScheduleJobLogVO;

/**
 * <P>
 * 定时任务调度日志 服务实现类
 * </P>
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 11:10:07
 */
@Service
@AllArgsConstructor
public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogMapper, ScheduleJobLog> implements ScheduleJobLogService {
    private ScheduleJobLogMapper scheduleJobLogMapper;

    @Override
    public PageInfo<ScheduleJobLogVO> pageList(ScheduleJobLogQuery query) {
        Page<ScheduleJobLog> page = new Page<>(query.getPageNum(), query.getPageSize());
        Page<ScheduleJobLog> pageList = scheduleJobLogMapper.selectPage(page, getWrapper(query));
        return new PageInfo<>(ScheduleJobLogVO.convertList(pageList.getRecords()), pageList.getTotal());
    }

    @Override
    public void save(ScheduleJobLogVO vo) {
        ScheduleJobLog entity = vo.reconvert();

        if (StrUtil.isNotBlank(entity.getId())) {
            scheduleJobLogMapper.updateById(entity);
        } else {
            scheduleJobLogMapper.insert(entity);
        }
    }

    @Override
    public void clear() {
        scheduleJobLogMapper.clear();
    }

    private LambdaQueryWrapper<ScheduleJobLog> getWrapper(ScheduleJobLogQuery query){
        LambdaQueryWrapper<ScheduleJobLog> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StrUtil.isNotBlank(query.getJobId()), ScheduleJobLog::getJobId, query.getJobId());
        wrapper.like(StrUtil.isNotBlank(query.getJobName()), ScheduleJobLog::getJobName, query.getJobName());
        wrapper.eq(StrUtil.isNotBlank(query.getJobGroup()), ScheduleJobLog::getJobGroup, query.getJobGroup());
        return wrapper;
    }

}