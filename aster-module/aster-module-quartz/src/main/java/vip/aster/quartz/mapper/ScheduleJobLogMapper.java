package vip.aster.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.quartz.entity.ScheduleJobLog;

/**
 * 定时任务调度日志 Mapper 接口
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 11:10:07
 */
public interface ScheduleJobLogMapper extends BaseMapper<ScheduleJobLog> {

    /**
     * 清空
     */
    void clear();
}