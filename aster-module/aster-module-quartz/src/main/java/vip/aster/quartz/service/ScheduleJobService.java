package vip.aster.quartz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.quartz.SchedulerException;
import vip.aster.common.utils.PageInfo;
import vip.aster.quartz.entity.ScheduleJob;
import vip.aster.quartz.query.ScheduleJobQuery;
import vip.aster.quartz.vo.ScheduleJobVO;

import java.util.List;

/**
 * <P>
 * 定时任务调度 服务接口
 * </P>
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 10:24:20
 */
public interface ScheduleJobService extends IService<ScheduleJob> {

    /**
     * 分页
     * @param query 查询条件
     * @return 分页结果
     */
    PageInfo<ScheduleJobVO> pageList(ScheduleJobQuery query);

    /**
     * 保存
     * @param vo 定时任务
     */
    void save(ScheduleJobVO vo) throws SchedulerException;

    /**
     * 执行任务
     * @param vo 定时任务
     */
    void run(ScheduleJobVO vo) throws SchedulerException;

    /**
     * 修改任务状态
     * @param vo 定时任务
     */
    void changeStatus(ScheduleJobVO vo) throws SchedulerException;

    /**
     * 暂停任务
     *
     * @param job 调度信息
     */
    public void pauseJob(ScheduleJob job) throws SchedulerException;

    /**
     * 恢复任务
     *
     * @param job 调度信息
     */
    public void resumeJob(ScheduleJob job) throws SchedulerException;

    /**
     * 删除任务
     * @param jobIds 任务id
     */
    void deleteJobByIds(List<String> jobIds) throws SchedulerException;
}