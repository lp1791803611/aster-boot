package vip.aster.quartz.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 定时任务调度
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 10:24:20
 */

@Data
@TableName("schedule_job")
public class ScheduleJob implements Serializable {

	@Serial
	private static final long serialVersionUID = 8637842071913399425L;

	/**
	* 任务ID
	*/
	@TableId("id")
	private String id;
	/**
	* 任务名称
	*/
	@TableField("job_name")
	private String jobName;
	/**
	* 任务组名
	*/
	@TableField("job_group")
	private String jobGroup;
	/**
	* 调用目标字符串
	*/
	@TableField("invoke_target")
	private String invokeTarget;
	/**
	* cron执行表达式
	*/
	@TableField("cron_expression")
	private String cronExpression;
	/**
	* 计划执行错误策略（1立即执行 2执行一次 3放弃执行）
	*/
	@TableField("misfire_policy")
	private String misfirePolicy;
	/**
	* 是否并发执行（0允许 1禁止）
	*/
	@TableField("concurrent")
	private String concurrent;
	/**
	* 版本号
	*/
	@TableField("version")
	private Integer version;
	/**
	* 创建时间
	*/
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime createTime;
	/**
	* 创建者
	*/
	@TableField(value = "create_by", fill = FieldFill.INSERT)
	private String createBy;
	/**
	* 更新时间
	*/
	@TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime updateTime;
	/**
	* 修改者
	*/
	@TableField(value = "update_by", fill = FieldFill.INSERT_UPDATE)
	private String updateBy;
	/**
	* 启用状态
	*/
	@TableField("status")
	private String status;
	/**
	* 删除状态
	*/
	@TableField("is_deleted")
	@TableLogic
	private String isDeleted;
	/**
	* 备注信息
	*/
	@TableField("remark")
	private String remark;
}