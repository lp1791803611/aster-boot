package vip.aster.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.quartz.entity.ScheduleJob;

/**
 * 定时任务调度 Mapper 接口
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-06 10:24:20
 */
public interface ScheduleJobMapper extends BaseMapper<ScheduleJob> {
	
}