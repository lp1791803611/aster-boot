package vip.aster.quartz.utils;

import org.quartz.JobExecutionContext;
import vip.aster.quartz.entity.ScheduleJob;

/**
 * 定时任务处理（允许并发执行）
 *
 * @author Aster
 * @since 2024/3/6 15:06
 */
public class QuartzJobExecution extends AbstractQuartzJob {
    @Override
    protected void doExecute(JobExecutionContext context, ScheduleJob scheduleJob) throws Exception {
        JobInvokeUtil.invokeMethod(scheduleJob);
    }
}
