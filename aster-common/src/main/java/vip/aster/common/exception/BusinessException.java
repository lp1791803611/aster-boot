package vip.aster.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * <p> 全局异常处理 </p>
 *
 * @author Aster
 * @since 2023/11/13 17:58
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BusinessException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 7974547933678847748L;
    private Integer errorCode;
    private String message;

    public BusinessException() {}

    public BusinessException(String message) {
        this.message = message;
    }

    public BusinessException(Integer errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public BusinessException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

}
