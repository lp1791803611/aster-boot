package vip.aster.common.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Setter;

/**
 * 分页查询
 *
 * @author Aster
 * @since 2023/12/4 15:08
 */
@Setter
public class Query {

    @Schema(description = "当前页码")
    Integer pageNum;

    @Schema(description = "每页条数")
    Integer pageSize;

    public Integer getPageNum() {
        return pageNum == null || pageNum < 1 ? 1 : pageNum;
    }

    public Integer getPageSize() {
        return pageSize == null || pageSize < 1 ? 10 : pageSize;
    }
}
