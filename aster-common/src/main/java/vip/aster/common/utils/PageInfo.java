package vip.aster.common.utils;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 *
 * @author Aster
 * @since 2023/12/4 14:49
 */
@Data
@Schema(description = "分页数据")
public class PageInfo <T> implements Serializable {
    @Serial
    private static final long serialVersionUID = 558394332185364096L;

    @Schema(description = "总记录数")
    private long total;

    @Schema(description = "列表数据")
    private List<T> list;

    public PageInfo() {
        super();
    }

    public PageInfo(List<T> list, long total) {
        this.list = list;
        this.total = total;
    }
}
