package vip.aster.common.utils;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p> ResultInfo </p>
 *
 * @author Aster
 * @since 2023/11/13 18:04
 */
@Data
@Schema(description = "通用返回对象")
public class ResultInfo<T> implements Serializable {
    @Serial
    private static final long serialVersionUID = 3189441413739383849L;

    /**
     * 返回代码
     */
    @Schema(description = "返回代码")
    private Integer code = 200;

    /**
     * 返回数据对象 data
     */
    @Schema(description = "返回数据对象")
    private T data;

    /**
     * 返回处理消息
     */
    @Schema(description = "返回处理消息")
    private String message = "操作成功！";

    /**
     * 时间戳
     */
    @Schema(description = "时间戳")
    private long timestamp = System.currentTimeMillis();

    public ResultInfo() {

    }

    /**
     * 成功返回结果
     *
     * @author Aster
     * @since 2023/11/23 16:05
     */
    public static ResultInfo<String> success() {
        return success(ResultCode.SUCCESS.getMessage(), ResultCode.SUCCESS.getMessage());
    }

    /**
     * 成功返回结果
     *
     * @param data 返回的数据
     * @author Aster
     * @since 2023/11/23 0023 16:06
     */
    public static <T> ResultInfo<T> success(T data) {
        if (data instanceof String) {
            return success(String.valueOf(data), data);
        }
        return success(ResultCode.SUCCESS.getMessage(), data);
    }

    /**
     * 成功返回结果
     *
     * @param msg  提示信息
     * @param data 返回的数据
     * @author Aster
     * @since 2023/11/23 16:09
     */
    public static <T> ResultInfo<T> success(String msg, T data) {
        ResultInfo<T> r = new ResultInfo<T>();
        r.setCode(ResultCode.SUCCESS.getCode());
        r.setMessage(msg);
        r.setData(data);
        return r;
    }

    /**
     * 失败返回结果
     *
     * @author Aster
     * @since 2023/11/23 16:10
     */
    public static <T> ResultInfo<T> failed() {
        return failed(ResultCode.INTERNAL_SERVER_ERROR.getMessage());
    }

    /**
     * 失败返回结果
     *
     * @param message 错误信息
     * @author Aster
     * @since 2023/11/23 16:10
     */
    public static <T> ResultInfo<T> failed(String message) {
        return failed(ResultCode.INTERNAL_SERVER_ERROR.getCode(), message);
    }

    /**
     * 失败返回结果
     *
     * @param code    错误码
     * @param message 错误信息
     * @author Aster
     * @since 2023/11/23 16:10
     */
    public static <T> ResultInfo<T> failed(int code, String message) {
        return failed(code, message, null);
    }

    /**
     * 失败返回结果
     *
     * @param code    错误码
     * @param message 错误信息
     * @param data T
     * @author Aster
     * @since 2023/11/23 16:10
     */
    public static <T> ResultInfo<T> failed(int code, String message, T data) {
        ResultInfo<T> r = new ResultInfo<>();
        r.setCode(code);
        r.setMessage(message);
        r.setData(data);
        return r;
    }

    /**
     * 参数验证失败返回结果
     *
     * @author Aster
     * @since 2023/11/23 16:12
     */
    public static <T> ResultInfo<T> validateFailed() {
        return validateFailed(ResultCode.VALIDATE_FAILED.getMessage());
    }

    /**
     * 参数验证失败返回结果
     *
     * @param message 错误信息
     * @author Aster
     * @since 2023/11/23 16:12
     */
    public static <T> ResultInfo<T> validateFailed(String message) {
        return failed(ResultCode.VALIDATE_FAILED.getCode(), message);
    }

    /**
     * 未登录返回结果
     *
     * @author Aster
     * @since 2023/11/23 16:12
     */
    public static <T> ResultInfo<T> unauthorized() {
        return failed(ResultCode.UNAUTHORIZED.getCode(), ResultCode.UNAUTHORIZED.getMessage());
    }

    /**
     * 未登录返回结果
     *
     * @param data T
     * @author Aster
     * @since 2023/11/23 16:12
     */
    public static <T> ResultInfo<T> unauthorized(T data) {
        return failed(ResultCode.UNAUTHORIZED.getCode(), ResultCode.UNAUTHORIZED.getMessage(), data);
    }

    /**
     * 未授权返回结果
     *
     * @author Aster
     * @since 2023/11/23 16:12
     */
    public static <T> ResultInfo<T> forbidden() {
        return failed(ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getMessage());
    }

    /**
     * 未授权返回结果
     *
     * @author Aster
     * @since 2023/11/23 16:12
     */
    public static <T> ResultInfo<T> forbidden(T data) {
        return failed(ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getMessage(), data);
    }

}
