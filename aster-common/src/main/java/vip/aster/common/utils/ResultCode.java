package vip.aster.common.utils;

/**
 * <p> ResultCode </p>
 *
 * @author Aster
 * @since 2023/11/16 17:22
 */
public enum ResultCode {
    /**
     * 200-操作成功
     */
    SUCCESS(200, "操作成功"),
    /**
     * 400-参数异常、缺失
     */
    BAD_REQUEST(400, "参数异常、缺失"),
    /**
     * 401-还未授权，不能访问
     */
    UNAUTHORIZED(401, "还未授权，不能访问"),
    /**
     * 403-没有权限，禁止访问
     */
    FORBIDDEN(403, "没有权限，禁止访问"),
    /**
     * 404-参数检验失败
     */
    VALIDATE_FAILED(404, "参数检验失败"),
    /**
     * 500-服务器异常，请稍后再试
     */
    INTERNAL_SERVER_ERROR(500, "服务器异常，请稍后再试");

    private Integer code;
    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
