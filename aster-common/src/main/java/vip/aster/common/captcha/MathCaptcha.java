package vip.aster.common.captcha;

import com.wf.captcha.ArithmeticCaptcha;

import java.awt.*;

/**
 * 图片验证码-数字计算
 *
 * @author Aster
 * @since 2023/12/4 10:25
 */
public class MathCaptcha extends ArithmeticCaptcha {
    public MathCaptcha() {
        super();
    }

    public MathCaptcha(int width, int height) {
        this();
        this.setWidth(width);
        this.setHeight(height);
    }

    public MathCaptcha(int width, int height, int len) {
        this(width, height);
        this.setLen(len);
    }

    public MathCaptcha(int width, int height, int len, Font font) {
        this(width, height, len);
        this.setFont(font);
    }

    @Override
    public void checkAlpha() {
        // 生成验证码直到非负为止
        while(chars == null || Integer.parseInt(chars) < 0) {
            alphas();
        }
    }
}
