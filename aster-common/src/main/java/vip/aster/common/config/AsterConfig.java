package vip.aster.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p> 项目配置 </p>
 *
 * @author Aster
 * @since 2023/11/20 10:34
 */
@Data
@Component
@ConfigurationProperties(prefix = "aster")
public class AsterConfig {
    /** 项目名称 */
    private String name;
    /** 项目版本 */
    private String version;
    /** 演示URL */
    private String demoUrl;
    /** 是否开启演示环境 */
    private Boolean demoEnabled;
    /** 验证码类型 */
    private String captchaType;
}
