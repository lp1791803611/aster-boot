package vip.aster.common.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import jakarta.annotation.Resource;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p> SwaggerConfig </p>
 *
 * @author Aster
 * @since 2023/11/20 15:14
 */
@Configuration
public class SwaggerConfig {
    @Resource
    private AsterConfig asterConfig;

    @Bean
    public GroupedOpenApi userApi() {
        String[] paths = {"/**"};
        String[] packagedToMatch = {"vip.aster"};
        return GroupedOpenApi.builder().group("AsterBoot")
                .pathsToMatch(paths)
                .packagesToScan(packagedToMatch).build();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        Contact contact = new Contact();
        contact.setName(asterConfig.getName());

        return new OpenAPI().info(new Info()
                .title(asterConfig.getName())
                .description(asterConfig.getName())
                .contact(contact)
                .version(asterConfig.getVersion())
                .termsOfService(asterConfig.getDemoUrl())
                .license(new License().name("MIT")
                        .url(asterConfig.getDemoUrl())));
    }
}
