package vip.aster.common.annotation;

import vip.aster.common.constant.CacheConstants;
import vip.aster.common.constant.Constants;
import vip.aster.common.constant.enums.LimitType;

import java.lang.annotation.*;

/**
 * 限流注解
 *
 * @author Aster
 * @since 2024/2/1 15:05
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimiter {
    /**
     * 限流key
     */
    public String key() default CacheConstants.RATE_LIMIT_KEY;

    /**
     * 限流时间,单位秒
     */
    public long time() default Constants.RATE_LIMIT_EXPIRATION;

    /**
     * 限流次数
     */
    public long count() default 100;

    /**
     * 限流类型
     */
    public LimitType limitType() default LimitType.DEFAULT;
}
