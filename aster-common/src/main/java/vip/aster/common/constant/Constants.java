package vip.aster.common.constant;

/**
 * <p> 常量 </p>
 *
 * @author Aster
 * @since 2023/11/20 17:14
 */
public class Constants {

    public static final String SUCCESS = "0";

    public static final String FAIL = "1";

    public static final String ROOT_NODE = "0";

    public static final String PAGE_PARAM = "page";

    public static final String DEFAULT_PASSWORD = "123456";

    /**
     * 令牌自定义标识
     */
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PARAMETER = "access_token";

    /**
     * token有效期（小时）
     */
    public static final long TOKEN_EXPIRATION = 2L;

    /**
     * 验证码有效期（分钟）
     */
    public static final long CAPTCHA_EXPIRATION = 2L;

    /**
     * 锁定用户时间（分钟）
     */
    public static final long RETRY_EXPIRATION = 30L;

    /**
     * 限流时间（秒）
     */
    public static final long RATE_LIMIT_EXPIRATION = 180L;

    /**
     * 防重复提交（秒）
     */
    public static final long REPEAT_EXPIRATION = 5L;

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";
    /**
     * 上传文件映射路径
     */
    public static final String RESOURCE_UPLOAD = "/upload";
    /**
     * 默认上传文件的后缀
     */
    public static final String[] DEFAULT_UPLOAD_EXTENSION = {
            // 图片
            "bmp", "gif", "jpg", "jpeg", "png",
            // word excel powerpoint
            "doc", "docx", "xls", "xlsx", "ppt", "pptx", "html", "htm", "txt",
            // 压缩文件
            "rar", "zip", "gz", "bz2",
            // 视频格式
            "mp4", "avi", "rmvb",
            // pdf
            "pdf",
            // csv
            "csv"
    };

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * RMI 远程方法调用
     */
    public static final String LOOKUP_RMI = "rmi:";

    /**
     * LDAP 远程方法调用
     */
    public static final String LOOKUP_LDAP = "ldap:";

    /**
     * LDAPS 远程方法调用
     */
    public static final String LOOKUP_LDAPS = "ldaps:";

    /**
     * 自动识别json对象白名单配置（仅允许解析的包名，范围越小越安全）
     */
    public static final String[] JSON_WHITELIST_STR = {"org.springframework", "vip.aster"};

    /**
     * 定时任务白名单配置（仅允许访问的包名，如其他需要可以自行添加）
     */
    public static final String[] JOB_WHITELIST_STR = {"vip.aster"};

    /**
     * 定时任务违规的字符
     */
    public static final String[] JOB_ERROR_STR = {"java.net.URL", "javax.naming.InitialContext", "org.yaml.snakeyaml",
            "org.springframework", "org.apache"};

    /**
     * Giteeh获取code
     * <p>clientId: 应用ID</p>
     * <p>redirectUrl: 回调地址</p>
     * <p>state: 状态码</p>
     */
    public static final String GITEE_CODE_URL = "https://gitee.com/oauth/authorize?response_type=code&scope=user_info&client_id={clientId}&state={state}&redirect_uri={redirectUrl}";

    /**
     * Giteeh获取token
     * <p>clientId: 应用ID</p>
     * <p>clientSecret: 应用密钥</p>
     * <p>code: 授权码</p>
     * <p>redirectUrl: 回调地址</p>
     */
    public static final String GITEE_AUTH_URL = "https://gitee.com/oauth/token?grant_type=authorization_code&client_id={clientId}&client_secret={clientSecret}&code={code}&redirect_uri={redirectUrl}";

    /**
     * Gitee检查授权用户是否 star 了一个仓库
     * <p>owner: 码云账号的名称</p>
     * <p>repo: 仓库名</p>
     */
    public static final String GITEE_STARRED_URL = "https://gitee.com/api/v5/user/starred/{owner}/{repo}?access_token={accessToken}";
}
