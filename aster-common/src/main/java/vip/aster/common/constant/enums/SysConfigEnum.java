package vip.aster.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统配置
 *
 * @author Aster
 * @since 2023/11/30 16:10
 */
@Getter
@AllArgsConstructor
public enum SysConfigEnum {
    /**
     * 注册
     */
    REGISTER("sys-register"),
    /**
     * 验证码
     */
    CAPTCHA("sys-captcha"),
    /**
     * 初始密码
     */
    PASSWORD("sys-password")
    ;
    private final String value;
}
