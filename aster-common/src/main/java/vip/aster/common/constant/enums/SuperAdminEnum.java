package vip.aster.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 超级管理员
 *
 * @author Aster
 * @since 2023/12/1 11:28
 */
@Getter
@AllArgsConstructor
public enum SuperAdminEnum {
    /**
     * 是
     */
    YES("0", "是"),
    /**
     * 否
     */
    NO("1", "否");

    private final String value;
    private final String name;

    public static String getNameByValue(String value) {
        for (SuperAdminEnum s : SuperAdminEnum.values()) {
            if (s.getValue().equals(value)) {
                return s.getName();
            }
        }
        return "";
    }

    public static String getValueByName(String name) {
        for (SuperAdminEnum s : SuperAdminEnum.values()) {
            if (Objects.equals(s.getName(), name)) {
                return s.getValue();
            }
        }
        return null;
    }
}
