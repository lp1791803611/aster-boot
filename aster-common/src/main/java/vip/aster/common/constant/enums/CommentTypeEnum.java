package vip.aster.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * CommentTypeEnum
 *
 * @author Aster
 * @since 2024/1/19 16:52
 */
@Getter
@AllArgsConstructor
public enum CommentTypeEnum {
    LIKE("like"),
    DISLIKE("dislike");

    private final String value;
}
