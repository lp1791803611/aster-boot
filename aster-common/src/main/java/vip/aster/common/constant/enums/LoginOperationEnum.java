package vip.aster.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * LoginOperationEnum
 *
 * @author Aster
 * @since 2023/11/30 17:10
 */
@Getter
@AllArgsConstructor
public enum LoginOperationEnum {
    /**
     * 登录成功
     */
    LOGIN_SUCCESS("0"),
    /**
     * 退出成功
     */
    LOGOUT_SUCCESS("1"),
    /**
     * 验证码错误
     */
    CAPTCHA_FAIL("2"),
    /**
     * 账号密码错误
     */
    ACCOUNT_FAIL("3");

    private final String value;
}
