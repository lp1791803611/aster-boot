package vip.aster.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 验证码类型
 *
 * @author Aster
 * @since 2023/11/30 11:09
 */
@Getter
@AllArgsConstructor
public enum CaptchaTypeEnum {
    CHAR("char"),
    MATH("math"),
    CHINESE("chinese"),
    GIF("gif"),
    CHINESE_GIF("chinese_gif"),
    ;

    private final String value;
}
