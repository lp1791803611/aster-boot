package vip.aster.common.constant.enums;

import lombok.Data;
import lombok.Getter;

/**
 * MenuTypeEnum
 *
 * @author Aster
 * @since 2024/1/19 15:15
 */
@Getter
public enum MenuTypeEnum {
    /**
     * 目录
     */
    DIRECTORY("0", "目录"),
    /**
     * 菜单
     */
    MENU("1", "菜单"),
    /**
     * 按钮
     */
    BUTTON("2", "按钮");


    private final String value;
    private final String name;

    MenuTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String getCode(String value) {
        MenuTypeEnum[] enums = MenuTypeEnum.values();
        for (MenuTypeEnum bEnum : enums) {
            if (bEnum.getName().equals(value)) {
                return bEnum.getValue();
            }
        }
        return null;
    }

    public static String getMessage(String code) {
        MenuTypeEnum[] enums = MenuTypeEnum.values();
        for (MenuTypeEnum bEnum : enums) {
            if (bEnum.getValue().equals(code)) {
                return bEnum.getName();
            }
        }
        return null;
    }
}
