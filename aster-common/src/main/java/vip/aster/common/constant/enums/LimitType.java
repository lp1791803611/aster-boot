package vip.aster.common.constant.enums;

/**
 * 限流类型
 *
 * @author Aster
 * @since 2024/2/1 15:06
 */
public enum LimitType {
    /**
     * 默认策略全局限流
     */
    DEFAULT,

    /**
     * 根据请求者IP进行限流
     */
    IP
}
