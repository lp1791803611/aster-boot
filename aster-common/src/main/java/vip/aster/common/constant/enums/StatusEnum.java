package vip.aster.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通用状态枚举类
 *
 * @author Aster
 * @since 2023/11/30 15:48
 */
@Getter
public enum StatusEnum {
    /**
     * 启用
     */
    ENABLE("0", "启用"),
    /**
     * 冻结
     */
    DISABLE("1", "冻结");


    private final String value;
    private final String name;

    StatusEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public static String getCode(String value) {
        StatusEnum[] enums = StatusEnum.values();
        for (StatusEnum bEnum : enums) {
            if (bEnum.getName().equals(value)) {
                return bEnum.getValue();
            }
        }
        return null;
    }

    public static String getMessage(String code) {
        StatusEnum[] enums = StatusEnum.values();
        for (StatusEnum bEnum : enums) {
            if (bEnum.getValue().equals(code)) {
                return bEnum.getName();
            }
        }
        return null;
    }
}
