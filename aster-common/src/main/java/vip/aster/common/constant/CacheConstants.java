package vip.aster.common.constant;

/**
 * <p> 缓存常量  </p>
 *
 * @author Aster
 * @since 2023/11/21 17:20
 */
public class CacheConstants {
    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes";

    /**
     * 秘钥
     */
    public static final String SECRET_KEY = "secret_key";
    public static final String PUBLIC_KEY = "public";
    public static final String PRIVATE_KEY = "private";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit";

    /**
     * 登录账户密码错误次数 redis key
     */
    public static final String PWD_ERR_CNT_KEY = "pwd_err_cnt";

    /**
     * 日志 redis key
     */
    public static final String SYS_LOG_KEY = "sys_log";

    /**
     * 操作日志
     */
    public static final String OPERATE_LOG_KEY = "operate_log";

    /**
     * 租户ID
     */
    public static final String TENANT_KEY = "tenant";

    /**
     * gitee code
     */
    public static final String GITEE_STATE = "gitee_state";
}
