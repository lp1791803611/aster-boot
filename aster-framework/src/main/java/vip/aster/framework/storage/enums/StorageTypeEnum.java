package vip.aster.framework.storage.enums;

/**
 * 存储类型枚举
 *
 * @author Aster
 * @since 2024/1/22 16:17
 */
public enum StorageTypeEnum {
    /**
     * 本地
     */
    LOCAL,
    /**
     * 阿里云
     */
    ALIYUN,
    /**
     * 腾讯云
     */
    TENCENT,
    /**
     * 七牛云
     */
    QINIU,
    /**
     * 华为云
     */
    HUAWEI,
    /**
     * Minio
     */
    MINIO;
}
