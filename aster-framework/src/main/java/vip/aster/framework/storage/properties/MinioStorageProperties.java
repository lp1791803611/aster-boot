package vip.aster.framework.storage.properties;

import lombok.Data;

/**
 * Minio存储配置项
 *
 * @author Aster
 * @since 2024/1/31 15:22
 */
@Data
public class MinioStorageProperties {
    /**
     * Minio服务所在地址,例 ip:端口
     */
    private String endPoint;
    /**
     * 访问的key
     */
    private String accessKey;
    /**
     * 访问的秘钥
     */
    private String secretKey;
    /**
     * 存储桶名称
     */
    private String bucketName;
}
