package vip.aster.framework.storage.properties;

import lombok.Data;

/**
 * 本地存储配置项
 *
 * @author Aster
 * @since 2024/1/22 16:13
 */
@Data
public class LocalStorageProperties {
    /**
     * 本地存储路径
     */
    private String path;
    /**
     * 资源起始路径
     */
    private String url = "upload";
}
