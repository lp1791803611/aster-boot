package vip.aster.framework.license.core;

import de.schlichtherle.license.LicenseManager;
import de.schlichtherle.license.LicenseParam;
import vip.aster.framework.license.config.AsterLicenseManager;

/**
 * LicenseManager的单例
 *
 * @author Aster
 * @since 2024/12/30 16:35
 */
public class LicenseManagerHolder {

    private static volatile LicenseManager LICENSE_MANAGER;

    public static LicenseManager getInstance(LicenseParam param){
        if(LICENSE_MANAGER == null){
            synchronized (LicenseManagerHolder.class){
                if(LICENSE_MANAGER == null){
                    LICENSE_MANAGER = new AsterLicenseManager(param);
                }
            }
        }
        return LICENSE_MANAGER;
    }

}
