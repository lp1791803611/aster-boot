package vip.aster.framework.license.core;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import vip.aster.common.exception.BusinessException;

/**
 * 证书校验拦截器
 *
 * @author Aster
 * @since 2024/12/30 16:35
 */
@Slf4j
public class LicenseCheckInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, Object handler) throws Exception {
        LicenseVerify licenseVerify = new LicenseVerify();
        //校验证书是否有效
        boolean verifyResult = licenseVerify.verify();
        if (verifyResult) {
            return true;
        } else {
            throw new BusinessException("您的许可证无效或过期，请重新申请！");
        }
    }

}
