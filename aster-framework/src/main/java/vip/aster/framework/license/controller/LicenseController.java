package vip.aster.framework.license.controller;

import cn.hutool.core.util.StrUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.license.core.LicenseCheckModel;
import vip.aster.framework.license.serverinfo.AbstractServerInfos;
import vip.aster.framework.license.serverinfo.LinuxServerInfos;
import vip.aster.framework.license.serverinfo.WindowsServerInfos;

/**
 * LicenseController
 *
 * @author Aster
 * @since 2025/1/2 11:18
 */
@RestController
@RequestMapping("/license")
public class LicenseController {

    /**
     * 获取服务器硬件信息
     *
     * @param osName 操作系统类型，如果为空则自动判断
     */
    @GetMapping("/serverInfo")
    public ResultInfo<LicenseCheckModel> getServerInfos(@RequestParam(required = false) String osName) {
        // 操作系统类型
        if (StrUtil.isBlank(osName)) {
            osName = System.getProperty("os.name");
        }
        osName = osName.toLowerCase();

        AbstractServerInfos abstractServerInfos = null;

        // 根据不同操作系统类型选择不同的数据获取方法
        if (osName.startsWith("windows")) {
            abstractServerInfos = new WindowsServerInfos();
        } else if (osName.startsWith("linux")) {
            abstractServerInfos = new LinuxServerInfos();
        } else {//其他服务器类型
            abstractServerInfos = new LinuxServerInfos();
        }

        return ResultInfo.success(abstractServerInfos.getServerInfos());
    }
}
