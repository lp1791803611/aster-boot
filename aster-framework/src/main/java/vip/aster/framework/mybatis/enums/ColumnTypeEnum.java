package vip.aster.framework.mybatis.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 字段类型
 *
 * @author Aster
 * @since 2024/3/21 10:57
 */
@Getter
@AllArgsConstructor
public enum ColumnTypeEnum {

    DEPT("dept"),
    USER("user");

    private final String value;
}
