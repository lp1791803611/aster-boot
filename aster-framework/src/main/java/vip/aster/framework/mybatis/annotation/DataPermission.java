package vip.aster.framework.mybatis.annotation;

import java.lang.annotation.*;

/**
 * 数据权限组 注解
 *
 * @author Aster
 * @since 2024/3/21 9:56
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataPermission {
    DataColumn[] value() default {};
}
