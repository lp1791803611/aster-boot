package vip.aster.framework.mybatis.annotation;

import vip.aster.framework.mybatis.enums.ColumnTypeEnum;

import java.lang.annotation.*;

/**
 * 数据权限
 *
 * @author Aster
 * @since 2024/3/21 10:32
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataColumn {
    /**
     * 表别名
     */
    String alias() default "";

    /**
     * 表字段
     */
    String value() default "org_id";

    /**
     * 字段类型
     */
    ColumnTypeEnum type() default ColumnTypeEnum.DEPT;
}
