package vip.aster.framework.mybatis.handler;

import cn.hutool.core.date.DateUtil;
import org.apache.ibatis.type.LocalDateTimeTypeHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 解决Oracle的TIMESTAMP无法转换为LocalDateTime
 *
 * @author Aster
 * @since 2024/4/10 15:36
 */
public class OracleLocalDateTimeTypeHandler extends LocalDateTimeTypeHandler {

    @Override
    public LocalDateTime getResult(ResultSet rs, String columnName) throws SQLException {
        Object object = rs.getObject(columnName);
        if (object == null) {
            return null;
        }
        Date date = null;
        if (object instanceof oracle.sql.TIMESTAMP) {
            oracle.sql.TIMESTAMP obj = (oracle.sql.TIMESTAMP) object;
            date = new Date(obj.dateValue().getTime());
        }
        if (object instanceof oracle.sql.DATE) {
            oracle.sql.DATE obj = (oracle.sql.DATE) object;
            date = new Date(obj.dateValue().getTime());
        }
        return date != null ? DateUtil.toLocalDateTime(date) : null;
    }
}
