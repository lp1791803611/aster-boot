package vip.aster.framework.cache;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.expiry.ExpiryPolicy;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vip.aster.common.constant.CacheConstants;
import vip.aster.common.constant.Constants;
import vip.aster.framework.log.dto.OperateLogDTO;
import vip.aster.framework.security.entity.UserDetail;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * EhcacheConfig
 *
 * @author Aster
 * @since 2023/12/1 17:40
 */
@Configuration
@ConditionalOnProperty(prefix = "spring.cache", name = {"type"}, havingValue = "jcache", matchIfMissing = false)
public class EhcacheConfig {

    @Bean
    public JCacheManagerCustomizer jCacheManagerCustomizer() {
        return cm -> {
            // 用户token
            cm.createCache(CacheConstants.LOGIN_TOKEN_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, UserDetail.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)  // 堆缓存大小为 10000 个条目
                                            .offheap(10, MemoryUnit.MB) // 堆外缓存大小为 10 MB
                            )
                            .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofHours(Constants.TOKEN_EXPIRATION * 12))) // 设置缓存的生存时间为 24 小时
                            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(Constants.TOKEN_EXPIRATION)))      // 设置缓存的空闲时间为  2 小时
                            .build()));
            // 验证码
            cm.createCache(CacheConstants.CAPTCHA_CODE_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofMinutes(Constants.CAPTCHA_EXPIRATION)))
                            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(Constants.CAPTCHA_EXPIRATION + 1)))
                            .build()));
            // 秘钥
            cm.createCache(CacheConstants.SECRET_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicy.NO_EXPIRY) // 永久生效
                            .build()));
            // 参数
            cm.createCache(CacheConstants.SYS_CONFIG_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicy.NO_EXPIRY) // 永久生效
                            .build()));
            // 字典
            cm.createCache(CacheConstants.SYS_DICT_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, ArrayList.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicy.NO_EXPIRY) // 永久生效
                            .build()));
            // 防重复提交
            cm.createCache(CacheConstants.REPEAT_SUBMIT_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, HashMap.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(Constants.REPEAT_EXPIRATION)))
                            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofSeconds(Constants.REPEAT_EXPIRATION)))
                            .build()));
            // 限流
            cm.createCache(CacheConstants.RATE_LIMIT_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Object.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(Constants.RATE_LIMIT_EXPIRATION)))
                            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofSeconds(Constants.RATE_LIMIT_EXPIRATION)))
                            .build()));
            // 密码错误次数
            cm.createCache(CacheConstants.PWD_ERR_CNT_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Integer.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofHours(24)))
                            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(Constants.RETRY_EXPIRATION)))
                            .build()));
            // 日志
            cm.createCache(CacheConstants.SYS_LOG_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, OperateLogDTO.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)
                                            .offheap(10, MemoryUnit.MB)
                            )
                            .withExpiry(ExpiryPolicy.NO_EXPIRY)
                            .build()));
            // 租户ID
            cm.createCache(CacheConstants.TENANT_KEY, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)  // 堆缓存大小为 10000 个条目
                                            .offheap(10, MemoryUnit.MB) // 堆外缓存大小为 10 MB
                            )
                            .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofHours(Constants.TOKEN_EXPIRATION * 12))) // 设置缓存的生存时间为 24 小时
                            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(Constants.TOKEN_EXPIRATION)))      // 设置缓存的空闲时间为  2 小时
                            .build()));

            // Gitee Code
            cm.createCache(CacheConstants.GITEE_STATE, Eh107Configuration.fromEhcacheCacheConfiguration(
                    CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                    ResourcePoolsBuilder.newResourcePoolsBuilder()
                                            .heap(10000, EntryUnit.ENTRIES)  // 堆缓存大小为 10000 个条目
                                            .offheap(10, MemoryUnit.MB) // 堆外缓存大小为 10 MB
                            )
                            .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofHours(Constants.TOKEN_EXPIRATION * 12))) // 设置缓存的生存时间为 24 小时
                            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(Constants.TOKEN_EXPIRATION)))      // 设置缓存的空闲时间为  2 小时
                            .build()));
        };
    }

}
