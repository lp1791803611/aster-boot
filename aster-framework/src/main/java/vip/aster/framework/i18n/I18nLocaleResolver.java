package vip.aster.framework.i18n;

import cn.hutool.core.util.StrUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Locale;

/**
 * 获取请求头国际化信息
 *
 * @author Aster
 * @since 2024/2/18 14:38
 */
public class I18nLocaleResolver implements LocaleResolver {

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        // 从请求中获取国际化信息
        String language = request.getHeader("language");
        Locale locale = Locale.getDefault();
        if (StrUtil.isNotBlank(language)) {
            if (language.contains("-")) {
                String[] split = language.split("-");
                locale = new Locale(split[0], split[1]);
            }
            if (language.contains("_")) {
                String[] split = language.split("_");
                locale = new Locale(split[0], split[1]);
            }
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        // 往响应中设置国际化信息
    }
}
