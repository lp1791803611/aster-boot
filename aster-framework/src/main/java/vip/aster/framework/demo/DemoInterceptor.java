package vip.aster.framework.demo;

import cn.hutool.core.util.StrUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import vip.aster.framework.i18n.MessageUtils;

/**
 * 演示环境拦截器
 *
 * @author Aster
 * @since 2024/4/2 17:02
 */
@Component
public class DemoInterceptor implements HandlerInterceptor {
    private static final String[] INCLUDE_URL = {"save", "delete"};
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String path = request.getServletPath();
        if (StrUtil.isNotBlank(path)) {
            for (String url : INCLUDE_URL) {
                if (path.contains(url)) {
                    throw new DemoException(MessageUtils.message("auth.demo.error"));
                }
            }
        }
        return true;
    }
}
