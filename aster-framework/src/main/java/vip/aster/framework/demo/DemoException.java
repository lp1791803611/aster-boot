package vip.aster.framework.demo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 演示环境异常处理
 *
 * @author Aster
 * @since 2024/4/2 17:00
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DemoException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -4964693361147066720L;

    private String message;

    public DemoException() {}

    public DemoException(String message) {
        this.message = message;
    }
}
