package vip.aster.framework.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import vip.aster.common.utils.ResultInfo;

/**
 * 演示环境异常处理器
 *
 * @author Aster
 * @since 2024/4/2 17:51
 */
@Slf4j
@RestControllerAdvice
public class DemoExceptionHandler {

    @ExceptionHandler(value = DemoException.class)
    public ResultInfo<String> handleException(DemoException e) {
        return ResultInfo.failed(e.getMessage());
    }
}
