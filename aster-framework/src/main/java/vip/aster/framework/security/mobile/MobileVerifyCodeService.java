package vip.aster.framework.security.mobile;

/**
 * 手机短信登录，验证码效验
 *
 * @author Aster
 * @since 2023/11/24 17:37
 */
public interface MobileVerifyCodeService {
    /**
     * 验证码效验
     *
     * @param mobile 手机号
     * @param code   验证码
     * @return boolean
     * @author Aster
     * @since 2023/11/24 17:43
     */
    boolean verifyCode(String mobile, String code);
}
