package vip.aster.framework.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 登录用户信息
 *
 * @author Aster
 * @since 2023/11/24 16:56
 */
@Data
public class UserDetail implements UserDetails {

    @Serial
    private static final long serialVersionUID = 56832044853079116L;

    private String id;
    private String username;
    private String password;
    private String realName;
    private String avatar;
    private String gender;
    private String email;
    private String mobile;
    private String orgId;
    private String status;
    private String superAdmin;
    private String tenantId;

    /**
     * 数据权限范围
     * <p>
     * null：表示全部数据权限
     */
    private List<String> dataScopeList;
    /**
     * 帐户是否过期
     */
    private boolean accountNonExpired = true;
    /**
     * 帐户是否被锁定
     */
    private boolean accountNonLocked = true;
    /**
     * 密码是否过期
     */
    private boolean credentialsNonExpired = true;
    /**
     * 帐户是否可用
     */
    private boolean enabled = true;
    /**
     * 拥有权限集合
     */
    private Set<String> authoritySet;
    /**
     * 拥有角色集合
     */
    private Set<String> roleCodeSet;

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authoritySet.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
    }
}
