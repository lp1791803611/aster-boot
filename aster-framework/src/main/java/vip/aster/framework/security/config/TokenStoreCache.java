package vip.aster.framework.security.config;

import cn.hutool.core.collection.ListUtil;
import lombok.AllArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Component;
import vip.aster.common.constant.CacheConstants;
import vip.aster.common.constant.Constants;
import vip.aster.common.utils.CacheUtils;
import vip.aster.framework.security.entity.UserDetail;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 认证 Cache
 *
 * @author Aster
 * @since 2023/11/24 17:48
 */
@Component
@AllArgsConstructor
public class TokenStoreCache {
    private static final String TOKEN_KEY = CacheConstants.LOGIN_TOKEN_KEY;

    public void saveUser(String accessToken, UserDetail user) {
        CacheUtils.set(TOKEN_KEY, accessToken, user, Constants.TOKEN_EXPIRATION, TimeUnit.HOURS);
    }

    public UserDetail getUser(String accessToken) {
        return CacheUtils.get(TOKEN_KEY, accessToken, UserDetail.class);
    }

    public void deleteUser(String accessToken) {
        CacheUtils.delete(TOKEN_KEY, accessToken);
    }

    public List<String> getUserKeyList() {
        Set<String> sets = CacheUtils.keys(TOKEN_KEY);
        return ListUtil.toList(sets);
    }

}
