package vip.aster.framework.security.utils;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import jakarta.servlet.http.HttpServletRequest;
import vip.aster.common.constant.Constants;

/**
 * Token工具类
 *
 * @author Aster
 * @since 2023/11/24 17:30
 */
public class TokenUtils {
    private TokenUtils() {

    }

    /**
     * 生成 AccessToken
     */
    public static String generator() {
        return UUID.fastUUID().toString(true);
    }

    /**
     * 获取 AccessToken
     */
    public static String getAccessToken(HttpServletRequest request) {
        String accessToken = request.getHeader(Constants.TOKEN_HEADER);
        if (StrUtil.isBlank(accessToken)) {
            accessToken = request.getParameter(Constants.TOKEN_PARAMETER);
        }

        return accessToken;
    }
}
