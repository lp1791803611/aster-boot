package vip.aster.framework.security.entity;

import lombok.Data;
import org.springframework.security.core.context.SecurityContextHolder;
import vip.aster.common.constant.enums.SuperAdminEnum;

/**
 * SecurityUser
 *
 * @author Aster
 * @since 2023/11/24 16:54
 */
@Data
public class SecurityUser {
    
    /**
     * 获取用户信息
     * @return UserDetail
     * @since 2023/11/24 17:13
     * @author Aster
     */
    public static UserDetail getUser() {
        UserDetail user;
        try {
            user = (UserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            return null;
        }
        return user;
    }

    /**
     * 获取用户ID
     * @return Long
     * @since 2023/11/24 17:13
     * @author Aster
     */
    public static String getUserId() {
        UserDetail user = getUser();
        if (user == null) {
            return null;
        }
        return user.getId();
    }

    /**
     * 获取用户部门ID
     * @return String
     * @since 2023/11/24 17:18
     * @author Aster
     */
    public static String getOrgId() {
        UserDetail user = getUser();
        if (user == null) {
            return null;
        }
        return user.getOrgId();
    }

    /**
     * 获取租户ID
     */
    public static String getTenantId() {
        UserDetail user = getUser();
        if (user == null) {
            return null;
        }
        return user.getTenantId();
    }

    /**
     * 用户是否登录
     * @return true-登录
     */
    public static boolean isLogin() {
        return getUser() != null;
    }

    /**
     * 用户是否超级管理员
     * @return true-是
     */
    public static boolean isSuperAdmin() {
        return getUser() != null && SuperAdminEnum.YES.getValue().equals(getUser().getSuperAdmin());
    }
}
