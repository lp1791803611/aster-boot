package vip.aster.framework.security.mobile;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * 手机短信登录
 *
 * @author Aster
 * @since 2023/11/24 17:32
 */
public interface MobileUserDetailsService {

    /**
     * 通过手机号加载用户信息
     *
     * @param mobile 手机号
     * @return UserDetails
     * @author Aster
     * @since 2023/11/24 17:35
     */
    UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException;
}
