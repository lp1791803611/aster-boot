package vip.aster.framework.log.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * BusinessType
 *
 * @author Aster
 * @since 2023/11/27 10:59
 */
@Getter
@AllArgsConstructor
public enum BusinessTypeEnum {

    /**
     * 查询
     */
    SEARCH("1"),

    /**
     * 保存
     */
    SAVE("2"),

    /**
     * 修改
     */
    UPDATE("3"),

    /**
     * 删除
     */
    DELETE("4"),

    /**
     * 授权
     */
    GRANT("5"),

    /**
     * 导出
     */
    EXPORT("6"),

    /**
     * 导入
     */
    IMPORT("7"),

    /**
     * 强退
     */
    FORCE("8"),

    /**
     * 清空
     */
    CLEAN("9"),

    /**
     * 上传
     */
    UPLOAD("10"),

    /**
     * 下载
     */
    DOWNLOAD("11"),

    /**
     * 其它
     */
    OTHER("99");

    private final String value;
}
