package vip.aster.framework.log.annotation;

import vip.aster.framework.log.enums.BusinessTypeEnum;

import java.lang.annotation.*;

/**
 * Log
 *
 * @author Aster
 * @since 2023/11/27 10:53
 */
// Target-表示该注解用于什么地方
@Target({ ElementType.PARAMETER, ElementType.METHOD })
// Retention-表示在什么级别保存该注解信息
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /**
     * 模块名
     */
    public String module() default "";

    /**
     * 操作名
     */
    public String name() default "";

    /**
     * 操作类型
     */
    public BusinessTypeEnum type() default BusinessTypeEnum.OTHER;

    /**
     * 是否保存请求的参数
     */
    public boolean isSaveRequestData() default true;
}
