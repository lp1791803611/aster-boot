package vip.aster.framework.log.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import vip.aster.common.constant.CacheConstants;
import vip.aster.common.utils.CacheUtils;
import vip.aster.framework.log.dto.OperateLogDTO;

/**
 * LogService
 *
 * @author Aster
 * @since 2023/11/27 15:44
 */
@Service
public class OperateLogService {
    @Async
    public void saveLog(OperateLogDTO log) {
        CacheUtils.leftPush(CacheConstants.SYS_LOG_KEY, CacheConstants.OPERATE_LOG_KEY, log);
    }
}
