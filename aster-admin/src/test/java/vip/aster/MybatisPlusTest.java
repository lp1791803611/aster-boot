package vip.aster;

import jakarta.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import vip.aster.admin.AdminApplication;
import vip.aster.system.entity.SysPost;
import vip.aster.system.mapper.SysPostMapper;

/**
 * vip.aster.MybatisPlusTest
 *
 * @author Aster
 * @since 2024/2/7 16:02
 */
@SpringBootTest(classes = {AdminApplication.class})
@RunWith(SpringRunner.class)
public class MybatisPlusTest {
    @Resource
    private SysPostMapper sysPostMapper;

    @Test
    public void testLock() {
        //1.先通过要修改的数据Id，将当前数据查询出来
        //获得version字段值
        SysPost emp1 = sysPostMapper.selectById("1749726238597283841"); //version=1
        SysPost emp2 = sysPostMapper.selectById("1749726238597283841"); //version=1
        //2. 修改数据，假设emp2先修改完成,结果修改成功，version从1->2
        emp2.setRemark("test1");
        sysPostMapper.updateById(emp2);

        //结果修改不成功
        emp1.setRemark("test2");
        sysPostMapper.updateById(emp1);
    }
}
