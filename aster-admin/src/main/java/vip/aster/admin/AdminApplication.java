package vip.aster.admin;

import cn.hutool.core.util.ArrayUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * AdminApplication
 *
 * @author Aster
 * @since 2023/11/29 14:18
 */
@EnableCaching
@MapperScan(value = "vip.aster.*.mapper")
@SpringBootApplication(scanBasePackages = "vip.aster")
public class AdminApplication {

    public static void main(String[] args) {
        if (!isValidArguments(args)) {
            System.err.println("Invalid startup parameters: only one parameter containing 'jasypt.password' is allowed.");
            System.err.println("启动参数无效：只允许一个包含“jasypt.password”的参数.");
            System.exit(1);
        }
        SpringApplication.run(AdminApplication.class, args);

        System.out.println(getFormatLogString("                       aster-boot启动成功                              ", 34, 0));
        System.out.println(
                "   ____        .-'''-.  ,---------.      .-''-.   .-------.          \n" +
                        " .'  __ `.    / _     \\ \\          \\   .'_ _   \\  |  _ _   \\    \n" +
                        "/   '  \\  \\  " + getFormatLogString("(`' )", 35, 0) + "/`--'  `--.  ,---'  / " + getFormatLogString("( ` )", 35, 0) + "   ' | " + getFormatLogString("( ' )", 35, 0) + "  |       \n" +
                        "|___|  /  | " + getFormatLogString("(_ o _)", 35, 0) + ".        |   \\    . " + getFormatLogString("(_ o _)", 35, 0) + "  | |" + getFormatLogString("(_ o _)", 35, 0) + " /        \n" +
                        "   _.-`   |  " + getFormatLogString("(_,_)", 35, 0) + ". '.      :_ _:    |  " + getFormatLogString("(_,_)", 35, 0) + "___| | " + getFormatLogString("(_,_)", 35, 0) + ".' __       \n" +
                        ".'   _    | .---.  \\  :     " + getFormatLogString("(_I_)", 35, 1) + "    '  \\   .---. |  |\\ \\  |  |  \n" +
                        "|  " + getFormatLogString("_( )_", 35, 0) + "  | \\    `-'  |    " + getFormatLogString("(_(=)_)", 35, 1) + "    \\  `-'    / |  | \\ `'   /   \n" +
                        "\\ " + getFormatLogString("(_ o _)", 35, 0) + " /  \\       /      " + getFormatLogString("(_I_)", 35, 1) + "      \\       /  |  |  \\    /   \n" +
                        " '." + getFormatLogString("(_,_)", 35, 0) + ".'    `-...-'       '---'       `'-..-'   ''-'   `'-'        \n"
        );
    }

    /**
     * @param colour  颜色代号：背景颜色代号(41-46)；前景色代号(31-36)
     * @param type    样式代号：0无；1加粗；3斜体；4下划线
     * @param content 要打印的内容
     */
    private static String getFormatLogString(String content, int colour, int type) {
        boolean hasType = type != 1 && type != 3 && type != 4;
        if (hasType) {
            return String.format("\033[%dm%s\033[0m", colour, content);
        } else {
            return String.format("\033[%d;%dm%s\033[0m", colour, type, content);
        }
    }

    /**
     * 校验有效参数
     * @param args 参数
     * @return true 表示参数正确，false 表示参数错误
     */
    private static boolean isValidArguments(String[] args) {
        if (ArrayUtil.isEmpty(args) || args.length > 1) {
            return false;
        }

        return args[0].contains("jasypt.password");
    }

}
