package vip.aster.system.controller;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.i18n.MessageUtils;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysTenant;
import vip.aster.system.query.SysTenantQuery;
import vip.aster.system.service.SysTenantService;
import vip.aster.system.vo.SysTenantVO;
import vip.aster.tenant.utils.TenantUtils;

import java.util.List;

/**
 * 租户
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-25 14:46:59
 */
@Tag(name = "租户")
@RestController
@RequestMapping("/sys/tenant")
@AllArgsConstructor
public class SysTenantController {
    private final SysTenantService sysTenantService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:tenant:list')")
    public ResultInfo<PageInfo<SysTenantVO>> page(@ParameterObject @Valid SysTenantQuery query){
        PageInfo<SysTenantVO> page = sysTenantService.pageList(query);

        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情") 
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:tenant:info')")
    public ResultInfo<SysTenantVO> get(@PathVariable("id") String id){
        SysTenant entity = sysTenantService.getById(id);

        return ResultInfo.success(new SysTenantVO(entity));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:tenant:add','sys:tenant:edit')")
    public ResultInfo<String> save(@RequestBody SysTenantVO vo) throws Exception {
        sysTenantService.save(vo);
        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:tenant:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList){
        sysTenantService.removeBatchByIds(idList);

        return ResultInfo.success();
    }

    @GetMapping("/switch/{id}")
    @Operation(summary = "切换租户")
    @Log(type = BusinessTypeEnum.OTHER)
    @PreAuthorize("hasAuthority('sys:tenant:edit')")
    public ResultInfo<String> switchTenant(@PathVariable("id") String id){
        SysTenant entity = sysTenantService.getById(id);
        if (ObjectUtil.isNotEmpty(entity)) {
            TenantUtils.setDynamic(id);
        } else {
            return ResultInfo.failed(MessageUtils.message("tenant.not.exists"));
        }
        return ResultInfo.success();
    }
}