package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vip.aster.common.exception.BusinessException;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.i18n.MessageUtils;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.framework.storage.service.StorageService;
import vip.aster.system.vo.SysFileUploadVO;

/**
 * 附件管理
 *
 * @author Aster
 * @since 2024/1/22 15:53
 */
@Tag(name = "附件管理")
@RestController
@RequestMapping("/sys/file")
@AllArgsConstructor
public class SysFileController {
    private StorageService storageService;

    @PostMapping("/upload")
    @Operation(summary = "上传")
    @Log(type = BusinessTypeEnum.UPLOAD)
    public ResultInfo<SysFileUploadVO> upload(@RequestParam("file") MultipartFile file) throws Exception {
         if (file.isEmpty()) {
            throw new BusinessException(MessageUtils.message("storage.upload.empty"));
        }

        // 上传路径
        String path = storageService.getPath(file.getOriginalFilename());
        // 上传文件
        String url = storageService.upload(file.getBytes(), path);

        SysFileUploadVO vo = new SysFileUploadVO();
        vo.setUrl(url);
        vo.setSize(file.getSize());
        vo.setName(file.getOriginalFilename());
        vo.setPlatform(storageService.properties.getConfig().getType().name());

        return ResultInfo.success(vo);
    }
}
