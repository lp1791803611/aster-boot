package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysMenu;
import vip.aster.system.query.SysMenuQuery;
import vip.aster.system.service.SysMenuService;
import vip.aster.system.vo.SysMenuVO;

import java.util.List;

/**
 * SysMenuController
 *
 * @author Aster
 * @since 2023/12/6 16:57
 */
@Tag(name = "菜单管理")
@RestController
@RequestMapping("/sys/menu")
@AllArgsConstructor
public class SysMenuController {
    private SysMenuService sysMenuService;

    @GetMapping("/list")
    @Operation(summary = "列表")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:menu:list')")
    public ResultInfo<List<SysMenuVO>> page(@ParameterObject @Valid SysMenuQuery query) {
        List<SysMenuVO> page = sysMenuService.treeList(query);
        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:menu:info')")
    public ResultInfo<SysMenuVO> info(@PathVariable("id") String id) {
        SysMenu info = sysMenuService.getById(id);
        return ResultInfo.success(new SysMenuVO(info));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:menu:add','sys:menu:edit')")
    public ResultInfo<String> save(@RequestBody SysMenuVO entity) {
        sysMenuService.save(entity);
        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:menu:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList) {
        sysMenuService.removeBatchByIds(idList);
        return ResultInfo.success();
    }
}
