package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysConfig;
import vip.aster.system.query.SysConfigQuery;
import vip.aster.system.service.SysConfigService;
import vip.aster.system.vo.SysConfigVO;

import java.util.List;

/**
 * 系统配置
 *
 * @author Aster
 * @since 2023/12/4 14:17
 */
@Tag(name = "系统配置")
@RestController
@RequestMapping("/sys/config")
@AllArgsConstructor
public class SysConfigController {
    private SysConfigService sysConfigService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:config:list')")
    public ResultInfo<PageInfo<SysConfigVO>> page(@ParameterObject @Valid SysConfigQuery query) {
        PageInfo<SysConfigVO> page = sysConfigService.pageList(query);
        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:config:info')")
    public ResultInfo<SysConfigVO> info(@PathVariable("id") String id) {
        SysConfig entity = sysConfigService.getById(id);
        return ResultInfo.success(new SysConfigVO(entity));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:config:add','sys:config:edit')")
    public ResultInfo<String> save(@RequestBody SysConfigVO entity) {
        sysConfigService.saveConfig(entity);
        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:config:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList) {
        sysConfigService.removeBatchByIds(idList);
        return ResultInfo.success();
    }
}
