package vip.aster.system.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.aster.common.utils.IpUtils;
import vip.aster.common.utils.ResultInfo;

import java.text.MessageFormat;

/**
 * 天气
 *
 * @author Aster
 * @since 2024/4/1 14:19
 */
@Tag(name = "天气")
@RestController
@RequestMapping("/sys/weather")
public class WeatherController {

    private static final String DEFAULT_CITY_CODE = "110000";
    private static final String CITY_CODE_KEY = "adcode";
    private static final String CITY_CODE_VALUE = "[]";
    private static final String STATUS_KEY = "status";
    private static final String STATUS_SUCCESS = "1";
    private static final String STATUS_FAILED = "0";

    @Value("${amap.enable: false}")
    private boolean amapEnable;
    @Value("${amap.key: null}")
    private String amapKey;
    @Value("${amap.ipApi: null}")
    private String ipApi;
    @Value("${amap.weatherApi: null}")
    private String weatherApi;

    @GetMapping("")
    public ResultInfo<JSONObject> base(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        result.put("enable", amapEnable);
        if (!amapEnable) {
            return ResultInfo.success(result);
        }
        String cityCode = DEFAULT_CITY_CODE;
        String ipAddr = IpUtils.getIpAddr(request);
        if (!IpUtils.internalIp(ipAddr)) {
            String ipUrl = MessageFormat.format(ipApi, amapKey, ipAddr);
            String ipResult = HttpUtil.get(ipUrl);
            if (StrUtil.isNotBlank(ipResult)) {
                JSONObject jsonObject = JSON.parseObject(ipResult);
                String status = jsonObject.getString(STATUS_KEY);
                String adcode = jsonObject.getString(CITY_CODE_KEY);
                if (STATUS_SUCCESS.equals(status) && StrUtil.isNotBlank(adcode) && !CITY_CODE_VALUE.equals(adcode)) {
                    cityCode = adcode;
                }
            }
        }
        String weatherUrl = MessageFormat.format(weatherApi, amapKey, cityCode, "all");
        String weatherResult = HttpUtil.get(weatherUrl);
        if (StrUtil.isBlank(weatherResult)) {
            return ResultInfo.failed("获取天气结果为空");
        }
        JSONObject weatherJson = JSON.parseObject(weatherResult);
        String status = weatherJson.getString(STATUS_KEY);
        if (STATUS_FAILED.equals(status)) {
            return ResultInfo.failed("获取天气结果为空");
        }
        JSONArray weatherArray = weatherJson.getJSONArray("forecasts");
        if (weatherArray == null || weatherArray.isEmpty()) {
            return ResultInfo.failed("获取天气结果为空");
        }
        JSONObject weatherObj = weatherArray.getJSONObject(0);
        result.put("weather", weatherObj);
        return ResultInfo.success(result);
    }
}
