package vip.aster.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysPost;
import vip.aster.system.query.SysPostQuery;
import vip.aster.system.service.SysPostService;
import vip.aster.system.vo.SysPostVO;

import java.util.List;

/**
 * 岗位管理
 *
 * @author Aster
 * @since 2023/12/6 14:20
 */
@RestController
@Tag(name = "岗位管理")
@RequestMapping("/sys/post")
@AllArgsConstructor
public class SysPostController {
    private SysPostService sysPostService;

    @GetMapping("/list")
    @Operation(summary = "全部")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:post:list')")
    public ResultInfo<List<SysPostVO>> list() {
        LambdaQueryWrapper<SysPost> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(SysPost::getSort);
        List<SysPost> list = sysPostService.list(queryWrapper);
        return ResultInfo.success(SysPostVO.convertList(list));
    }

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:post:list')")
    public ResultInfo<PageInfo<SysPostVO>> page(@ParameterObject @Valid SysPostQuery query) {
        PageInfo<SysPostVO> page = sysPostService.pageList(query);
        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:post:info')")
    public ResultInfo<SysPostVO> info(@PathVariable("id") String id) {
        SysPost entity = sysPostService.getById(id);
        return ResultInfo.success(new SysPostVO(entity));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:post:add','sys:post:edit')")
    public ResultInfo<String> save(@RequestBody SysPostVO entity) {
        sysPostService.save(entity);
        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:post:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList) {
        sysPostService.removeBatchByIds(idList);
        return ResultInfo.success();
    }

}
