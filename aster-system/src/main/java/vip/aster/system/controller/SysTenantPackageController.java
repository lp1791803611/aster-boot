package vip.aster.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.constant.enums.StatusEnum;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysTenantPackage;
import vip.aster.system.query.SysTenantPackageQuery;
import vip.aster.system.service.SysTenantPackageService;
import vip.aster.system.vo.SysTenantPackageVO;

import java.util.List;

/**
 * 租户套餐
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-26 10:29:18
 */
@Tag(name = "租户套餐")
@RestController
@RequestMapping("/sys/tenantPackage")
@AllArgsConstructor
public class SysTenantPackageController {
    private final SysTenantPackageService sysTenantPackageService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:tenantPackage:list')")
    public ResultInfo<PageInfo<SysTenantPackageVO>> page(@ParameterObject @Valid SysTenantPackageQuery query) {
        PageInfo<SysTenantPackageVO> page = sysTenantPackageService.pageList(query);

        return ResultInfo.success(page);
    }

    @GetMapping("/list")
    @Operation(summary = "列表")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:tenantPackage:list')")
    public ResultInfo<List<SysTenantPackageVO>> get() {
        LambdaQueryWrapper<SysTenantPackage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysTenantPackage::getStatus, StatusEnum.ENABLE.getValue());
        List<SysTenantPackage> list = sysTenantPackageService.list(queryWrapper);

        return ResultInfo.success(SysTenantPackageVO.convertList(list));
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:tenantPackage:info')")
    public ResultInfo<SysTenantPackageVO> get(@PathVariable("id") String id) {
        SysTenantPackage entity = sysTenantPackageService.getById(id);

        return ResultInfo.success(new SysTenantPackageVO(entity));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:tenantPackage:add','sys:tenantPackage:edit')")
    public ResultInfo<String> save(@RequestBody SysTenantPackageVO vo) {
        sysTenantPackageService.save(vo);

        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:tenantPackage:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList) {
        sysTenantPackageService.removeBatchByIds(idList);

        return ResultInfo.success();
    }
}