package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysLogOperate;
import vip.aster.system.query.SysLogOperateQuery;
import vip.aster.system.service.SysLogOperateService;
import vip.aster.system.vo.SysLogOperateVO;

/**
 * 操作日志
 *
 * @author Aster
 * @since 2023/12/7 16:17
 */
@Tag(name = "操作日志")
@RestController
@RequestMapping("/sys/log/operate")
@AllArgsConstructor
public class SysLogOperateController {
    private SysLogOperateService sysLogOperateService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @PreAuthorize("hasAuthority('sys:log:operate')")
    public ResultInfo<PageInfo<SysLogOperateVO>> page(@ParameterObject @Valid SysLogOperateQuery query) {
        PageInfo<SysLogOperateVO> page = sysLogOperateService.pageList(query);
        return ResultInfo.success(page);
    }

    @GetMapping("/detail/{id}")
    @Operation(summary = "详情")
    @PreAuthorize("hasAuthority('sys:log:operate')")
    public ResultInfo<SysLogOperateVO> detail(@PathVariable String id) {
        SysLogOperate operate = sysLogOperateService.getById(id);
        if (operate == null) {
            return ResultInfo.success(new SysLogOperateVO());
        }
        return ResultInfo.success(new SysLogOperateVO(operate));
    }

    @GetMapping("/export")
    @Operation(summary = "导出excel")
    @Log(type = BusinessTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('sys:log:operate')")
    public void export(@ParameterObject @Valid SysLogOperateQuery query) {
        sysLogOperateService.export(query);
    }

}
