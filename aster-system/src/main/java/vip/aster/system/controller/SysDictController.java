package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysDictData;
import vip.aster.system.entity.SysDictType;
import vip.aster.system.query.SysDictDataQuery;
import vip.aster.system.query.SysDictTypeQuery;
import vip.aster.system.service.SysDictDataService;
import vip.aster.system.service.SysDictTypeService;
import vip.aster.system.vo.SysDictDataVO;
import vip.aster.system.vo.SysDictTypeVO;

import java.util.List;

/**
 * 字典
 *
 * @author Aster
 * @since 2023/12/4 16:47
 */
@RestController
@RequestMapping("/sys/dict")
@Tag(name = "字典")
@AllArgsConstructor
public class SysDictController {
    private SysDictTypeService dictTypeService;
    private SysDictDataService dictDataService;

    @GetMapping("")
    @Operation(summary = "全部字典数据")
    @Log(type = BusinessTypeEnum.SEARCH)
    public ResultInfo<List<SysDictTypeVO>> all() {
        List<SysDictTypeVO> list = dictTypeService.getDictAll();
        return ResultInfo.success(list);
    }

    @GetMapping("/type/page")
    @Operation(summary = "字典类型分页查询")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:dict:list')")
    public ResultInfo<PageInfo<SysDictTypeVO>> typePage(@ParameterObject @Valid SysDictTypeQuery query) {
        PageInfo<SysDictTypeVO> list = dictTypeService.pageList(query);
        return ResultInfo.success(list);
    }

    @GetMapping("/type/{id}")
    @Operation(summary = "字典类型详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:dict:info')")
    public ResultInfo<SysDictTypeVO> typeInfo(@PathVariable("id") String id) {
        SysDictType entity = dictTypeService.getById(id);
        return ResultInfo.success(new SysDictTypeVO(entity));
    }

    @PostMapping("/type/save")
    @Operation(summary = "字典类型保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:dict:add','sys:dict:edit')")
    public ResultInfo<String> typeSave(@RequestBody SysDictTypeVO entity) {
        dictTypeService.saveType(entity);
        return ResultInfo.success();
    }

    @PostMapping("/type/delete")
    @Operation(summary = "字典类型删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:dict:delete')")
    public ResultInfo<String> typeDelete(@RequestBody List<String> idList) {
        dictTypeService.removeDictTypByIds(idList);
        return ResultInfo.success();
    }

    @GetMapping("/data/page")
    @Operation(summary = "字典数据分页查询")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:dict:list')")
    public ResultInfo<PageInfo<SysDictDataVO>> dataPage(@ParameterObject @Valid SysDictDataQuery query) {
        PageInfo<SysDictDataVO> list = dictDataService.pageList(query);
        return ResultInfo.success(list);
    }

    @GetMapping("/data/{id}")
    @Operation(summary = "字典数据详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:dict:info')")
    public ResultInfo<SysDictDataVO> dataInfo(@PathVariable("id") String id) {
        SysDictData entity = dictDataService.getById(id);
        return ResultInfo.success(new SysDictDataVO(entity));
    }

    @PostMapping("/data/save")
    @Operation(summary = "字典数据保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:dict:add','sys:dict:edit')")
    public ResultInfo<String> dataSave(@RequestBody SysDictDataVO entity) {
        dictDataService.saveData(entity);
        return ResultInfo.success();
    }

    @PostMapping("/data/delete")
    @Operation(summary = "字典数据删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:dict:delete')")
    public ResultInfo<String> dataDelete(@RequestBody List<String> idList) {
        dictDataService.removeDictDataByIds(idList);
        return ResultInfo.success();
    }
}
