package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.quartz.SchedulerException;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysNotice;
import vip.aster.system.query.SysNoticeQuery;
import vip.aster.system.service.SysNoticeService;
import vip.aster.system.vo.SysNoticeVO;

import java.util.List;

/**
 * 通知公告
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-12 11:47:01
 */
@Tag(name = "通知公告")
@RestController
@RequestMapping("/sys/notice")
@AllArgsConstructor
public class SysNoticeController {
    private final SysNoticeService sysNoticeService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:notice:list')")
    public ResultInfo<PageInfo<SysNoticeVO>> page(@ParameterObject @Valid SysNoticeQuery query){
        PageInfo<SysNoticeVO> page = sysNoticeService.pageList(query);

        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情") 
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:notice:info')")
    public ResultInfo<SysNoticeVO> get(@PathVariable("id") String id){
        SysNotice entity = sysNoticeService.getById(id);

        return ResultInfo.success(new SysNoticeVO(entity));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:notice:add','sys:notice:edit')")
    public ResultInfo<String> save(@RequestBody SysNoticeVO vo){
        sysNoticeService.save(vo);

        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:notice:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList){
        sysNoticeService.removeBatchByIds(idList);

        return ResultInfo.success();
    }

    @PostMapping("/publish")
    @Operation(summary = "发布")
    @Log(type = BusinessTypeEnum.OTHER)
    @PreAuthorize("hasAnyAuthority('sys:notice:edit')")
    public ResultInfo<String> publish(@RequestBody SysNoticeVO vo) throws SchedulerException {
        sysNoticeService.publish(vo.getId(), vo.getPublishTime());

        return ResultInfo.success();
    }

    @GetMapping("/announcement")
    @Operation(summary = "获取公告信息")
    @Log(type = BusinessTypeEnum.SEARCH)
    public ResultInfo<SysNoticeVO> publish() {
        SysNoticeVO noticeVO = sysNoticeService.getAnnouncement();

        return ResultInfo.success(noticeVO);
    }
}