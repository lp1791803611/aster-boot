package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vip.aster.common.utils.PageInfo;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.query.SysLogLoginQuery;
import vip.aster.system.service.SysLogLoginService;
import vip.aster.system.vo.SysLogLoginVO;

/**
 * SysLogLoginController
 *
 * @author Aster
 * @since 2023/12/6 17:48
 */
@Tag(name = "访问日志")
@RestController
@RequestMapping("/sys/log/login")
@AllArgsConstructor
public class SysLogLoginController {
    private SysLogLoginService sysLogLoginService;

    @GetMapping("/page")
    @Operation(summary = "分页")
    @PreAuthorize("hasAuthority('sys:log:login')")
    public ResultInfo<PageInfo<SysLogLoginVO>> page(@ParameterObject @Valid SysLogLoginQuery query) {
        PageInfo<SysLogLoginVO> page = sysLogLoginService.pageList(query);
        return ResultInfo.success(page);
    }

    @GetMapping("/export")
    @Operation(summary = "导出excel")
    @Log(type = BusinessTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('sys:log:login')")
    public void export(@ParameterObject @Valid SysLogLoginQuery query) {
        sysLogLoginService.export(query);
    }
}
