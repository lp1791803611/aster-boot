package vip.aster.system.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysOrg;
import vip.aster.system.query.SysOrgQuery;
import vip.aster.system.service.SysOrgService;
import vip.aster.system.vo.SysOrgVO;

import java.util.List;

/**
 * 组织机构
 *
 * @author Aster
 * @since 2023/12/6 14:57
 */
@Tag(name = "组织机构")
@RestController
@RequestMapping("/sys/org")
@AllArgsConstructor
public class SysOrgController {
    private SysOrgService sysOrgService;

    @GetMapping("/list")
    @Operation(summary = "列表")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:org:list')")
    public ResultInfo<List<SysOrgVO>> page(@ParameterObject @Valid SysOrgQuery query) {
        List<SysOrgVO> page = sysOrgService.treeList(query);
        return ResultInfo.success(page);
    }

    @GetMapping("/info/{id}")
    @Operation(summary = "详情")
    @Log(type = BusinessTypeEnum.SEARCH)
    @PreAuthorize("hasAuthority('sys:org:info')")
    public ResultInfo<SysOrgVO> info(@PathVariable("id") String id) {
        SysOrg info = sysOrgService.getById(id);
        return ResultInfo.success(new SysOrgVO(info));
    }

    @PostMapping("/save")
    @Operation(summary = "保存")
    @Log(type = BusinessTypeEnum.SAVE)
    @PreAuthorize("hasAnyAuthority('sys:org:add','sys:org:edit')")
    public ResultInfo<String> save(@RequestBody SysOrgVO entity) {
        sysOrgService.save(entity);
        return ResultInfo.success();
    }

    @PostMapping("/delete")
    @Operation(summary = "删除")
    @Log(type = BusinessTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('sys:org:delete')")
    public ResultInfo<String> delete(@RequestBody List<String> idList) {
        sysOrgService.removeBatchByIds(idList);
        return ResultInfo.success();
    }
}
