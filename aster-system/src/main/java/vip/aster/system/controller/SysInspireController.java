package vip.aster.system.controller;

import com.alibaba.fastjson2.JSONObject;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import vip.aster.common.utils.ResultInfo;
import vip.aster.framework.log.annotation.Log;
import vip.aster.framework.log.enums.BusinessTypeEnum;
import vip.aster.system.entity.SysInspire;
import vip.aster.system.service.SysInspireService;

import java.util.List;

/**
 * SysInspireController
 *
 * @author Aster
 * @since 2024/1/19 16:16
 */
@Tag(name = "激励")
@RestController
@RequestMapping("/sys/inspire")
@AllArgsConstructor
public class SysInspireController {
    private SysInspireService sysInspireService;

    @GetMapping("/list")
    @Operation(summary = "全部")
    @Log(type = BusinessTypeEnum.SEARCH)
    public ResultInfo<List<SysInspire>> list() {
        List<SysInspire> list = sysInspireService.list();
        return ResultInfo.success(list);
    }

    @PostMapping("/batch/insert")
    @Operation(summary = "批量保存")
    @Log(type = BusinessTypeEnum.SAVE)
    public ResultInfo<String> batchInsert(@RequestBody List<String> contents) {
        sysInspireService.batchInsert(contents);
        return ResultInfo.success();
    }

    @PostMapping("/comment")
    @Operation(summary = "评论")
    @Log(type = BusinessTypeEnum.SAVE)
    public ResultInfo<String> comment(@RequestBody JSONObject comment) {
        String content = comment.getString("content");
        String type = comment.getString("type");
        sysInspireService.comment(content, type);
        return ResultInfo.success();
    }

}
