package vip.aster.system.excel.convert;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import vip.aster.system.utils.ConvertUtils;

/**
 * 操作类型转换
 *
 * @author Aster
 * @since 2023/12/7 17:17
 */
public class BusinessTypeConvert implements Converter<String> {

    @Override
    public Class<Integer> supportJavaTypeKey() {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public String convertToJavaData(ReadCellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return ConvertUtils.convertToJavaData("business_type", cellData, contentProperty, globalConfiguration);
    }


    @Override
    public WriteCellData<String> convertToExcelData(String value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return ConvertUtils.convertToExcelData("business_type", value, contentProperty, globalConfiguration);
    }
}
