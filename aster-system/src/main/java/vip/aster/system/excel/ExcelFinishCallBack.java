package vip.aster.system.excel;

import java.util.List;

/**
 * excel读取数据完成
 *
 * @author Aster
 * @since 2023/12/7 10:45
 */
public interface ExcelFinishCallBack<T> {
    /**
     * 导出后置处理数据
     * @param result 结果
     */
    void doAfterAllAnalysed(List<T> result);

    /**
     * 批量保持
     * @param result
     */
    default void doSaveBatch(List<T> result) {
    }
}
