package vip.aster.system.excel.convert;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import vip.aster.system.utils.ConvertUtils;

/**
 * 性别转换
 *
 * @author Aster
 * @since 2023/12/13 16:25
 */
public class GenderConvert implements Converter<String> {

    @Override
    public Class<Integer> supportJavaTypeKey() {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public String convertToJavaData(ReadCellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return ConvertUtils.convertToJavaData("gender", cellData, contentProperty, globalConfiguration);
    }


    @Override
    public WriteCellData<String> convertToExcelData(String value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return ConvertUtils.convertToExcelData("gender", value, contentProperty, globalConfiguration);
    }
}