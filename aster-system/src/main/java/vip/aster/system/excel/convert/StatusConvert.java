package vip.aster.system.excel.convert;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import vip.aster.system.entity.SysDictData;
import vip.aster.system.utils.ConvertUtils;
import vip.aster.system.utils.DictUtils;

import java.util.List;

/**
 * 成功状态转换
 *
 * @author Aster
 * @since 2023/12/7 14:56
 */
public class StatusConvert implements Converter<String> {

    @Override
    public Class<Integer> supportJavaTypeKey() {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public String convertToJavaData(ReadCellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return ConvertUtils.convertToJavaData("status", cellData, contentProperty, globalConfiguration);
    }


    @Override
    public WriteCellData<String> convertToExcelData(String value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return ConvertUtils.convertToExcelData("status", value, contentProperty, globalConfiguration);
    }
}
