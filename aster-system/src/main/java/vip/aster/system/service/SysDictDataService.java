package vip.aster.system.service;

import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysDictData;
import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.system.query.SysDictDataQuery;
import vip.aster.system.vo.SysDictDataVO;

import java.util.List;

/**
 * <p>
 * 字典数据 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysDictDataService extends IService<SysDictData> {

    /**
     * 分页
     * @param query 条件
     * @return 字典数据
     */
    PageInfo<SysDictDataVO> pageList(SysDictDataQuery query);

    /**
     * 保存
     * @param dictDataVO 字典数据
     */
    void saveData(SysDictDataVO dictDataVO);

    /**
     * 根据类型id查询字典数据
     * @param dictTypeId 字典类型
     * @return 字典数据
     */
    List<SysDictData> getDataByTypId(String dictTypeId);

    /**
     * 批量删除
     * @param idList 字典数据id
     */
    void removeDictDataByIds(List<String> idList);

    /**
     * 根据字典类型和字典值获取字典标签
     * @param dictType 字典类型
     * @param dictValue 字典值
     * @return 字典标签
     */
    String getDictLabel(String dictType, String dictValue);
}
