package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysLogOperate;
import vip.aster.system.query.SysLogOperateQuery;
import vip.aster.system.vo.SysLogOperateVO;

/**
 * <p>
 * 操作日志 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysLogOperateService extends IService<SysLogOperate> {
    /**
     * 分页
     * @param query 条件
     * @return 操作日志
     */
    PageInfo<SysLogOperateVO> pageList(SysLogOperateQuery query);

    /**
     * 导出
     * @param query 条件
     */
    void export(SysLogOperateQuery query);
}
