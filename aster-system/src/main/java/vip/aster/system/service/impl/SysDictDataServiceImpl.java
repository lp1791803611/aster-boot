package vip.aster.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysDictData;
import vip.aster.system.entity.SysDictType;
import vip.aster.system.mapper.SysDictDataMapper;
import vip.aster.system.mapper.SysDictTypeMapper;
import vip.aster.system.query.SysDictDataQuery;
import vip.aster.system.service.SysDictDataService;
import vip.aster.system.utils.DictUtils;
import vip.aster.system.vo.SysDictDataVO;

import java.util.List;

/**
 * <p>
 * 字典数据 服务实现类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
@Service
@AllArgsConstructor
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements SysDictDataService {
    private SysDictDataMapper sysDictDataMapper;
    private SysDictTypeMapper sysDictTypeMapper;

    @Override
    public PageInfo<SysDictDataVO> pageList(SysDictDataQuery query) {
        Page<SysDictData> page = new Page<>(query.getPageNum(), query.getPageSize());
        Page<SysDictData> pageList = this.page(page, getWrapper(query));
        return new PageInfo<>(SysDictDataVO.convertList(pageList.getRecords()), pageList.getTotal());
    }

    @Override
    public void saveData(SysDictDataVO dictDataVO) {
        SysDictData dictData = new SysDictData();
        BeanUtil.copyProperties(dictDataVO, dictData, true);
        if (StrUtil.isNotBlank(dictData.getId())) {
            sysDictDataMapper.updateById(dictData);
        } else {
            sysDictDataMapper.insert(dictData);
        }
        // 更新缓存
        List<SysDictData> dataList = this.getDataByTypId(dictData.getDictTypeId());
        SysDictType dictType = sysDictTypeMapper.selectById(dictData.getDictTypeId());
        DictUtils.setDictCache(dictType.getDictType(), dataList);
    }

    @Override
    public List<SysDictData> getDataByTypId(String dictTypeId) {
        LambdaQueryWrapper<SysDictData> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysDictData::getDictTypeId, dictTypeId);
        return this.list(queryWrapper);
    }

    @Override
    public void removeDictDataByIds(List<String> idList) {
        for (String id : idList) {
            SysDictData dictData = this.getById(id);
            this.removeById(id);
            List<SysDictData> dataList = this.getDataByTypId(dictData.getDictTypeId());
            SysDictType dictType = sysDictTypeMapper.selectById(dictData.getDictTypeId());
            DictUtils.setDictCache(dictType.getDictType(), dataList);
        }
    }

    @Override
    public String getDictLabel(String dictType, String dictValue) {
        if (StrUtil.isBlank(dictType) || StrUtil.isBlank(dictValue)) {
            return null;
        }
        // 先从缓存中拿
        String label = DictUtils.getDictLabel(dictType, dictValue);
        if (StrUtil.isBlank(label)) {
            // 缓存没有再从数据库拿
            LambdaQueryWrapper<SysDictType> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(StrUtil.isNotBlank(dictType), SysDictType::getDictType, dictType);
            SysDictType type = sysDictTypeMapper.selectOne(queryWrapper);
            if (type == null) {
                return null;
            }
            List<SysDictData> dictDataList = this.getDataByTypId(type.getId());
            if (CollUtil.isEmpty(dictDataList)) {
                return null;
            }
            List<String> labelList = dictDataList.stream().filter(f -> f.getDictValue().equals(dictValue)).map(SysDictData::getDictLabel).toList();
            return CollUtil.isNotEmpty(labelList) ? labelList.get(0) : null;
        }
        return label;
    }

    private Wrapper<SysDictData> getWrapper(SysDictDataQuery query) {
        LambdaQueryWrapper<SysDictData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictData::getDictTypeId, query.getDictTypeId());
        wrapper.orderByAsc(SysDictData::getSort);
        return wrapper;
    }
}
