package vip.aster.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysPost;
import vip.aster.system.mapper.SysPostMapper;
import vip.aster.system.query.SysPostQuery;
import vip.aster.system.service.SysPostService;
import vip.aster.system.vo.SysPostVO;

/**
 * <p>
 * 岗位 服务实现类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
@Service
@AllArgsConstructor
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements SysPostService {
    private SysPostMapper sysPostMapper;

    @Override
    public PageInfo<SysPostVO> pageList(SysPostQuery query) {
        Page<SysPost> page = new Page<>(query.getPageNum(), query.getPageSize());
        Page<SysPost> pageList = sysPostMapper.selectPage(page, getWrapper(query));
        return new PageInfo<>(SysPostVO.convertList(pageList.getRecords()), pageList.getTotal());
    }

    @Override
    public void save(SysPostVO vo) {
        SysPost post = new SysPost();
        BeanUtil.copyProperties(vo, post, true);
        if (StrUtil.isNotBlank(post.getId())) {
            sysPostMapper.updateById(post);
        } else {
            sysPostMapper.insert(post);
        }
    }

    private LambdaQueryWrapper<SysPost> getWrapper(SysPostQuery query) {
        LambdaQueryWrapper<SysPost> wrapper = Wrappers.lambdaQuery();

        wrapper.like(StrUtil.isNotBlank(query.getPostCode()), SysPost::getPostCode, query.getPostCode());
        wrapper.like(StrUtil.isNotBlank(query.getPostName()), SysPost::getPostName, query.getPostName());
        wrapper.eq(StrUtil.isNotBlank(query.getStatus()), SysPost::getStatus, query.getStatus());
        wrapper.orderByAsc(SysPost::getSort);

        return wrapper;
    }
}
