package vip.aster.system.service;

import vip.aster.system.entity.SysLogOperate;
import vip.aster.system.entity.SysOrg;
import vip.aster.system.entity.SysUser;

import java.util.List;

/**
 * TestService
 *
 * @author Aster
 * @since 2024/3/22 11:37
 */
public interface TestService {

    List<SysOrg> orgList();
    List<SysLogOperate> operateList();
    List<SysUser> userList();
    void updateUser(String email);
}
