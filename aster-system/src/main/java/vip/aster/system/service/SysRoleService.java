package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.framework.security.entity.UserDetail;
import vip.aster.system.entity.SysRole;
import vip.aster.system.query.SysRoleQuery;
import vip.aster.system.vo.SysRoleOrgVO;
import vip.aster.system.vo.SysRoleVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysRoleService extends IService<SysRole> {
    /**
     * 获取用户的数据权限
     * @param userId 用户ID
     * @return 数据权限
     */
    String getDataScopeByUserId(String userId);

    /**
     * 分页
     * @param query 条件
     * @return 角色
     */
    PageInfo<SysRoleVO> pageList(SysRoleQuery query);

    /**
     * 保存
     * @param vo 角色
     */
    void save(SysRoleVO vo);

    /**
     * 批量删除
     * @param idList 角色ID
     */
    void delete(List<String> idList);

    /**
     * 更新数据权限
     * @param roleOrg 角色机构
     */
    void updateOrg(SysRoleOrgVO roleOrg);

    /**
     * 获取用户的角色code
     * @param user 用户
     * @return 角色code
     */
    Set<String> getRoleCode(UserDetail user);
}
