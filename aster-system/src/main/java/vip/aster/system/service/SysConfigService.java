package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysConfig;
import vip.aster.system.query.SysConfigQuery;
import vip.aster.system.vo.SysConfigVO;

import java.util.List;

/**
 * <p>
 * 系统配置 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysConfigService extends IService<SysConfig> {
    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数键值
     */
    String selectConfigByKey(String configKey);

    /**
     * 获取验证码开关
     *
     * @return true开启，false关闭
     */
    boolean selectCaptchaOnOff();

    /**
     * 批量删除参数信息
     *
     * @param configIds 需要删除的参数ID
     */
    void deleteConfigByIds(List<String> configIds);

    /**
     * 加载参数缓存数据
     */
    void loadingConfigCache();

    /**
     * 清空参数缓存数据
     */
    void clearConfigCache();

    /**
     * 重置参数缓存数据
     */
    void resetConfigCache();

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数信息
     * @return 结果 true-唯一, false-不唯一
     */
    boolean checkConfigKeyUnique(SysConfig config);

    /**
     * 分页
     * @param config 分页条件
     * @return page
     */
    PageInfo<SysConfigVO> pageList(SysConfigQuery config);

    /**
     * 保存
     * @param configVO 配置
     */
    void saveConfig(SysConfigVO configVO);
}
