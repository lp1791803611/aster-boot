package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.system.entity.SysUserRole;

import java.util.List;

/**
 * <p>
 * 用户角色关系 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysUserRoleService extends IService<SysUserRole> {
    /**
     * 根据角色ID删除用户角色关系
     * @param roleIds 角色id
     */
    void deleteByRoleIdList(List<String> roleIds);

    /**
     * 删除用户角色关系
     * @param roleId 角色id
     * @param userIds 用户id
     */
    void deleteByUserIdList(String roleId, List<String> userIds);

    /**
     * 删除用户角色关系
     * @param userIds 用户id
     */
    void deleteByUserIdList(List<String> userIds);

    /**
     * 保存用户角色关系
     * @param roleId 角色id
     * @param userIds 用户id
     */
    void saveUserList(String roleId, List<String> userIds);

    /**
     * 查询用户的角色
     * @param userId 用户ID
     * @return 角色
     */
    List<String> getRoleIdList(String userId);

    /**
     * 保存用户角色关系
     * @param userId 用户id
     * @param roleIds 角色id
     */
    void saveRoleList(String userId, List<String> roleIds);
}
