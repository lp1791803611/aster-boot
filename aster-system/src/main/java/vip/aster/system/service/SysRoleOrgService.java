package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.system.entity.SysRoleOrg;

import java.util.List;

/**
 * <p>
 * 角色机构关系 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysRoleOrgService extends IService<SysRoleOrg> {
    /**
     * 获取用户的数据权限列表
     *
     * @param userId 用户ID
     * @return 机构ID列表
     */
    List<String> getDataScopeList(String userId);

    /**
     * 通过角色查询机构
     * @param roleId 角色id
     * @return 机构id
     */
    List<String> getOrgIdList(String roleId);

    /**
     * 根据角色ID删除角色机构关系
     * @param roleIds 角色ID
     */
    void deleteByRoleIdList(List<String> roleIds);

    /**
     * 保存角色机构关系
     * @param roleId 角色id
     * @param orgIdList 机构id
     */
    void save(String roleId, List<String> orgIdList);
}
