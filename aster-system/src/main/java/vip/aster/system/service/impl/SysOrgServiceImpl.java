package vip.aster.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.common.constant.Constants;
import vip.aster.system.entity.SysOrg;
import vip.aster.system.entity.SysPost;
import vip.aster.system.mapper.SysOrgMapper;
import vip.aster.system.query.SysOrgQuery;
import vip.aster.system.query.SysPostQuery;
import vip.aster.system.service.SysOrgService;
import vip.aster.system.vo.SysOrgVO;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 机构管理 服务实现类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
@Service
@AllArgsConstructor
public class SysOrgServiceImpl extends ServiceImpl<SysOrgMapper, SysOrg> implements SysOrgService {
    private SysOrgMapper sysOrgMapper;

    @Override
    public List<String> getSubOrgIdList(String orgId) {
        // 先查出所有机构
        List<SysOrg> orgList = this.list();

        // 递归查询所有子机构ID列表
        List<String> subIdList = new ArrayList<>();
        getTree(orgId, orgList, subIdList);

        // 本机构也添加进去
        subIdList.add(orgId);

        return subIdList;
    }

    @Override
    public List<SysOrgVO> treeList(SysOrgQuery query) {

        // 先查出所有机构
        List<SysOrg> orgList = this.list(getWrapper(query));

        // 组装树
        List<SysOrgVO> list = SysOrgVO.convertList(orgList);

        long count = list.stream().filter(org -> Constants.ROOT_NODE.equals(org.getPid())).count();
        if (count == 0L) {
            return list;
        }
        return treeBuild(list, Constants.ROOT_NODE);
    }

    @Override
    public void save(SysOrgVO entity) {
        SysOrg org = new SysOrg();
        BeanUtil.copyProperties(entity, org, true);
        String pid = StrUtil.isBlank(org.getPid()) ? "0" : org.getPid();
        org.setPid(pid);
        if (StrUtil.isNotBlank(org.getId())) {
            sysOrgMapper.updateById(org);
        } else {
            sysOrgMapper.insert(org);
        }
    }

    private List<SysOrgVO> treeBuild(List<SysOrgVO> nodes, String rootNode) {
        List<SysOrgVO> list = new ArrayList<>();
        for (SysOrgVO node : nodes) {
            // 从root节点开始组合树
            if (rootNode.equals(node.getPid())) {
                node.setChildren(getChildren(node.getId(), nodes));
                list.add(node);
            }
        }
        return list;
    }

    private List<SysOrgVO> getChildren(String id, List<SysOrgVO> nodes) {
        List<SysOrgVO> list = new ArrayList<>();
        for (SysOrgVO node : nodes) {
            if (node.getPid().equals(id)) {
                node.setChildren(getChildren(node.getId(), nodes));
                list.add(node);
            }
        }
        return list;
    }

    private void getTree(String id, List<SysOrg> orgList, List<String> subIdList) {
        for(SysOrg org : orgList){
            if (org.getPid().equals(id)){
                getTree(org.getId(), orgList, subIdList);
                subIdList.add(org.getId());
            }
        }
    }

    private LambdaQueryWrapper<SysOrg> getWrapper(SysOrgQuery query) {
        LambdaQueryWrapper<SysOrg> wrapper = Wrappers.lambdaQuery();

        wrapper.like(StrUtil.isNotBlank(query.getOrgName()), SysOrg::getOrgName, query.getOrgName());
        wrapper.eq(StrUtil.isNotBlank(query.getStatus()), SysOrg::getStatus, query.getStatus());
        wrapper.orderByAsc(SysOrg::getSort);

        return wrapper;
    }
}
