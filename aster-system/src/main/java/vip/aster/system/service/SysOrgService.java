package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.system.entity.SysOrg;
import vip.aster.system.query.SysOrgQuery;
import vip.aster.system.vo.SysOrgVO;

import java.util.List;

/**
 * <p>
 * 机构管理 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysOrgService extends IService<SysOrg> {
    /**
     * 根据机构ID，获取子机构ID列表(包含本机构ID)
     * @param orgId 机构id
     * @return 机构ID列表
     */
    List<String> getSubOrgIdList(String orgId);

    /**
     * 获取部门树
     * @param query 条件
     * @return 机构树
     */
    List<SysOrgVO> treeList(SysOrgQuery query);

    /**
     * 保存
     * @param entity 机构
     */
    void save(SysOrgVO entity);
}
