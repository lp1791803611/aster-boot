package vip.aster.system.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.system.entity.SysLogOperate;
import vip.aster.system.entity.SysOrg;
import vip.aster.system.entity.SysUser;
import vip.aster.system.mapper.TestMapper;
import vip.aster.system.service.TestService;

import java.util.List;

/**
 * TestServiceImpl
 *
 * @author Aster
 * @since 2024/3/22 11:37
 */
@Service
@AllArgsConstructor
public class TestServiceImpl implements TestService {
    private TestMapper testMapper;

    @Override
    public List<SysOrg> orgList() {
        return testMapper.orgList();
    }

    @Override
    public List<SysLogOperate> operateList() {
        return testMapper.operateList();
    }

    @Override
    public List<SysUser> userList() {
        return testMapper.userList();
    }

    @Override
    public void updateUser(String email) {
        testMapper.updateUser(email);
    }
}
