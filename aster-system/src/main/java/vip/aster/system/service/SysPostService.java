package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysPost;
import vip.aster.system.query.SysPostQuery;
import vip.aster.system.vo.SysPostVO;

/**
 * <p>
 * 岗位 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysPostService extends IService<SysPost> {
    /**
     * 分页查询
     * @param query 条件
     * @return 列表
     */
    PageInfo<SysPostVO> pageList(SysPostQuery query);

    /**
     * 保存
     * @param vo 岗位信息
     */
    void save(SysPostVO vo);
}
