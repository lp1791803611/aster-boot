package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysUser;
import vip.aster.system.query.SysUserQuery;
import vip.aster.system.query.SysUserRoleQuery;
import vip.aster.system.vo.SysUserVO;

import java.util.List;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 根据手机号查询用户
     *
     * @param mobile 手机号
     * @return 用户信息
     */
    SysUserVO getUserByMobile(String mobile);

    /**
     * 获取 UserDetails 对象
     *
     * @param user 用户
     * @return UserDetails
     */
    UserDetails getUserDetails(SysUserVO user);

    /**
     * 根据用户名获取用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    SysUserVO getUserByUsername(String username);

    /**
     * 分配角色
     *
     * @param query 查询条件
     * @return 分页
     */
    PageInfo<SysUserVO> roleUserPage(SysUserRoleQuery query);

    /**
     * 分页查询
     *
     * @param query 查询条件
     * @return 分页
     */
    PageInfo<SysUserVO> pageList(SysUserQuery query);

    /**
     * 更新密码
     *
     * @param userId      用户id
     * @param newPassword 新密码
     */
    void updatePassword(String userId, String newPassword);

    /**
     * 保存
     *
     * @param sysUserVO 用户
     */
    void save(SysUserVO sysUserVO);

    /**
     * 删除用户
     *
     * @param idList 用户id
     */
    void delete(List<String> idList);

    /**
     * 导入用户
     *
     * @param file     待导入文件
     * @param password 密码
     */
    void importByExcel(MultipartFile file, String password);

    /**
     * 导出用户
     */
    void export();

    /**
     * 更新个人信息
     * @param user 用户信息
     */
    void editInfo(SysUserVO user);

    /**
     * 根据租户和用户名查询用户信息
     * @param tenantId 租户id
     * @param username 用户名
     * @return 用户信息
     */
    SysUserVO getUserByTenant(String tenantId, String username);
}
