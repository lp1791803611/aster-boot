package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysTenantPackage;
import vip.aster.system.query.SysTenantPackageQuery;
import vip.aster.system.vo.SysTenantPackageVO;

/**
 * <P>
 * 租户套餐 服务接口
 * </P>
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-26 09:46:32
 */
public interface SysTenantPackageService extends IService<SysTenantPackage> {
    /**
     * 分页
     * @param query 查询条件
     * @return 查询结果
     */
    PageInfo<SysTenantPackageVO> pageList(SysTenantPackageQuery query);

    /**
     * 保存
     * @param vo 租户套餐
     */
    void save(SysTenantPackageVO vo);
}