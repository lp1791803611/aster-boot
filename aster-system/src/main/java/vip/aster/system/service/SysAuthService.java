package vip.aster.system.service;

import vip.aster.system.vo.SysAccountLoginVO;
import vip.aster.system.vo.SysMobileLoginVO;
import vip.aster.system.vo.SysTokenVO;

import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * 权限认证服务
 *
 * @author Aster
 * @since 2023/11/30 16:48
 */
public interface SysAuthService {
    /**
     * 账号密码登录
     *
     * @param login 登录信息
     */
    SysTokenVO loginByAccount(SysAccountLoginVO login);

    /**
     * 手机短信登录
     *
     * @param login 登录信息
     */
    SysTokenVO loginByMobile(SysMobileLoginVO login);

    /**
     * 发送手机验证码
     *
     * @param mobile 手机号
     */
    boolean sendCode(String mobile);

    /**
     * 退出登录
     *
     * @param accessToken accessToken
     */
    void logout(String accessToken);

    /**
     * 重置密码
     * @param idList 用户ID
     */
    void resetPassword(List<String> idList);

    /**
     * 生成秘钥
     * @return 公钥
     */
    String generateSecretKey() throws NoSuchAlgorithmException;

    /**
     * 检验租户
     * @param tenantId 租户id
     */
    void checkTenant(String tenantId);

}
