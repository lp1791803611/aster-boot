package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.framework.security.entity.UserDetail;
import vip.aster.system.entity.SysMenu;
import vip.aster.system.query.SysMenuQuery;
import vip.aster.system.vo.SysMenuVO;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysMenuService extends IService<SysMenu> {
    /**
     * 获取用户权限列表
     *
     * @param user 用户信息
     * @return 权限列表
     */
    Set<String> getUserAuthority(UserDetail user);

    /**
     * 菜单树
     * @param query 条件
     * @return 树
     */
    List<SysMenuVO> treeList(SysMenuQuery query);

    /**
     * 保存
     * @param entity 菜单
     */
    void save(SysMenuVO entity);

    /**
     * 用户菜单列表
     * @param user 用户
     * @param types 菜单类型
     * @return 菜单
     */
    List<SysMenuVO> getUserMenuList(UserDetail user, List<String> types);
}
