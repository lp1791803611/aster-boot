package vip.aster.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.aster.system.entity.SysUserRole;
import vip.aster.system.mapper.SysUserRoleMapper;
import vip.aster.system.service.SysUserRoleService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户角色关系 服务实现类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
@Service
@AllArgsConstructor
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {
    @Override
    public void deleteByRoleIdList(List<String> roleIds) {
        LambdaQueryWrapper<SysUserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(SysUserRole::getRoleId, roleIds);
        this.remove(queryWrapper);
    }

    @Override
    public void deleteByUserIdList(String roleId, List<String> userIds) {
        LambdaQueryWrapper<SysUserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUserRole::getRoleId, roleId).in(SysUserRole::getUserId, userIds);
        this.remove(queryWrapper);
    }

    @Override
    public void deleteByUserIdList(List<String> userIds) {
        LambdaQueryWrapper<SysUserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(SysUserRole::getUserId, userIds);
        this.remove(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUserList(String roleId, List<String> userIds) {
        List<SysUserRole> list = userIds.stream().map(userId -> {
            SysUserRole entity = new SysUserRole();
            entity.setUserId(userId);
            entity.setRoleId(roleId);
            return entity;
        }).collect(Collectors.toList());

        // 批量新增
        this.saveBatch(list);
    }

    @Override
    public List<String> getRoleIdList(String userId) {
        LambdaQueryWrapper<SysUserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUserRole::getUserId, userId);
        List<SysUserRole> list = this.list(queryWrapper);
        return CollUtil.isEmpty(list) ? ListUtil.empty() : list.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveRoleList(String userId, List<String> roleIds) {
        List<SysUserRole> list = roleIds.stream().map(roleId -> {
            SysUserRole entity = new SysUserRole();
            entity.setUserId(userId);
            entity.setRoleId(roleId);
            return entity;
        }).collect(Collectors.toList());

        // 先删除再新增
        LambdaQueryWrapper<SysUserRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUserRole::getUserId, userId);
        this.remove(queryWrapper);

        // 批量新增
        this.saveBatch(list);
    }
}
