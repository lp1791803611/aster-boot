package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysLogLogin;
import vip.aster.system.query.SysLogLoginQuery;
import vip.aster.system.vo.SysLogLoginVO;

/**
 * <p>
 * 访问日志 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysLogLoginService extends IService<SysLogLogin> {
    /**
     * 保存登录日志
     *
     * @param userId    用户Id
     * @param username  用户名
     * @param status    登录状态
     * @param operation 操作信息
     * @param tenantId  租户Id
     */
    void save(String userId, String username, String status, String operation, String tenantId);

    /**
     * 分页查询
     *
     * @param query 条件
     * @return 日志
     */
    PageInfo<SysLogLoginVO> pageList(SysLogLoginQuery query);

    /**
     * 导出
     */
    void export(SysLogLoginQuery query);
}
