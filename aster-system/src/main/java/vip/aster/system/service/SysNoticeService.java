package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.quartz.SchedulerException;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysNotice;
import vip.aster.system.query.SysNoticeQuery;
import vip.aster.system.vo.SysNoticeVO;

/**
 * <P>
 * 通知公告 服务接口
 * </P>
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-12 11:10:07
 */
public interface SysNoticeService extends IService<SysNotice> {

    /**
     * 分页
     * @param query 查询条件
     * @return 查询结果
     */
    PageInfo<SysNoticeVO> pageList(SysNoticeQuery query);

    /**
     * 保存
     * @param vo 通知公告
     */
    void save(SysNoticeVO vo);

    /**
     * 发布公告
     * @param id 公告id
     * @param publishTime 发布时间
     */
    void publish(String id, String publishTime) throws SchedulerException;

    /**
     * 发布公告
     * @param content 公告内容
     */
    void publishNotice(String content);

    /**
     * 获取公告
     * @return 公告
     */
    SysNoticeVO getAnnouncement();

}