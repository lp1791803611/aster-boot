package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysDictType;
import vip.aster.system.query.SysDictTypeQuery;
import vip.aster.system.vo.SysConfigVO;
import vip.aster.system.vo.SysDictTypeVO;

import java.util.List;

/**
 * <p>
 * 字典类型 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysDictTypeService extends IService<SysDictType> {
    /**
     * 获取所有字典
     * @return 字典
     */
    List<SysDictTypeVO> getDictAll();

    /**
     * 分页查询
     * @param query 条件
     * @return list
     */
    PageInfo<SysDictTypeVO> pageList(SysDictTypeQuery query);

    /**
     * 保存
     * @param dictTypeVO 字典类型
     */
    void saveType(SysDictTypeVO dictTypeVO);

    /**
     * 批量删除
     * @param ids 字典类型id
     */
    void removeDictTypByIds(List<String> ids);
}
