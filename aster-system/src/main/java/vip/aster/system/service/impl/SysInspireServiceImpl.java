package vip.aster.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.aster.common.constant.enums.CommentTypeEnum;
import vip.aster.system.entity.SysInspire;
import vip.aster.system.mapper.SysInspireMapper;
import vip.aster.system.service.SysInspireService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 激励 服务实现类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
@Service
@AllArgsConstructor
public class SysInspireServiceImpl extends ServiceImpl<SysInspireMapper, SysInspire> implements SysInspireService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchInsert(List<String> contents) {
        List<SysInspire> list = new ArrayList<>();
        if (CollUtil.isEmpty(contents)) {
            return;
        }
        for (String content : contents) {
            SysInspire inspire = new SysInspire();
            inspire.setContent(content);
            inspire.setLikeNum(0);
            inspire.setDislikeNum(0);
            list.add(inspire);
        }
        this.saveBatch(list);
    }

    @Override
    public void comment(String content, String type) {
        if (StrUtil.isBlank(content)) {
            return;
        }
        LambdaQueryWrapper<SysInspire> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysInspire::getContent, content);

        SysInspire inspire = this.getOne(queryWrapper);
        if (CommentTypeEnum.DISLIKE.getValue().equals(type)) {
            inspire.setDislikeNum(inspire.getDislikeNum() + 1);
        } else {
            inspire.setLikeNum(inspire.getLikeNum() + 1);
        }
        this.updateById(inspire);
    }
}
