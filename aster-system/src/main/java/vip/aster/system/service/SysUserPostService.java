package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.system.entity.SysUserPost;

import java.util.List;

/**
 * <p>
 * 用户岗位关系 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysUserPostService extends IService<SysUserPost> {
    /**
     * 查询用户的岗位
     * @param userId 用户id
     * @return 岗位
     */
    List<String> getPostIdList(String userId);

    /**
     * 保存用户岗位关系
     * @param userId 用户id
     * @param postIds 岗位id
     */
    void savePostList(String userId, List<String> postIds);

    /**
     * 删除用户岗位关系
     * @param userIds 用户id
     */
    void deleteByUserIdList(List<String> userIds);
}
