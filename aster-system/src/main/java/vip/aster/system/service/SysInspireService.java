package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.system.entity.SysInspire;

import java.util.List;

/**
 * SysInspireService
 *
 * @author Aster
 * @since 2024/1/19 15:57
 */
public interface SysInspireService extends IService<SysInspire> {

    /**
     * 批量insert
     * @param contents 内容
     */
    void batchInsert(List<String> contents);

    /**
     * 评论
     * @param content 内容
     * @param type 类型
     */
    void comment(String content, String type);
}
