package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.system.entity.SysRoleMenu;

import java.util.List;

/**
 * <p>
 * 角色菜单关系 服务类
 * </p>
 *
 * @author Aster
 * @since 2023-11-28 10:36
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {
    /**
     * 通过角色查询菜单
     * @param roleId 角色id
     * @return 菜单id
     */
    List<String> getMenuIdList(String roleId);

    /**
     * 根据角色id保存菜单
     * @param roleId 角色ID
     * @param menuIds 菜单id
     */
    void saveByRoleId(String roleId, List<String> menuIds);

    /**
     * 根据角色ID删除角色菜单关系
     * @param roleIds 角色ID
     */
    void deleteByRoleIdList(List<String> roleIds);
}
