package vip.aster.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysTenantPackage;
import vip.aster.system.mapper.SysTenantPackageMapper;
import vip.aster.system.query.SysTenantPackageQuery;
import vip.aster.system.service.SysTenantPackageService;
import vip.aster.system.vo.SysTenantPackageVO;

/**
 * <P>
 * 租户套餐 服务实现类
 * </P>
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-26 09:46:32
 */
@Service
@AllArgsConstructor
public class SysTenantPackageServiceImpl extends ServiceImpl<SysTenantPackageMapper, SysTenantPackage> implements SysTenantPackageService {
    private SysTenantPackageMapper sysTenantPackageMapper;

    @Override
    public PageInfo<SysTenantPackageVO> pageList(SysTenantPackageQuery query) {
        Page<SysTenantPackage> page = new Page<>(query.getPageNum(), query.getPageSize());
        Page<SysTenantPackage> pageList = sysTenantPackageMapper.selectPage(page, getWrapper(query));
        return new PageInfo<>(SysTenantPackageVO.convertList(pageList.getRecords()), pageList.getTotal());
    }

    @Override
    public void save(SysTenantPackageVO vo) {
        SysTenantPackage entity = vo.reconvert();

        if (StrUtil.isNotBlank(entity.getId())) {
            sysTenantPackageMapper.updateById(entity);
        } else {
            sysTenantPackageMapper.insert(entity);
        }
    }

    private LambdaQueryWrapper<SysTenantPackage> getWrapper(SysTenantPackageQuery query){
        LambdaQueryWrapper<SysTenantPackage> wrapper = Wrappers.lambdaQuery();
        wrapper.like(StrUtil.isNotBlank(query.getPackageName()), SysTenantPackage::getPackageName, query.getPackageName());
        wrapper.eq(StrUtil.isNotBlank(query.getStatus()), SysTenantPackage::getStatus, query.getStatus());
        return wrapper;
    }

}