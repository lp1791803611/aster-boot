package vip.aster.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.aster.common.utils.PageInfo;
import vip.aster.system.entity.SysTenant;
import vip.aster.system.query.SysTenantQuery;
import vip.aster.system.vo.SysTenantVO;

import java.util.List;

/**
 * <P>
 * 租户 服务接口
 * </P>
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-25 14:46:59
 */
public interface SysTenantService extends IService<SysTenant> {
    /**
     * 分页
     * @param query 查询条件
     * @return 查询结果
     */
    PageInfo<SysTenantVO> pageList(SysTenantQuery query);

    /**
     * 保存
     * @param vo 租户
     */
    void save(SysTenantVO vo) throws Exception;

    /**
     * 获取有效租户列表
     */
    List<SysTenantVO> getList();

    /**
     * 判断用户是否租户管理员
     * @param userId 用户ID
     * @param tenantId 租户ID
     * @return true-管理员 false-非管理员
     */
    boolean isTenantAdmin(String userId, String tenantId);
}