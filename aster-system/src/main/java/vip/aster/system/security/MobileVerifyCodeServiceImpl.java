package vip.aster.system.security;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vip.aster.framework.security.mobile.MobileVerifyCodeService;

/**
 * 短信验证码效验
 *
 * @author Aster
 * @since 2023/12/1 11:45
 */
@Service
@AllArgsConstructor
public class MobileVerifyCodeServiceImpl implements MobileVerifyCodeService {

    @Override
    public boolean verifyCode(String mobile, String code) {
        //TODO 短信验证码校验
        return false;
    }
}
