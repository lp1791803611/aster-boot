package vip.aster.system.security;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vip.aster.framework.i18n.MessageUtils;
import vip.aster.system.entity.SysUser;
import vip.aster.system.service.SysUserService;
import vip.aster.system.vo.SysUserVO;

/**
 * UserDetailsServiceImpl
 *
 * @author Aster
 * @since 2023/12/1 11:46
 */
@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private SysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUserVO user = sysUserService.getUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(MessageUtils.message("auth.login.account.error"));
        }

        return sysUserService.getUserDetails(user);
    }
}
