package vip.aster.system.security;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vip.aster.framework.i18n.MessageUtils;
import vip.aster.framework.security.mobile.MobileUserDetailsService;
import vip.aster.system.entity.SysUser;
import vip.aster.system.service.SysUserService;
import vip.aster.system.vo.SysUserVO;

/**
 * 手机验证码登录
 *
 * @author Aster
 * @since 2023/12/1 10:07
 */
@Service
@AllArgsConstructor
public class MobileUserDetailsServiceImpl implements MobileUserDetailsService {
    private final SysUserService sysUserService;

    @Override
    public UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException {
        SysUserVO user = sysUserService.getUserByMobile(mobile);
        if (user == null) {
            throw new UsernameNotFoundException(MessageUtils.message("auth.login.mobile.error"));
        }

        return sysUserService.getUserDetails(user);
    }

}
