package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

/**
 * 用户查询
 *
 * @author Aster
 * @since 2023/12/12 14:59
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "用户查询")
public class SysUserQuery extends Query {

    @Schema(description = "用户名/昵称/手机号")
    private String name;

    @Schema(description = "机构ID")
    private String orgId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "手机号")
    private String mobile;

    @Schema(description = "性别")
    private String gender;

    @Schema(description = "状态")
    private String status;
}
