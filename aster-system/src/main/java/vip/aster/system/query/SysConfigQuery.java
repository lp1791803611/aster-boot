package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

/**
 * SysConfigQuery
 *
 * @author Aster
 * @since 2023/12/6 10:33
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "参数查询")
public class SysConfigQuery extends Query {
    @Schema(description = "参数名称")
    private String configName;

    @Schema(description = "参数键名")
    private String configKey;

    @Schema(description = "系统内置,'0'-'是','1'-‘否’")
    private String configType;
}
