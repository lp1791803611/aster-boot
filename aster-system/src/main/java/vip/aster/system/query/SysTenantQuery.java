package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

import java.time.LocalDateTime;

/**
 * 租户查询
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-25 14:46:59
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "租户查询")
public class SysTenantQuery extends Query {
    @Schema(description = "联系人")
    private String contactUserName;

    @Schema(description = "企业名称")
    private String companyName;

    @Schema(description = "启用状态")
    private String status;

}