package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 菜单查询
 *
 * @author Aster
 * @since 2023/12/6 17:09
 */
@Data
@Schema(description = "菜单查询")
public class SysMenuQuery {

    @Schema(description = "菜单名称")
    private String name;

    @Schema(description = "菜单URL")
    private String path;

    @Schema(description = "启用状态")
    private String status;
}
