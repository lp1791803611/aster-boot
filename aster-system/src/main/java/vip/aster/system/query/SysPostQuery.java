package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

/**
 * 岗位查询
 *
 * @author Aster
 * @since 2023/12/6 11:16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "岗位查询")
public class SysPostQuery extends Query {

    @Schema(description = "岗位编码")
    private String postCode;

    @Schema(description = "岗位名称")
    private String postName;

    @Schema(description = "启用状态")
    private String status;
}
