package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

/**
 * 角色查询
 *
 * @author Aster
 * @since 2023/12/8 16:39
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "角色查询")
public class SysRoleQuery extends Query {

    @Schema(description = "角色名称")
    private String roleName;

    @Schema(description = "角色名称")
    private String roleCode;

    @Schema(description = "启用状态")
    private String status;
}
