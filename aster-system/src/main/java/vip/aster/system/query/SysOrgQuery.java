package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

/**
 * 组织机构查询
 *
 * @author Aster
 * @since 2023/12/6 14:56
 */
@Data
@Schema(description = "组织机构查询")
public class SysOrgQuery {

    @Schema(description = "机构名称")
    private String orgName;

    @Schema(description = "启用状态")
    private String status;
}
