package vip.aster.system.query;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

import java.time.LocalDateTime;

/**
 * 操作日志查询
 *
 * @author Aster
 * @since 2023/12/7 16:26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "访问日志查询")
public class SysLogOperateQuery extends Query {

    @Schema(description = "操作人")
    private String username;

    @Schema(description = "操作类型")
    private String businessType;

    @Schema(description = "开始时间")
    private String startTime;

    @Schema(description = "结束时间")
    private String endTime;
}
