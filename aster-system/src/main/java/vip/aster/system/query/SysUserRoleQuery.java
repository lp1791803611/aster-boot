package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * SysUserRoleQuery
 *
 * @author Aster
 * @since 2023/12/12 14:46
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Schema(description = "用户角色查询")
public class SysUserRoleQuery extends SysUserQuery {

    @Schema(description = "角色ID")
    private String roleId;
}
