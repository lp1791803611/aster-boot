package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

import java.time.LocalDateTime;

/**
 * 租户套餐查询
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-26 09:46:32
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "租户套餐查询")
public class SysTenantPackageQuery extends Query {
    @Schema(description = "套餐名称")
    private String packageName;

    @Schema(description = "启用状态")
    private String status;

}