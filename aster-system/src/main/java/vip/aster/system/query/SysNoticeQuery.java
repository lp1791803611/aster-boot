package vip.aster.system.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;

/**
 * 通知公告查询
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-12 11:10:07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "通知公告查询")
public class SysNoticeQuery extends Query {
    @Schema(description = "公告标题")
    private String title;

    @Schema(description = "公告类型（1通知 2公告）")
    private String type;

    @Schema(description = "启用状态")
    private String status;

}