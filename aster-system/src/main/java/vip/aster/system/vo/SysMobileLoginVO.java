package vip.aster.system.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 手机号登录
 *
 * @author Aster
 * @since 2023/11/30 16:51
 */
@Data
@Schema(description = "手机号登录")
public class SysMobileLoginVO implements Serializable {
    @Serial
    private static final long serialVersionUID = 9190799550864859792L;

    @Schema(description = "手机号")
    private String mobile;

    @Schema(description = "验证码")
    private String code;

    @Schema(description = "租户ID")
    private String tenantId;
}
