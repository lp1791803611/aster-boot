package vip.aster.system.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.LocalDateTimeUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.system.entity.SysTenant;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 租户
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-25 14:46:59
 */
@Data
@Schema(description = "租户")
public class SysTenantVO implements Serializable {

    @Serial
    private static final long serialVersionUID = -2087432259846425065L;

    @Schema(description = "id")
    private String id;
    @Schema(description = "联系人")
    private String contactUserName;
    @Schema(description = "联系电话")
    private String contactPhone;
    @Schema(description = "企业名称")
    private String companyName;
    @Schema(description = "统一社会信用代码")
    private String licenseNumber;
    @Schema(description = "地址")
    private String address;
    @Schema(description = "企业简介")
    private String intro;
    @Schema(description = "域名")
    private String domain;
    @Schema(description = "租户套餐编号")
    private String packageId;
    @Schema(description = "过期时间")
    private String expireTime;
    @Schema(description = "用户数量（-1不限制）")
    private Integer accountCount;
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
    @Schema(description = "启用状态")
    private String status;
    @Schema(description = "备注")
    private String remark;

    @Schema(description = "用户名")
    private String username;
    @Schema(description = "密码")
    private String password;


    public SysTenantVO() {
        super();
    }

    public SysTenantVO(SysTenant entity) {
        this.id = entity.getId();
        this.contactUserName = entity.getContactUserName();
        this.contactPhone = entity.getContactPhone();
        this.companyName = entity.getCompanyName();
        this.licenseNumber = entity.getLicenseNumber();
        this.address = entity.getAddress();
        this.intro = entity.getIntro();
        this.domain = entity.getTenantDomain();
        this.packageId = entity.getPackageId();
        if (entity.getExpireTime() != null) {
            this.expireTime = LocalDateTimeUtil.format(entity.getExpireTime(), DatePattern.NORM_DATETIME_PATTERN);
        } else {
            this.expireTime = null;
        }
        this.accountCount = entity.getAccountCount();
        this.createTime = entity.getCreateTime();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

    public static List<SysTenantVO> convertList(List<SysTenant> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysTenantVO> list = new ArrayList<>();
        for (SysTenant entity : source) {
            list.add(new SysTenantVO(entity));
        }
        return list;
    }

    public SysTenant reconvert() {
        SysTenant entity = new SysTenant();
        entity.setId(this.id);
        entity.setContactUserName(this.contactUserName);
        entity.setContactPhone(this.contactPhone);
        entity.setCompanyName(this.companyName);
        entity.setLicenseNumber(this.licenseNumber);
        entity.setAddress(this.address);
        entity.setIntro(this.intro);
        entity.setTenantDomain(this.domain);
        entity.setPackageId(this.packageId);
        if (this.expireTime != null) {
            entity.setExpireTime(LocalDateTimeUtil.parse(this.expireTime, DatePattern.NORM_DATETIME_PATTERN));
        } else {
            entity.setExpireTime(null);
        }
        entity.setAccountCount(this.accountCount);
        entity.setCreateTime(this.createTime);
        entity.setStatus(this.status);
        entity.setRemark(this.remark);
        return entity;
    }

}