package vip.aster.system.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.system.entity.SysPost;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 岗位数据
 *
 * @author Aster
 * @since 2023/12/6 11:10
 */
@Data
@Schema(description = "岗位数据")
public class SysPostVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -6172592623453698790L;

    @Schema(description = "ID")
    private String id;

    @Schema(description = "岗位编码")
    private String postCode;

    @Schema(description = "岗位名称")
    private String postName;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "备注信息")
    private String remark;

    public SysPostVO() {
        super();
    }

    public SysPostVO(SysPost entity) {
        this.id = entity.getId();
        this.postName = entity.getPostName();
        this.postCode = entity.getPostCode();
        this.sort = entity.getSort();
        this.createTime = entity.getCreateTime();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

    public static List<SysPostVO> convertList(List<SysPost> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysPostVO> list = new ArrayList<>();
        for (SysPost post : source) {
            list.add(new SysPostVO(post));
        }
        return list;
    }
}
