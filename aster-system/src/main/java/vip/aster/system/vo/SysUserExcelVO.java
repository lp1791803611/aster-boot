package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import vip.aster.system.entity.SysUser;
import vip.aster.system.excel.convert.DateConverter;
import vip.aster.system.excel.convert.GenderConvert;
import vip.aster.system.excel.convert.StatusConvert;
import vip.aster.system.excel.convert.WhetherConvert;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * excel用户表
 *
 * @author Aster
 * @since 2023/12/13 16:15
 */
@Data
public class SysUserExcelVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -7903038643026709177L;

    @ExcelIgnore
    private String id;

    @ExcelProperty("用户名")
    private String username;

    @ExcelProperty("昵称")
    private String nickName;

    @ExcelProperty("姓名")
    private String realName;

    @ExcelProperty(value = "身份证号")
    private String idNumber;

    @ExcelProperty(value = "性别", converter = GenderConvert.class)
    private String gender;

    @ExcelProperty("邮箱")
    private String email;

    @ExcelProperty("手机号")
    private String mobile;

    @ExcelProperty("机构ID")
    private String orgId;

    @ExcelProperty(value = "状态", converter = StatusConvert.class)
    private String status;

    @ExcelProperty(value = "超级管理员", converter = WhetherConvert.class)
    private String superAdmin;

    @ExcelProperty(value = "创建时间", converter = DateConverter.class)
    private Date createTime;

    public static List<SysUserExcelVO> convertList(List<SysUser> list) {
        if (CollUtil.isEmpty(list)) {
            return ListUtil.empty();
        }
        return BeanUtil.copyToList(list, SysUserExcelVO.class);
    }
}
