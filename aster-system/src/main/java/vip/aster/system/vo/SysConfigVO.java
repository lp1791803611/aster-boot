package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;
import vip.aster.system.entity.SysConfig;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 参数管理
 *
 * @author Aster
 * @since 2023/12/4 14:56
 */
@Data
@Schema(description = "参数管理")
public class SysConfigVO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8691099868570338090L;

    @Schema(description = "主键")
    private String id;

    @Schema(description = "参数名称")
    private String configName;

    @Schema(description = "参数键名")
    private String configKey;

    @Schema(description = "参数键值")
    private String configValue;

    @Schema(description = "系统内置,'0'-'是','1'-‘否’")
    private String configType;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "备注信息")
    private String remark;

    public SysConfigVO() {
        super();
    }


    public SysConfigVO (SysConfig entity) {
        this.id = entity.getId();
        this.configName = entity.getConfigName();
        this.configKey = entity.getConfigKey();
        this.configValue = entity.getConfigValue();
        this.configType = entity.getConfigType();
        this.createTime = entity.getCreateTime();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

    public static List<SysConfigVO> convertList(List<SysConfig> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysConfigVO> list = new ArrayList<>();
        for (SysConfig config : source) {
            list.add(new SysConfigVO(config));
        }
        return list;
    }
    

}
