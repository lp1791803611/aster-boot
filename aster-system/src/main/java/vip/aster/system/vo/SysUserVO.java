package vip.aster.system.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.framework.security.entity.UserDetail;
import vip.aster.sensitive.annotation.Sensitive;
import vip.aster.sensitive.core.SensitiveStrategy;
import vip.aster.system.entity.SysUser;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户
 *
 * @author Aster
 * @since 2023/11/30 17:21
 */
@Data
@Schema(description = "用户")
public class SysUserVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -7582669725386752060L;

    @Schema(description = "id")
    private String id;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Schema(description = "昵称")
    private String nickName;

    @Schema(description = "头像")
    private String avatar;

    @Schema(description = "性别   0：男   1：女   2：未知")
    private String gender;

    @Schema(description = "邮箱")
    @Sensitive(strategy = SensitiveStrategy.EMAIL)
    private String email;

    @Schema(description = "手机号")
    @Sensitive(strategy = SensitiveStrategy.PHONE)
    private String mobile;

    @Schema(description = "姓名")
    private String realName;

    @Schema(description = "身份证号")
    @Sensitive(strategy = SensitiveStrategy.ID_CARD)
    private String idNumber;

    @Schema(description = "超级管理员   0：是   1：否")
    private String superAdmin;

    @Schema(description = "角色ID列表")
    private List<String> roleIdList;

    @Schema(description = "岗位ID列表")
    private List<String> postIdList;

    @Schema(description = "机构ID")
    private String orgId;

    @Schema(description = "机构名称")
    private String orgName;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "最后登录时间")
    private LocalDateTime lastLoginTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "租户ID")
    private String tenantId;

    @Schema(description = "生日")
    private String birthday;

    @Schema(description = "地区")
    @Sensitive(strategy = SensitiveStrategy.ADDRESS)
    private String area;

    @Schema(description = "签名")
    private String signature;

    public SysUserVO() {
        super();
    }

    public SysUserVO(SysUser entity) {
        this.id = entity.getId();
        this.username = entity.getUsername();
        this.password = entity.getPassword();
        this.nickName = entity.getNickName();
        this.avatar = entity.getAvatar();
        this.gender = entity.getGender();
        this.email = entity.getEmail();
        this.mobile = entity.getMobile();
        this.realName = entity.getRealName();
        this.idNumber = entity.getIdNumber();
        this.superAdmin = entity.getSuperAdmin();
        this.orgId = entity.getOrgId();
        this.createTime = entity.getCreateTime();
        this.lastLoginTime = entity.getLastLoginTime();
        this.status = entity.getStatus();
        this.tenantId = entity.getTenantId();
        this.birthday = entity.getBirthday();
        this.area = entity.getArea();
        this.signature = entity.getSignature();
    }

    public static List<SysUserVO> convertList(List<SysUser> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysUserVO> list = new ArrayList<>();
        for (SysUser entity : source) {
            list.add(new SysUserVO(entity));
        }
        return list;
    }

    public static UserDetail convertDetail(SysUserVO entity) {
        if ( entity == null ) {
            return null;
        }

        UserDetail userDetail = new UserDetail();

        userDetail.setId( entity.getId() );
        userDetail.setUsername( entity.getUsername() );
        userDetail.setPassword( entity.getPassword() );
        userDetail.setRealName( entity.getRealName() );
        userDetail.setAvatar( entity.getAvatar() );
        userDetail.setGender( entity.getGender() );
        userDetail.setEmail( entity.getEmail() );
        userDetail.setMobile( entity.getMobile() );
        userDetail.setOrgId( entity.getOrgId() );
        userDetail.setStatus( entity.getStatus() );
        userDetail.setSuperAdmin( entity.getSuperAdmin() );
        userDetail.setTenantId( entity.getTenantId() );

        return userDetail;
    }
}