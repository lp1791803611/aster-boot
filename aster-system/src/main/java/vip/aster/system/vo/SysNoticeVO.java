package vip.aster.system.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.system.entity.SysNotice;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 通知公告
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-12 11:10:07
 */
@Data
@Schema(description = "通知公告")
public class SysNoticeVO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@Schema(description = "公告ID")
	private String id;
	@Schema(description = "公告标题")
	private String title;
	@Schema(description = "公告类型（1通知 2公告）")
	private String type;
	@Schema(description = "公告内容")
	private String content;
	@Schema(description = "租户ID")
	private String tenantId;
	@Schema(description = "版本号")
	private Integer version;
	@Schema(description = "创建时间")
	private LocalDateTime createTime;
	@Schema(description = "创建者")
	private String createBy;
	@Schema(description = "更新时间")
	private LocalDateTime updateTime;
	@Schema(description = "修改者")
	private String updateBy;
	@Schema(description = "启用状态")
	private String status;
	@Schema(description = "删除状态")
	private String isDeleted;
	@Schema(description = "备注信息")
	private String remark;
	@Schema(description = "发布时间")
	private String publishTime;

	public SysNoticeVO() {
		super();
	}

	public SysNoticeVO(SysNotice entity) {
		this.id = entity.getId();
		this.title = entity.getTitle();
		this.type = entity.getNoticeType();
		this.content = entity.getContent();
		this.tenantId = entity.getTenantId();
		this.version = entity.getVersion();
		this.createTime = entity.getCreateTime();
		this.createBy = entity.getCreateBy();
		this.updateTime = entity.getUpdateTime();
		this.updateBy = entity.getUpdateBy();
		this.status = entity.getStatus();
		this.isDeleted = entity.getIsDeleted();
		this.remark = entity.getRemark();
	}

	public static List<SysNoticeVO> convertList(List<SysNotice> source) {
		if (CollUtil.isEmpty(source)) {
			return ListUtil.empty();
		}
		List<SysNoticeVO> list = new ArrayList<>();
		for (SysNotice entity : source) {
			list.add(new SysNoticeVO(entity));
		}
		return list;
	}

	public SysNotice reconvert() {
		SysNotice entity = new SysNotice();
		entity.setId(this.id);
		entity.setTitle(this.title);
		entity.setNoticeType(this.type);
		entity.setContent(this.content);
		entity.setTenantId(this.tenantId);
		entity.setVersion(this.version);
		entity.setCreateTime(this.createTime);
		entity.setCreateBy(this.createBy);
		entity.setUpdateTime(this.updateTime);
		entity.setUpdateBy(this.updateBy);
		entity.setStatus(this.status);
		entity.setIsDeleted(this.isDeleted);
		entity.setRemark(this.remark);
		return entity;
	}

}