package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.system.entity.SysDictData;
import vip.aster.system.entity.SysOrg;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 组织机构
 *
 * @author Aster
 * @since 2023/12/6 14:52
 */
@Data
@Schema(description = "组织机构")
public class SysOrgVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -4100804976202276169L;

    @Schema(description = "ID")
    private String id;

    @Schema(description = "上级ID")
    private String pid;

    @Schema(description = "机构名称")
    private String orgName;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "备注信息")
    private String remark;

    @Schema(description = "子机构")
    private List<SysOrgVO> children;

    public SysOrgVO() {
        super();
    }

    public SysOrgVO(SysOrg entity) {
        this.id = entity.getId();
        this.pid = entity.getPid();
        this.orgName = entity.getOrgName();
        this.sort = entity.getSort();
        this.createTime = entity.getCreateTime();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

    public static List<SysOrgVO> convertList(List<SysOrg> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysOrgVO> list = new ArrayList<>();
        for (SysOrg org : source) {
            list.add(new SysOrgVO(org));
        }
        return list;
    }
}
