package vip.aster.system.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.system.entity.SysLogLogin;
import vip.aster.system.excel.convert.LoginOperationConvert;
import vip.aster.system.excel.convert.StatusConvert;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 访问日志
 *
 * @author Aster
 * @since 2023/12/7 10:10
 */
@Data
@Schema(description = "访问日志")
public class SysLogLoginVO implements Serializable {

    @Serial
    private static final long serialVersionUID = 9044458227655170758L;

    @ExcelIgnore
    @Schema(description = "ID")
    private String id;

    @ExcelProperty("用户名")
    @Schema(description = "用户名")
    private String username;

    @ExcelProperty("登录IP")
    @Schema(description = "登录IP")
    private String ipAddress;

    @ExcelProperty("登录地点")
    @Schema(description = "登录地点")
    private String loginLocation;

    @ExcelProperty("User Agent")
    @Schema(description = "User Agent")
    private String userAgent;

    @ExcelProperty(value = "登录状态", converter = StatusConvert.class)
    @Schema(description = "登录状态")
    private String status;

    @ExcelProperty(value = "操作信息", converter = LoginOperationConvert.class)
    @Schema(description = "操作信息   0：登录成功   1：退出成功  2：验证码错误  3：账号密码错误")
    private String operation;

    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    @ExcelProperty("访问时间")
    @Schema(description = "访问时间")
    private LocalDateTime loginTime;

    public SysLogLoginVO() {
        super();
    }

    public SysLogLoginVO(SysLogLogin entity) {
        this.id = entity.getId();
        this.username = entity.getUsername();
        this.ipAddress = entity.getIpAddress();
        this.loginLocation = entity.getLoginLocation();
        this.userAgent = entity.getUserAgent();
        this.status = entity.getStatus();
        this.operation = entity.getOperation();
        this.loginTime = entity.getLoginTime();
    }

    public static List<SysLogLoginVO> convertList(List<SysLogLogin> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysLogLoginVO> list = new ArrayList<>();
        for (SysLogLogin login : source) {
            list.add(new SysLogLoginVO(login));
        }
        return list;
    }
}
