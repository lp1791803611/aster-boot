package vip.aster.system.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * SysRoleDataScopeVO
 *
 * @author Aster
 * @since 2023/12/12 11:41
 */
@Data
@Schema(description = "角色机构")
public class SysRoleOrgVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -2848269303703003353L;

    @Schema(description = "id")
    private String id;

    @Schema(description = "数据范围  0：全部数据  1：本机构及子机构数据  2：本机构数据  3：本人数据  4：自定义数据")
    private String dataScope;

    @Schema(description = "机构ID列表")
    private List<String> orgIdList;
}
