package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import vip.aster.system.entity.SysRole;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 角色数据
 *
 * @author Aster
 * @since 2023/12/8 17:19
 */
@Data
@Schema(description = "角色数据")
public class SysRoleVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -5825120847911334310L;

    @Schema(description = "ID")
    private String id;

    @Schema(description = "角色名称")
    private String roleName;

    @Schema(description = "角色代码")
    private String roleCode;

    @Schema(description = "数据范围  0：全部数据  1：本机构及子机构数据  2：本机构数据  3：本人数据  4：自定义数据")
    private String dataScope;

    @Schema(description = "菜单树选择项是否关联显示")
    private String menuCheckStrictly;

    @Schema(description = "机构ID")
    private String orgId;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "备注信息")
    private String remark;

    @Schema(description = "菜单ID列表")
    private List<String> menuIdList;

    @Schema(description = "机构ID列表")
    private List<String> orgIdList;

    public SysRoleVO() {
        super();
    }

    public SysRoleVO(SysRole entity) {
        this.id = entity.getId();
        this.roleName = entity.getRoleName();
        this.roleCode = entity.getRoleCode();
        this.dataScope = entity.getDataScope();
        this.menuCheckStrictly = entity.getMenuCheckStrictly();
        this.orgId = entity.getOrgId();
        this.sort = entity.getSort();
        this.createTime = entity.getCreateTime();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

    public static List<SysRoleVO> convertList(List<SysRole> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysRoleVO> list = new ArrayList<>();
        for (SysRole role : source) {
            list.add(new SysRoleVO(role));
        }
        return list;
    }

}
