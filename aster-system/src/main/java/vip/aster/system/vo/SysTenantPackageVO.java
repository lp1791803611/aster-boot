package vip.aster.system.vo;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.util.StreamUtils;
import vip.aster.system.entity.SysTenantPackage;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 租户套餐
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-26 09:46:32
 */
@Data
@Schema(description = "租户套餐")
public class SysTenantPackageVO implements Serializable {

	@Serial
	private static final long serialVersionUID = -958427100905088708L;

	@Schema(description = "租户套餐id")
	private String id;
	@Schema(description = "套餐名称")
	private String packageName;
	@Schema(description = "关联菜单id")
	private List<String> menuIds;
	@Schema(description = "创建时间")
	private LocalDateTime createTime;
	@Schema(description = "启用状态")
	private String status;
	@Schema(description = "备注")
	private String remark;

	public SysTenantPackageVO() {
		super();
	}

	public SysTenantPackageVO(SysTenantPackage entity) {
		this.id = entity.getId();
		this.packageName = entity.getPackageName();
		if (StrUtil.isNotBlank(entity.getMenuIds())) {
			this.menuIds = Convert.toList(String.class, entity.getMenuIds());
		}
		this.createTime = entity.getCreateTime();
		this.status = entity.getStatus();
		this.remark = entity.getRemark();
	}

	public static List<SysTenantPackageVO> convertList(List<SysTenantPackage> source) {
		if (CollUtil.isEmpty(source)) {
			return ListUtil.empty();
		}
		List<SysTenantPackageVO> list = new ArrayList<>();
		for (SysTenantPackage entity : source) {
			list.add(new SysTenantPackageVO(entity));
		}
		return list;
	}

	public SysTenantPackage reconvert() {
		SysTenantPackage entity = new SysTenantPackage();
		entity.setId(this.id);
		entity.setPackageName(this.packageName);
		if (CollUtil.isNotEmpty(this.menuIds)) {
			entity.setMenuIds(CollUtil.join(this.menuIds, ","));
		}
		entity.setCreateTime(this.createTime);
		entity.setStatus(this.status);
		entity.setRemark(this.remark);
		return entity;
	}

}