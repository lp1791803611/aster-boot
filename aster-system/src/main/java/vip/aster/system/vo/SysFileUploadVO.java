package vip.aster.system.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * SysFileUploadVO
 *
 * @author Aster
 * @since 2024/1/22 16:54
 */
@Data
@Schema(description = "文件上传")
public class SysFileUploadVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -8898247912667770971L;

    @Schema(description = "文件名称")
    private String name;

    @Schema(description = "文件地址")
    private String url;

    @Schema(description = "文件大小")
    private Long size;

    @Schema(description = "存储平台")
    private String platform;
}
