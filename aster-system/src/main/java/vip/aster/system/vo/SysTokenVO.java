package vip.aster.system.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户Token
 *
 * @author Aster
 * @since 2023/11/30 16:49
 */
@Data
@AllArgsConstructor
@Schema(description = "用户登录")
public class SysTokenVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -3430261852935861959L;

    @Schema(description = "access_token")
    private String access_token;
}

