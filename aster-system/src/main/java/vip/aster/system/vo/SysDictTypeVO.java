package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;
import vip.aster.system.entity.SysDictType;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 字典类型
 *
 * @author Aster
 * @since 2023/12/4 16:56
 */
@Data
@Schema(description = "字典类型")
public class SysDictTypeVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -3955161154873802402L;

    @Schema(description = "ID")
    private String id;

    @Schema(description = "字典类型")
    private String dictType;

    @Schema(description = "字典名称")
    private String dictName;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "备注信息")
    private String remark;

    @Schema(description = "字典数据")
    private List<SysDictDataVO> dataList = new ArrayList<>();

    public static List<SysDictTypeVO> convertList(List<SysDictType> types) {
        if (CollUtil.isEmpty(types)) {
            return ListUtil.empty();
        }
        List<SysDictTypeVO> list = new ArrayList<>();
        for (SysDictType type : types) {
            list.add(new SysDictTypeVO(type));
        }
        return list;
    }

    public SysDictType reconvert() {
        SysDictType dictType = new SysDictType();
        dictType.setId(this.id);
        dictType.setDictType(this.dictType);
        dictType.setDictName(this.dictName);
        dictType.setSort(this.sort);
        dictType.setStatus(this.status);
        dictType.setRemark(this.remark);
        return dictType;
    }

    public SysDictTypeVO() {
        super();
    }

    public SysDictTypeVO(SysDictType entity) {
        this.id = entity.getId();
        this.dictType = entity.getDictType();
        this.dictName = entity.getDictName();
        this.sort = entity.getSort();
        this.createTime = entity.getCreateTime();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

}
