package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import vip.aster.common.entity.Query;
import vip.aster.system.entity.SysDictData;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 字典数据
 *
 * @author Aster
 * @since 2023/12/4 16:59
 */
@Data
@Schema(description = "字典数据")
public class SysDictDataVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -7415946884166225911L;

    @Schema(description = "ID")
    private String id;

    @Schema(description = "字典类型ID")
    private String dictTypeId;

    @Schema(description = "字典标签")
    private String dictLabel;

    @Schema(description = "字典值")
    private String dictValue;

    @Schema(description = "标签样式")
    private String labelClass;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "备注信息")
    private String remark;

    public static List<SysDictDataVO> convertList(List<SysDictData> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysDictDataVO> list = new ArrayList<>();
        for (SysDictData data : source) {
            list.add(new SysDictDataVO(data));
        }
        return list;
    }

    public SysDictDataVO() {
        super();
    }

    public SysDictDataVO(SysDictData entity) {
        this.id = entity.getId();
        this.dictTypeId = entity.getDictTypeId();
        this.dictLabel = entity.getDictLabel();
        this.dictValue = entity.getDictValue();
        this.labelClass = entity.getLabelClass();
        this.sort = entity.getSort();
        this.createTime = entity.getCreateTime();
        this.status = entity.getStatus();
        this.remark = entity.getRemark();
    }

    public SysDictData reconvert() {
        SysDictData dictData = new SysDictData();
        dictData.setId(this.id);
        dictData.setDictTypeId(this.dictTypeId);
        dictData.setDictLabel(this.dictLabel);
        dictData.setDictValue(this.dictValue);
        dictData.setSort(this.sort);
        dictData.setStatus(this.status);
        dictData.setRemark(this.remark);
        dictData.setLabelClass(this.labelClass);
        return dictData;
    }
}
