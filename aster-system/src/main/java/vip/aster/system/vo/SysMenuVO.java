package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.system.entity.SysMenu;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * SysMenuVO
 *
 * @author Aster
 * @since 2023/12/6 17:06
 */
@Data
@Schema(description = "菜单数据")
public class SysMenuVO implements Serializable {
    @Serial
    private static final long serialVersionUID = 1222095295029742854L;

    @Schema(description = "ID")
    private String id;

    @Schema(description = "上级ID，一级菜单为0")
    private String pid;

    @Schema(description = "菜单名称")
    private String name;

    @Schema(description = "菜单URL")
    private String path;

    @Schema(description = "组件地址")
    private String component;

    @Schema(description = "授权标识(多个用逗号分隔，如：sys:menu:list,sys:menu:save)")
    private String perms;

    @Schema(description = "类型   0：目录   1：菜单   2：按钮")
    private String type;

    @Schema(description = "打开方式   0：内部   1：外部")
    private Integer openStyle;

    @Schema(description = "菜单图标")
    private String icon;

    @Schema(description = "是否缓存")
    private String isKeepAlive;

    @Schema(description = "是否固定")
    private String isAffix;

    @Schema(description = "是否全屏")
    private String isFull;

    @Schema(description = "是否隐藏")
    private String isHide;

    @Schema(description = "菜单排序")
    private Integer sort;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "启用状态")
    private String status;

    @Schema(description = "备注信息")
    private String remark;

    @Schema(description = "子菜单")
    private List<SysMenuVO> children;

    public SysMenuVO() {
        super();
    }

    public SysMenuVO(SysMenu entity) {
        if (entity != null) {
            this.id = entity.getId();
            this.pid = entity.getPid();
            this.name = entity.getName();
            this.path = entity.getMenuPath();
            this.component = entity.getComponent();
            this.perms = entity.getPerms();
            this.type = entity.getMenuType();
            this.icon = entity.getIcon();
            this.openStyle = entity.getOpenStyle();
            this.isKeepAlive = entity.getIsKeepAlive();
            this.isAffix = entity.getIsAffix();
            this.isFull = entity.getIsFull();
            this.isHide = entity.getIsHide();
            this.status = entity.getStatus();
            this.sort = entity.getSort();
            this.createTime = entity.getCreateTime();
            this.remark = entity.getRemark();
        }
    }

    public static List<SysMenuVO> convertList(List<SysMenu> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        List<SysMenuVO> list = new ArrayList<>();
        for (SysMenu menu : source) {
            list.add(new SysMenuVO(menu));
        }
        return list;
    }

    public SysMenu reconvert() {
        SysMenu sysMenu = new SysMenu();
        sysMenu.setId(this.id);
        sysMenu.setPid(this.pid);
        sysMenu.setName(this.name);
        sysMenu.setMenuPath(this.path);
        sysMenu.setComponent(this.component);
        sysMenu.setPerms(this.perms);
        sysMenu.setMenuType(this.type);
        sysMenu.setIcon(this.icon);
        sysMenu.setOpenStyle(this.openStyle);
        sysMenu.setIsKeepAlive(this.isKeepAlive);
        sysMenu.setIsAffix(this.isAffix);
        sysMenu.setIsFull(this.isFull);
        sysMenu.setIsHide(this.isHide);
        sysMenu.setStatus(this.status);
        sysMenu.setSort(this.sort);
        sysMenu.setRemark(this.remark);
        return sysMenu;
    }
}
