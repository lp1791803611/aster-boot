package vip.aster.system.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 账号登录
 *
 * @author Aster
 * @since 2023/11/30 16:50
 */
@Data
@Schema(description = "账号登录")
public class SysAccountLoginVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -8401158244046633338L;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    private String password;

    @Schema(description = "唯一key")
    private String key;

    @Schema(description = "验证码")
    private String captcha;

    @Schema(description = "租户ID")
    private String tenantId;
}
