package vip.aster.system.vo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vip.aster.system.entity.SysLogOperate;
import vip.aster.system.excel.convert.BusinessTypeConvert;
import vip.aster.system.excel.convert.StatusConvert;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 操作日志
 *
 * @author Aster
 * @since 2023/12/7 16:40
 */
@Data
@Schema(description = "操作日志")
public class SysLogOperateVO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8822863894057080670L;

    @ExcelIgnore
    @Schema(description = "id")
    private String id;

    @ExcelProperty("模块名")
    @Schema(description = "模块名")
    private String module;

    @ExcelProperty("操作名")
    @Schema(description = "操作名")
    private String name;

    @ExcelProperty("请求URI")
    @Schema(description = "请求URI")
    private String requestUri;

    @ExcelProperty("请求方法")
    @Schema(description = "请求方法")
    private String requestMethod;

    @ExcelProperty("请求参数")
    @Schema(description = "请求参数")
    private String requestParams;

    @ExcelProperty("操作IP")
    @Schema(description = "操作IP")
    private String ip;

    @ExcelProperty("登录地点")
    @Schema(description = "登录地点")
    private String address;

    @ExcelProperty("User Agent")
    @Schema(description = "User Agent")
    private String userAgent;

    @ExcelProperty(value = "操作类型", converter = BusinessTypeConvert.class)
    @Schema(description = "操作类型")
    private String businessType;

    @ExcelProperty("执行时长")
    @Schema(description = "执行时长")
    private Integer duration;

    @ExcelProperty(value = "操作状态", converter = StatusConvert.class)
    @Schema(description = "操作状态")
    private String status;

    @ExcelProperty("操作人")
    @Schema(description = "操作人")
    private String username;

    @ExcelProperty("返回消息")
    @Schema(description = "返回消息")
    private String resultMsg;

    @ExcelProperty("操作时间")
    @Schema(description = "操作时间")
    private LocalDateTime operTime;

    public SysLogOperateVO() {
        super();
    }

    public SysLogOperateVO(SysLogOperate operate) {
        this.id = operate.getId();
        this.module = operate.getModule();
        this.name = operate.getName();
        this.requestUri = operate.getRequestUri();
        this.requestMethod = operate.getRequestMethod();
        this.requestParams = operate.getRequestParams();
        this.ip = operate.getIp();
        this.address = operate.getAddress();
        this.userAgent = operate.getUserAgent();
        this.businessType = operate.getBusinessType();
        this.duration = operate.getDuration();
        this.status = operate.getStatus();
        this.username = operate.getUsername();
        this.resultMsg = operate.getResultMsg();
        this.operTime = operate.getOperTime();
    }

    public static List<SysLogOperateVO> convertList(List<SysLogOperate> source) {
        if (CollUtil.isEmpty(source)) {
            return ListUtil.empty();
        }
        return BeanUtil.copyToList(source, SysLogOperateVO.class);
    }
}
