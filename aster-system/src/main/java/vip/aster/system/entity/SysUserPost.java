package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户岗位关系
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user_post")
@Schema(description = "用户岗位关系")
public class SysUserPost extends Model<SysUserPost> {

    @Serial
    private static final long serialVersionUID = -1017141560817292772L;

    @Schema(description = "用户ID")
    @TableId("user_id")
    private String userId;

    @Schema(description = "岗位ID")
    @TableField("post_id")
    private String postId;

    @Schema(description = "版本号")
    @Version
    @TableField(value = "version", fill = FieldFill.INSERT)
    private Integer version;

    @Override
    public Serializable pkVal() {
        return this.userId;
    }

}
