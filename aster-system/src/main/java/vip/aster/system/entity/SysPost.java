package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 岗位
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_post")
@Schema(description = "岗位")
public class SysPost extends BaseEntity<SysPost> {

    @Serial
    private static final long serialVersionUID = -5531270127157960504L;

    @Schema(description = "ID")
    @TableId("id")
    private String id;

    @Schema(description = "岗位编码")
    @TableField("post_code")
    private String postCode;

    @Schema(description = "岗位名称")
    @TableField("post_name")
    private String postName;

    @Schema(description = "排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "启用状态")
    @TableField("status")
    private String status;

    @Schema(description = "备注信息")
    @TableField("remark")
    private String remark;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
