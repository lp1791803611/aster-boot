package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 角色菜单关系
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role_menu")
@Schema(description = "角色菜单关系")
public class SysRoleMenu extends Model<SysRoleMenu> {

    @Serial
    private static final long serialVersionUID = 7767207877692187740L;

    @Schema(description = "roleId")
    @TableId("role_id")
    private String roleId;

    @Schema(description = "menuId")
    @TableField("menu_id")
    private String menuId;

    @Schema(description = "版本号")
    @Version
    @TableField(value = "version", fill = FieldFill.INSERT)
    private Integer version;


    @Override
    public Serializable pkVal() {
        return this.roleId;
    }

}
