package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 机构管理
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_org")
@Schema(description = "机构管理")
public class SysOrg extends BaseEntity<SysOrg> {

    @Serial
    private static final long serialVersionUID = 5083315035593960750L;

    @Schema(description = "ID")
    @TableId("id")
    private String id;

    @Schema(description = "上级ID")
    @TableField("pid")
    private String pid;

    @Schema(description = "机构名称")
    @TableField("org_name")
    private String orgName;

    @Schema(description = "排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "启用状态")
    @TableField("status")
    private String status;

    @Schema(description = "备注信息")
    @TableField("remark")
    private String remark;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
