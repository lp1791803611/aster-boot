package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 通知公告
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-12 11:34:34
 */
@EqualsAndHashCode(callSuper=false)
@Data
@TableName("sys_notice")
public class SysNotice extends BaseEntity<SysNotice> {

	@Serial
	private static final long serialVersionUID = 1760260375928387807L;

	/**
	 * 公告ID
	 */
	@TableId("id")
	private String id;
	/**
	 * 公告标题
	 */
	@TableField("title")
	private String title;
	/**
	 * 公告类型（1通知 2公告）
	 */
	@TableField("notice_type")
	private String noticeType;
	/**
	 * 公告内容
	 */
	@TableField("content")
	private String content;
	/**
	 * 租户ID
	 */
	@TableField("tenant_id")
	private String tenantId;
	/**
	 * 启用状态
	 */
	@TableField("status")
	private String status;
	/**
	 * 备注信息
	 */
	@TableField("remark")
	private String remark;
}