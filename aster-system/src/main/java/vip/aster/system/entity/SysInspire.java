package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 激励
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_inspire")
@Schema(description = "激励")
public class SysInspire extends Model<SysInspire> {

    @Serial
    private static final long serialVersionUID = 3499597749956298442L;
    @Schema(description = "ID")
    @TableId("id")
    private String id;

    @Schema(description = "内容")
    @TableField("content")
    private String content;

    @Schema(description = "点赞数")
    @TableField("like_num")
    private Integer likeNum;

    @Schema(description = "狂踩数")
    @TableField("dislike_num")
    private Integer dislikeNum;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
