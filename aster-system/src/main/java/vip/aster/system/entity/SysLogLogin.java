package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 访问日志
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_log_login")
@Schema(description = "访问日志")
public class SysLogLogin implements Serializable {

    @Serial
    private static final long serialVersionUID = -3554029305029537738L;

    @Schema(description = "ID")
    @TableId("id")
    private String id;

    @Schema(description = "用户ID")
    @TableField("user_id")
    private String userId;

    @Schema(description = "用户名")
    @TableField("username")
    private String username;

    @Schema(description = "登录IP")
    @TableField("ip_address")
    private String ipAddress;

    @Schema(description = "登录地点")
    @TableField("login_location")
    private String loginLocation;

    @Schema(description = "User Agent")
    @TableField("user_agent")
    private String userAgent;

    @Schema(description = "登录状态")
    @TableField("status")
    private String status;

    @Schema(description = "操作信息   0：登录成功   1：退出成功  2：验证码错误  3：账号密码错误")
    @TableField("operation")
    private String operation;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "访问时间")
    @TableField("login_time")
    private LocalDateTime loginTime;
}
