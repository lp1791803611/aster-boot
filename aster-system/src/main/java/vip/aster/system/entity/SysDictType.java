package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 字典类型
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dict_type")
@Schema(description = "字典类型")
public class SysDictType extends BaseEntity<SysDictType> {

    @Serial
    private static final long serialVersionUID = -4392857261811035581L;

    @Schema(description = "ID")
    @TableId("id")
    private String id;

    @Schema(description = "字典类型")
    @TableField("dict_type")
    private String dictType;

    @Schema(description = "字典名称")
    @TableField("dict_name")
    private String dictName;

    @Schema(description = "排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "启用状态")
    @TableField("status")
    private String status;

    @Schema(description = "备注信息")
    @TableField("remark")
    private String remark;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
