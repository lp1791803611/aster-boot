package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
@Schema(description = "用户")
public class SysUser extends BaseEntity<SysUser> {

    @Serial
    private static final long serialVersionUID = 6078744010644727166L;

    @Schema(description = "id")
    @TableId("id")
    private String id;

    @Schema(description = "用户名")
    @TableField("username")
    private String username;

    @Schema(description = "密码")
    @TableField("password")
    private String password;

    @Schema(description = "机构ID")
    @TableField("org_id")
    private String orgId;

    @Schema(description = "昵称")
    @TableField("nick_name")
    private String nickName;

    @Schema(description = "头像")
    @TableField("avatar")
    private String avatar;

    @Schema(description = "性别   0：男   1：女   2：未知")
    @TableField("gender")
    private String gender;

    @Schema(description = "邮箱")
    @TableField("email")
    private String email;

    @Schema(description = "手机号")
    @TableField("mobile")
    private String mobile;

    @Schema(description = "姓名")
    @TableField("real_name")
    private String realName;

    @Schema(description = "身份证号")
    @TableField("id_number")
    private String idNumber;

    @Schema(description = "超级管理员   0：是   1：否")
    @TableField(value = "super_admin", updateStrategy = FieldStrategy.NEVER)
    private String superAdmin;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "登录次数")
    @TableField("login_number")
    private Long loginNumber;

    @Schema(description = "最后登录时间")
    @TableField("last_login_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime lastLoginTime;

    @Schema(description = "手机验证码")
    @TableField("mobile_verification_code")
    private String mobileVerificationCode;

    @Schema(description = "邮箱验证码")
    @TableField("email_verification_code")
    private String emailVerificationCode;

    @Schema(description = "密钥")
    @TableField("salt")
    private String salt;

    @Schema(description = "生日")
    @TableField("birthday")
    private String birthday;

    @Schema(description = "地区")
    @TableField("area")
    private String area;

    @Schema(description = "签名")
    @TableField("signature")
    private String signature;

    @Schema(description = "启用状态")
    @TableField("status")
    private String status;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
