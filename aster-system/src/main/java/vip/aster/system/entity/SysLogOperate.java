package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_log_operate")
@Schema(description="操作日志")
public class SysLogOperate extends Model<SysLogOperate> {

    @Serial
    private static final long serialVersionUID = 3936079089020495863L;

    @Schema(description = "id")
    @TableId("id")
    private String id;

    @Schema(description = "模块名")
    @TableField("module")
    private String module;

    @Schema(description = "操作名")
    @TableField("name")
    private String name;

    @Schema(description = "请求URI")
    @TableField("request_uri")
    private String requestUri;

    @Schema(description = "请求方法")
    @TableField("request_method")
    private String requestMethod;

    @Schema(description = "请求参数")
    @TableField("request_params")
    private String requestParams;

    @Schema(description = "操作IP")
    @TableField("ip")
    private String ip;

    @Schema(description = "登录地点")
    @TableField("address")
    private String address;

    @Schema(description = "User Agent")
    @TableField("user_agent")
    private String userAgent;

    @Schema(description = "操作类型")
    @TableField("business_type")
    private String businessType;

    @Schema(description = "执行时长")
    @TableField("duration")
    private Integer duration;

    @Schema(description = "操作状态")
    @TableField("status")
    private String status;

    @Schema(description = "用户ID")
    @TableField("user_id")
    private String userId;

    @Schema(description = "操作人")
    @TableField("username")
    private String username;

    @Schema(description = "返回消息")
    @TableField("result_msg")
    private String resultMsg;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "操作时间")
    @TableField("oper_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime operTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
