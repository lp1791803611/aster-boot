package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 角色机构关系
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role_org")
@Schema(description = "角色机构关系")
public class SysRoleOrg extends Model<SysRoleOrg> {

    @Serial
    private static final long serialVersionUID = 3362352222115217489L;

    @Schema(description = "角色ID")
    @TableId("role_id")
    private String roleId;

    @Schema(description = "机构ID")
    @TableField("org_id")
    private String orgId;

    @Schema(description = "版本号")
    @Version
    @TableField(value = "version", fill = FieldFill.INSERT)
    private Integer version;

    @Override
    public Serializable pkVal() {
        return this.roleId;
    }

}
