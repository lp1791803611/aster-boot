package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户角色关系
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user_role")
@Schema(description="用户角色关系")
public class SysUserRole extends Model<SysUserRole> {

    @Serial
    private static final long serialVersionUID = -153704336630857942L;

    @Schema(description = "用户ID")
    @TableId("user_id")
    private String userId;

    @Schema(description = "角色ID")
    @TableField("role_id")
    private String roleId;

    @Schema(description = "版本号")
    @Version
    @TableField(value = "version", fill = FieldFill.INSERT)
    private Integer version;

    @Override
    public Serializable pkVal() {
        return this.userId;
    }

}
