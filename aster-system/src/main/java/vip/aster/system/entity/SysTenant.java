package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.time.LocalDateTime;

import java.io.Serial;

/**
 * 租户
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-25 14:46:59
 */
@EqualsAndHashCode(callSuper=false)
@Data
@TableName("sys_tenant")
public class SysTenant extends BaseEntity<SysTenant> {

	@Serial
	private static final long serialVersionUID = 6507856838023156305L;

	/**
	 * id
	 */
	@TableId("id")
	private String id;
	/**
	 * 联系人
	 */
	@TableField("contact_user_name")
	private String contactUserName;
	/**
	 * 联系电话
	 */
	@TableField("contact_phone")
	private String contactPhone;
	/**
	 * 企业名称
	 */
	@TableField("company_name")
	private String companyName;
	/**
	 * 统一社会信用代码
	 */
	@TableField("license_number")
	private String licenseNumber;
	/**
	 * 地址
	 */
	@TableField("address")
	private String address;
	/**
	 * 企业简介
	 */
	@TableField("intro")
	private String intro;
	/**
	 * 域名
	 */
	@TableField("tenant_domain")
	private String tenantDomain;
	/**
	 * 租户套餐编号
	 */
	@TableField("package_id")
	private String packageId;
	/**
	 * 过期时间
	 */
	@TableField("expire_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime expireTime;
	/**
	 * 用户数量（-1不限制）
	 */
	@TableField("account_count")
	private Integer accountCount;
	/**
	 * 启用状态
	 */
	@TableField("status")
	private String status;
	/**
	 * 备注
	 */
	@TableField("remark")
	private String remark;
}