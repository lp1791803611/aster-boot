package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 角色
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role")
@Schema(description="角色")
public class SysRole extends BaseEntity<SysRole> {

    @Serial
    private static final long serialVersionUID = 7742376683975994051L;

    @Schema(description = "ID")
    @TableId("id")
    private String id;

    @Schema(description = "角色名称")
    @TableField("role_name")
    private String roleName;

    @Schema(description = "角色代码")
    @TableField("role_code")
    private String roleCode;

    @Schema(description = "数据范围  0：全部数据  1：本机构及子机构数据  2：本机构数据  3：本人数据  4：自定义数据")
    @TableField("data_scope")
    private String dataScope;

    @Schema(description = "菜单树选择项是否关联显示")
    @TableField(value = "menu_check_strictly")
    private String menuCheckStrictly;

    @Schema(description = "机构ID")
    @TableField("org_id")
    private String orgId;

    @Schema(description = "排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "启用状态")
    @TableField("status")
    private String status;

    @Schema(description = "备注信息")
    @TableField("remark")
    private String remark;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
