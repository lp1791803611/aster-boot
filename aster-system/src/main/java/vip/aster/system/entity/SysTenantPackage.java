package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.time.LocalDateTime;

import java.io.Serial;

/**
 * 租户套餐
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-26 09:46:32
 */
@EqualsAndHashCode(callSuper=false)
@Data
@TableName("sys_tenant_package")
public class SysTenantPackage extends BaseEntity<SysTenantPackage> {

	@Serial
	private static final long serialVersionUID = 7072649735635076639L;

	/**
	 * 租户套餐id
	 */
	@TableId("id")
	private String id;
	/**
	 * 套餐名称
	 */
	@TableField("package_name")
	private String packageName;
	/**
	 * 关联菜单id
	 */
	@TableField("menu_ids")
	private String menuIds;
	/**
	 * 启用状态
	 */
	@TableField("status")
	private String status;
	/**
	 * 备注
	 */
	@TableField("remark")
	private String remark;
}