package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * 菜单
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
@Schema(description = "菜单")
public class SysMenu extends BaseEntity<SysMenu> {

    @Serial
    private static final long serialVersionUID = -672994284842033657L;

    @Schema(description = "ID")
    @TableId("id")
    private String id;

    @Schema(description = "上级ID，一级菜单为0")
    @TableField("pid")
    private String pid;

    @Schema(description = "菜单名称")
    @TableField("name")
    private String name;

    @Schema(description = "菜单URL")
    @TableField("menu_path")
    private String menuPath;

    @Schema(description = "组件地址")
    @TableField("component")
    private String component;

    @Schema(description = "授权标识(多个用逗号分隔，如：sys:menu:list,sys:menu:save)")
    @TableField("perms")
    private String perms;

    @Schema(description = "类型   0：目录   1：菜单   2：按钮")
    @TableField("menu_type")
    private String menuType;

    @Schema(description = "打开方式   0：内部   1：外部")
    @TableField("open_style")
    private Integer openStyle;

    @Schema(description = "菜单图标")
    @TableField("icon")
    private String icon;

    @Schema(description = "是否缓存")
    @TableField("is_keep_alive")
    private String isKeepAlive;

    @Schema(description = "是否固定")
    @TableField("is_affix")
    private String isAffix;

    @Schema(description = "是否全屏")
    @TableField("is_full")
    private String isFull;

    @Schema(description = "是否隐藏")
    @TableField("is_hide")
    private String isHide;

    @Schema(description = "菜单排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "启用状态")
    @TableField("status")
    private String status;

    @Schema(description = "备注信息")
    @TableField("remark")
    private String remark;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
