package vip.aster.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 系统配置
 *
 * @author Aster
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_config")
@Schema(description = "系统配置")
public class SysConfig extends BaseEntity<SysConfig> {

    @Serial
    private static final long serialVersionUID = -2053517918840961011L;

    @Schema(description = "主键")
    @TableId("id")
    private String id;

    @Schema(description = "参数名称")
    @TableField("config_name")
    private String configName;

    @Schema(description = "参数键名")
    @TableField("config_key")
    private String configKey;

    @Schema(description = "参数键值")
    @TableField("config_value")
    private String configValue;

    @Schema(description = "系统内置,'0'-'是','1'-‘否’")
    @TableField("config_type")
    private String configType;

    @Schema(description = "租户ID")
    @TableField("tenant_id")
    private String tenantId;

    @Schema(description = "启用状态")
    @TableField("status")
    private String status;

    @Schema(description = "备注信息")
    @TableField("remark")
    private String remark;

}
