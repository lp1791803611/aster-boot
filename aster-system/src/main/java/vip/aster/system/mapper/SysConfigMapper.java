package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysConfig;

/**
 * 系统配置 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
