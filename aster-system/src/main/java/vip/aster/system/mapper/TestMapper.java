package vip.aster.system.mapper;

import org.apache.ibatis.annotations.Param;
import vip.aster.framework.mybatis.annotation.DataColumn;
import vip.aster.framework.mybatis.annotation.DataPermission;
import vip.aster.framework.mybatis.enums.ColumnTypeEnum;
import vip.aster.system.entity.*;

import java.util.List;

/**
 * TestMapper
 *
 * @author Aster
 * @since 2024/3/22 11:38
 */
public interface TestMapper {

    @DataPermission({
            @DataColumn(value = "pid", type = ColumnTypeEnum.DEPT)
    })
    List<SysOrg> orgList();

    @DataPermission({
            @DataColumn(value = "user_id", type = ColumnTypeEnum.USER)
    })
    List<SysLogOperate> operateList();

    @DataPermission({
            @DataColumn(alias = "t1", value = "org_id", type = ColumnTypeEnum.DEPT),
            @DataColumn(alias = "t1", value = "create_by", type = ColumnTypeEnum.USER)
    })
    List<SysUser> userList();

    @DataPermission({
            @DataColumn(alias = "t1", value = "org_id", type = ColumnTypeEnum.DEPT),
            @DataColumn(alias = "t1", value = "create_by", type = ColumnTypeEnum.USER)
    })
    void updateUser(@Param("email") String email);
}
