package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysInspire;

/**
 * SysInspireMapper
 *
 * @author Aster
 * @since 2024/1/19 16:14
 */
public interface SysInspireMapper extends BaseMapper<SysInspire> {
}
