package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.aster.system.entity.SysMenu;

import java.util.List;

/**
 * 菜单 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据用户ID获取菜单列表
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    List<String> getUserAuthorityList(@Param("userId") String userId);

    /**
     * 查询用户菜单列表
     * @param userId 用户id
     * @param types 菜单类型
     * @return 菜单
     */
    List<SysMenu> getUserMenuList(@Param("userId") String userId, @Param("types") List<String> types);
}
