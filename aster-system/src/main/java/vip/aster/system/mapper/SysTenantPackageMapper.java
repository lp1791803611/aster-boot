package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysTenantPackage;

/**
 * 租户套餐 Mapper 接口
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-26 09:46:32
 */
public interface SysTenantPackageMapper extends BaseMapper<SysTenantPackage> {
	
}