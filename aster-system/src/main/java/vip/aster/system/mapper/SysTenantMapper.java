package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysTenant;

/**
 * 租户 Mapper 接口
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-25 14:46:59
 */
public interface SysTenantMapper extends BaseMapper<SysTenant> {
	
}