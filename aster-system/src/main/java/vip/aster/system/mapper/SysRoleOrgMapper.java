package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.aster.system.entity.SysRoleOrg;

import java.util.List;

/**
 * 角色机构关系 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysRoleOrgMapper extends BaseMapper<SysRoleOrg> {

    /**
     * 获取用户的数据权限列表
     *
     * @param userId 用户ID
     * @return 机构ID列表
     */
    List<String> getDataScopeList(@Param("userId") String userId);
}
