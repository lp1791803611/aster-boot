package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysOrg;

/**
 * 机构管理 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysOrgMapper extends BaseMapper<SysOrg> {

}
