package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysLogOperate;

/**
 * 操作日志 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysLogOperateMapper extends BaseMapper<SysLogOperate> {

}
