package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysDictData;

/**
 * 字典数据 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

}
