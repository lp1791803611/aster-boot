package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import vip.aster.system.entity.SysRole;

import java.util.List;

/**
 * 角色 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<String> getDataScopeByUserId(@Param("userId") String userId);

    List<String> getUserRoleCode(@Param("userId") String id);
}
