package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysUserPost;

/**
 * 用户岗位关系 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {

}
