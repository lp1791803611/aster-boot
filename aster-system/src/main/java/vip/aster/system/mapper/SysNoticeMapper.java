package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysNotice;

/**
 * 通知公告 Mapper 接口
 *
 * @author Aster lipian1004@163.com
 * @since 2024-03-12 11:10:07
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {
	
}