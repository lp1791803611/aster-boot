package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysLogLogin;

/**
 * 访问日志 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysLogLoginMapper extends BaseMapper<SysLogLogin> {

}
