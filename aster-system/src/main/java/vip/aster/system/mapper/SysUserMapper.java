package vip.aster.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.aster.system.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * 用户 Mapper 接口
 *
 * @author Aster
 * @since 2023-11-28
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据角色获取用户
     * @param params 查询条件
     * @return 用户
     */
    List<SysUser> getRoleUserList(Map<String, Object> params);
}
