package vip.aster.system.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import vip.aster.system.entity.SysDictData;

import java.util.List;

/**
 * 转换工具类
 *
 * @author Aster
 * @since 2023/12/7 15:21
 */
public class ConvertUtils {

    public static String convertToJavaData(String dictType, ReadCellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        String value = "";
        String str = cellData.getStringValue();
        List<SysDictData> sysDictDataList = DictUtils.getDictCache(dictType);
        if (CollUtil.isNotEmpty(sysDictDataList)) {
            SysDictData sysDictData = sysDictDataList.stream().filter(s -> StrUtil.equals(s.getDictLabel(), str)).findFirst().orElse(null);
            if (sysDictData != null) {
                value = sysDictData.getDictValue();
            }
        }
        return value;
    }

    public static WriteCellData<String> convertToExcelData(String dictType, String value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        String str = "未知";
        List<SysDictData> sysDictDataList = DictUtils.getDictCache(dictType);
        if (CollUtil.isNotEmpty(sysDictDataList)) {
            SysDictData sysDictData = sysDictDataList.stream().filter(s -> StrUtil.equals(s.getDictValue(), value)).findFirst().orElse(null);
            if (sysDictData != null) {
                str = sysDictData.getDictLabel();
            }
        }
        return new WriteCellData<>(str);
    }
}
