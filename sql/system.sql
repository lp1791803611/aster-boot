/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯云服务器
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 43.143.209.252:3306
 Source Schema         : aster_boot

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 06/01/2025 11:11:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '主键',
    `config_name`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '参数名称',
    `config_key`   varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '参数键名',
    `config_value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '参数键值',
    `config_type`  varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '系统内置,\'0\'-\'是\',\'1\'-‘否’',
    `tenant_id`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`      int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time`  datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time`  datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`       varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`       varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '系统配置'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config`
VALUES ('1731967769932730369', '用户登录-验证码开关', 'sys-captcha', 'true', '0', '000000', 0, '2023-12-05 17:24:29',
        '1', '2024-03-27 15:01:10', '1', '0', '0', '');
INSERT INTO `sys_config`
VALUES ('1731973274830901249', '是否开启用户注册', 'sys-register', 'false', '0', '000000', 0, '2023-12-05 17:46:22',
        '1', '2024-03-27 15:01:10', '1', '0', '0', '');
INSERT INTO `sys_config`
VALUES ('1731973864961085442', '账号初始密码', 'sys-password', '123456', '0', '000000', 0, '2023-12-05 17:48:43', '1',
        '2024-03-27 15:01:10', '1', '0', '0', '');
INSERT INTO `sys_config`
VALUES ('1752200846047047681', '密码错误次数', 'pwd_err_cnt', '5', '1', '000000', 0, '2024-01-30 13:23:31', '1',
        '2024-04-11 11:10:12', '1', '0', '0', '');
INSERT INTO `sys_config`
VALUES ('1772916752603414530', '用户登录-验证码开关', 'sys-captcha', 'true', '0', '042771', 0, '2023-12-05 17:24:29',
        '1', '2024-03-27 15:01:10', '1', '0', '0', '');
INSERT INTO `sys_config`
VALUES ('1772916752691494914', '是否开启用户注册', 'sys-register', 'false', '0', '042771', 0, '2023-12-05 17:46:22',
        '1', '2024-03-27 15:01:10', '1', '0', '0', '');
INSERT INTO `sys_config`
VALUES ('1772916752733437954', '账号初始密码', 'sys-password', '123456', '0', '042771', 0, '2023-12-05 17:48:43', '1',
        '2024-03-27 15:01:10', '1', '0', '0', '');
INSERT INTO `sys_config`
VALUES ('1772916752758603778', '密码错误次数', 'pwd_err_cnt', '5', '0', '042771', 0, '2024-01-30 13:23:31', '1',
        '2024-03-27 15:01:10', '1', '0', '0', '');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `dict_type_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '字典类型ID',
    `dict_label`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典标签',
    `dict_value`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典值',
    `label_class`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签样式',
    `sort`         int(4)                                                        NULL DEFAULT NULL COMMENT '排序',
    `tenant_id`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`      int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time`  datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time`  datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`       varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`       varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '字典数据'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data`
VALUES ('1747161724098007042', '1747156982101737474', '启用', '0', 'primary', 1, '000000', 0, '2024-01-16 15:39:50',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747161781291536386', '1747156982101737474', '停用', '1', 'info', 2, '000000', 0, '2024-01-16 15:40:04', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747163240120799233', '1747159762342281217', '登录成功', '0', 'success', 1, '000000', 0, '2024-01-16 15:45:52',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747163474825662466', '1747159762342281217', '退出成功', '1', 'info', 2, '000000', 0, '2024-01-16 15:46:48',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747163582204039170', '1747159762342281217', '验证码错误', '2', 'warning', 3, '000000', 0,
        '2024-01-16 15:47:13', '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747163668296323073', '1747159762342281217', '账号密码错误', '3', 'danger', 4, '000000', 0,
        '2024-01-16 15:47:34', '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747164022966669314', '1747160739174764545', '是', '0', 'primary', 1, '000000', 0, '2024-01-16 15:48:58', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747164082513203202', '1747160739174764545', '否', '1', 'info', 2, '000000', 0, '2024-01-16 15:49:13', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747167122800594945', '1747166651880919042', '男', '0', 'primary', 1, '000000', 0, '2024-01-16 16:01:18', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747167157428768770', '1747166651880919042', '女', '1', 'warning', 2, '000000', 0, '2024-01-16 16:01:26', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747167211430432770', '1747166651880919042', '未知', '2', 'info', 3, '000000', 0, '2024-01-16 16:01:39', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747176977930211330', '1747170511718535169', '查询', '1', 'primary', 1, '000000', 0, '2024-01-16 16:40:27',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177023023173633', '1747170511718535169', '保存', '2', 'primary', 2, '000000', 0, '2024-01-16 16:40:38',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177060641886209', '1747170511718535169', '修改', '3', 'primary', 3, '000000', 0, '2024-01-16 16:40:47',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177106615652354', '1747170511718535169', '删除', '4', 'danger', 4, '000000', 0, '2024-01-16 16:40:58', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177139654184962', '1747170511718535169', '授权', '5', 'success', 5, '000000', 0, '2024-01-16 16:41:06',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177175385460737', '1747170511718535169', '导出', '6', 'primary', 6, '000000', 0, '2024-01-16 16:41:14',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177208419799042', '1747170511718535169', '导入', '7', 'primary', 7, '000000', 0, '2024-01-16 16:41:22',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177243568066562', '1747170511718535169', '强退', '8', 'warning', 8, '000000', 0, '2024-01-16 16:41:31',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177280494718977', '1747170511718535169', '清空', '9', 'warning', 9, '000000', 0, '2024-01-16 16:41:39',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177326082609154', '1747170511718535169', '上传', '10', 'primary', 10, '000000', 0, '2024-01-16 16:41:50',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177366972878849', '1747170511718535169', '下载', '11', 'primary', 11, '000000', 0, '2024-01-16 16:42:00',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747177449575501825', '1747170511718535169', '其它', '99', 'info', 99, '000000', 0, '2024-01-16 16:42:20', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747920929222348801', '1747920455324717057', '目录', '0', 'success', 1, '000000', 0, '2024-01-18 17:56:39',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747920986772393985', '1747920455324717057', '菜单', '1', 'primary', 2, '000000', 0, '2024-01-18 17:56:53',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747921048688709634', '1747920455324717057', '按钮', '2', 'info', 3, '000000', 0, '2024-01-18 17:57:07', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747921707571924994', '1747921567675109377', '启用', '0', 'primary', 1, '000000', 0, '2024-01-18 17:59:45',
        '1', '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747921768028622850', '1747921567675109377', '禁用', '1', 'danger', 2, '000000', 0, '2024-01-18 17:59:59', '1',
        '2024-03-27 14:40:26', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747922236888895489', '1747922159625621506', '失败', '1', 'danger', 2, '000000', 0, '2024-01-18 18:01:51', '1',
        '2024-04-10 17:19:01', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1747922291460984833', '1747922159625621506', '成功', '0', 'success', 1, '000000', 0, '2024-01-18 18:02:04',
        '1', '2024-04-10 17:18:53', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1750709806051909634', '1750709670844325890', '全部数据', '0', 'primary', 1, '000000', 0, '2024-01-26 10:38:39',
        '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1750709871583715330', '1750709670844325890', '本机构及子机构数据', '1', 'success', 2, '000000', 0,
        '2024-01-26 10:38:55', '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1750709951258714114', '1750709670844325890', '本机构数据', '2', 'info', 3, '000000', 0, '2024-01-26 10:39:14',
        '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1750710026470973442', '1750709670844325890', '本人数据', '3', 'warning', 4, '000000', 0, '2024-01-26 10:39:32',
        '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1750710082662064130', '1750709670844325890', '自定义数据', '4', 'danger', 5, '000000', 0,
        '2024-01-26 10:39:45', '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1751795843914346498', '1751795674464464897', '内部打开', '0', 'primary', 1, '000000', 0, '2024-01-29 10:34:11',
        '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1751795926160453633', '1751795674464464897', '外部链接', '1', 'warning', 2, '000000', 0, '2024-01-29 10:34:30',
        '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1765673796922044417', '1765673616051073026', '默认', 'default', 'primary', 1, '000000', 0,
        '2024-03-07 17:40:12', '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1765673875749793793', '1765673616051073026', '系统', 'system', 'success', 2, '000000', 0,
        '2024-03-07 17:40:31', '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1767381601731276801', '1767381405970526209', '通知', '1', 'primary', 1, '000000', 0, '2024-03-12 10:46:25',
        '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1767381665602138113', '1767381405970526209', '公告', '2', 'success', 2, '000000', 0, '2024-03-12 10:46:40',
        '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1767823888772820994', '1765673616051073026', '通知公告', 'notify', 'warning', 3, '000000', 0,
        '2024-03-13 16:03:54', '1', '2024-03-27 14:40:00', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916801710325761', '1772916777920233473', '启用', '0', 'primary', 1, '042771', 0, '2024-03-27 17:21:27',
        '1', '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916801710325762', '1772916777920233473', '停用', '1', 'info', 2, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692546', '1772916834723692545', '登录成功', '0', 'success', 1, '042771', 0, '2024-03-27 17:21:27',
        '1', '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692547', '1772916834723692545', '退出成功', '1', 'info', 2, '042771', 0, '2024-03-27 17:21:27',
        '1', '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692548', '1772916834723692545', '验证码错误', '2', 'warning', 3, '042771', 0,
        '2024-03-27 17:21:28', '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692549', '1772916834723692545', '账号密码错误', '3', 'danger', 4, '042771', 0,
        '2024-03-27 17:21:28', '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692551', '1772916834723692550', '是', '0', 'primary', 1, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692552', '1772916834723692550', '否', '1', 'info', 2, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692554', '1772916834723692553', '男', '0', 'primary', 1, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692555', '1772916834723692553', '女', '1', 'warning', 2, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692556', '1772916834723692553', '未知', '2', 'info', 3, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692558', '1772916834723692557', '查询', '1', 'primary', 1, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692559', '1772916834723692557', '保存', '2', 'primary', 2, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692560', '1772916834723692557', '修改', '3', 'primary', 3, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692561', '1772916834723692557', '删除', '4', 'danger', 4, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692562', '1772916834723692557', '授权', '5', 'success', 5, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692563', '1772916834723692557', '导出', '6', 'primary', 6, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692564', '1772916834723692557', '导入', '7', 'primary', 7, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692565', '1772916834723692557', '强退', '8', 'warning', 8, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692566', '1772916834723692557', '清空', '9', 'warning', 9, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692567', '1772916834723692557', '上传', '10', 'primary', 10, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692568', '1772916834723692557', '下载', '11', 'primary', 11, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692569', '1772916834723692557', '其它', '99', 'info', 99, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692571', '1772916834723692570', '目录', '0', 'success', 1, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692572', '1772916834723692570', '菜单', '1', 'primary', 2, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692573', '1772916834723692570', '按钮', '2', 'info', 3, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692575', '1772916834723692574', '启用', '0', 'primary', 1, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692576', '1772916834723692574', '禁用', '1', 'danger', 2, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692578', '1772916834723692577', '成功', '0', 'primary', 1, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692579', '1772916834723692577', '失败', '1', 'danger', 2, '042771', 0, '2024-03-27 17:21:28', '1',
        '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692581', '1772916834723692580', '全部数据', '0', 'primary', 1, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692582', '1772916834723692580', '本机构及子机构数据', '1', 'success', 2, '042771', 0,
        '2024-03-27 17:21:28', '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692583', '1772916834723692580', '本机构数据', '2', 'info', 3, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692584', '1772916834723692580', '本人数据', '3', 'warning', 4, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692585', '1772916834723692580', '自定义数据', '4', 'danger', 5, '042771', 0,
        '2024-03-27 17:21:28', '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692587', '1772916834723692586', '内部打开', '0', 'primary', 1, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692588', '1772916834723692586', '外部链接', '1', 'warning', 2, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692590', '1772916834723692589', '默认', 'default', 'primary', 1, '042771', 0,
        '2024-03-27 17:21:28', '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692591', '1772916834723692589', '系统', 'system', 'success', 2, '042771', 0,
        '2024-03-27 17:21:28', '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692592', '1772916834723692589', '通知公告', 'notify', 'warning', 3, '042771', 0,
        '2024-03-27 17:21:28', '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692594', '1772916834723692593', '通知', '1', 'primary', 1, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');
INSERT INTO `sys_dict_data`
VALUES ('1772916834723692595', '1772916834723692593', '公告', '2', 'success', 2, '042771', 0, '2024-03-27 17:21:28',
        '1', '2024-03-27 17:21:28', '1', '0', '0', '');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`
(
    `id`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `dict_type`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
    `dict_name`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典名称',
    `sort`        int(11)                                                       NULL DEFAULT NULL COMMENT '排序',
    `tenant_id`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`     int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time` datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time` datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`      varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`  varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`      varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '字典类型'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type`
VALUES ('1747156982101737474', 'status', '启用状态', 1, '000000', 0, '2024-01-16 15:21:00', '1', '2024-03-27 14:40:57',
        '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1747159762342281217', 'login_operation', '登录操作', 2, '000000', 0, '2024-01-16 15:32:03', '1',
        '2024-03-27 14:40:57', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1747160739174764545', 'whether', '是否', 3, '000000', 0, '2024-01-16 15:35:56', '1', '2024-03-27 14:40:57',
        '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1747166651880919042', 'gender', '性别', 4, '000000', 0, '2024-01-16 15:59:25', '1', '2024-03-27 14:40:57', '1',
        '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1747170511718535169', 'business_type', '业务类型', 5, '000000', 0, '2024-01-16 16:14:46', '1',
        '2024-03-27 14:40:57', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1747920455324717057', 'menu_type', '菜单类型', 6, '000000', 0, '2024-01-18 17:54:46', '1',
        '2024-03-27 14:40:57', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1747921567675109377', 'enable_disable', '启用禁用', 7, '000000', 0, '2024-01-18 17:59:11', '1',
        '2024-03-27 14:40:57', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1747922159625621506', 'success_fail', '成功失败', 0, '000000', 0, '2024-01-18 18:01:32', '1',
        '2024-03-27 14:40:57', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1750709670844325890', 'data_scope', '数据权限', 9, '000000', 0, '2024-01-26 10:38:07', '1',
        '2024-03-27 14:38:42', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1751795674464464897', 'open_style', '打开方式', 10, '000000', 0, '2024-01-29 10:33:30', '1',
        '2024-03-27 14:38:42', '1', '0', '0', '菜单打开方式');
INSERT INTO `sys_dict_type`
VALUES ('1765673616051073026', 'schedule_group', '任务组名', 11, '000000', 0, '2024-03-07 17:39:29', '1',
        '2024-03-27 14:38:42', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1767381405970526209', 'notice_type', '通知公告', 12, '000000', 0, '2024-03-12 10:45:38', '1',
        '2024-03-27 14:38:42', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916777920233473', 'status', '启用状态', 1, '042771', 0, '2024-03-27 17:21:27', '1', '2024-03-27 17:21:27',
        '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692545', 'login_operation', '登录操作', 2, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692550', 'whether', '是否', 3, '042771', 0, '2024-03-27 17:21:27', '1', '2024-03-27 17:21:27',
        '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692553', 'gender', '性别', 4, '042771', 0, '2024-03-27 17:21:27', '1', '2024-03-27 17:21:27', '1',
        '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692557', 'business_type', '业务类型', 5, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692570', 'menu_type', '菜单类型', 6, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692574', 'enable_disable', '启用禁用', 7, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692577', 'success_fail', '成功失败', 0, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692580', 'data_scope', '数据权限', 9, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692586', 'open_style', '打开方式', 10, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '菜单打开方式');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692589', 'schedule_group', '任务组名', 11, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');
INSERT INTO `sys_dict_type`
VALUES ('1772916834723692593', 'notice_type', '通知公告', 12, '042771', 0, '2024-03-27 17:21:27', '1',
        '2024-03-27 17:21:27', '1', '0', '0', '');

-- ----------------------------
-- Table structure for sys_inspire
-- ----------------------------
DROP TABLE IF EXISTS `sys_inspire`;
CREATE TABLE `sys_inspire`
(
    `id`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `content`     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
    `like_num`    int(11)                                                       NULL DEFAULT NULL COMMENT '点赞数',
    `dislike_num` int(11)                                                       NULL DEFAULT NULL COMMENT '狂踩数',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '激励'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_inspire
-- ----------------------------
INSERT INTO `sys_inspire`
VALUES ('1748263141441310721', '业精于勤而荒于嬉，行成于思而毁于随', 31, 22);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310722', '别说什么来日方长，这世上多的是一见如故，再见陌路', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310723', '你的转身只需一秒，我的转身却需要一辈子', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310724', '为你生为你而快活！我为你而生，你的感动就是我的快乐', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310725', '细节决定命运，态度决定高度', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310726', '前世一千次的卖萌，才能换来你今生一次的回眸', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310727', '此曲有意无人传，愿随春风寄燕然', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310728', '曾经几许青涩年华，如今逝水了无痕', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310729', '向来缘浅，奈何情深', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310730', '幸福，就是找一个温暖的人过一辈子', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310731', '我相信这个世界永远那么美', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310732', '没有什么过不去，只是再也回不去', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310733', '知道的越少越快乐，想的越多越难过', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310734', '不念过往，不为未来，珍惜现在', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310735', '最值得欣赏的风景，是自己奋斗的足迹', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310736', '记住该记住的，健忘该健忘的。改变能改变的，理解不能改变的', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310737', '乌云的背后是阳光，阳光的背后是彩虹', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310738', '你的能量超乎你想象', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310739', '人最大的改变就是去做自己害怕的事情', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310740', '不要轻言放弃，否则对不起自己', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310741', '没有遇到挫折，永远不会懂得自己的力量有多大', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310742', '问世间，情是何物，直教人生死相许', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310743', '相见争如不见，有情何似无情', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141441310744', '衣带渐宽终不悔，为伊消得人憔悴', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141504225281', '用我三生烟火，换你一世迷离', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141504225282', '心微动奈何情己远，物也非，人也非，事事非，往日不可追', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141504225283', '一花一世界，一叶一追寻。一曲一场叹，一生为一人', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141504225284', '有志者自有千方百计，无志者只感千难万难', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141504225285', '只愿君心似我心，定不负相思意', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141504225286', '世界上只有想不通的人，没有走不通的路', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613890', '这世界上，每个人都有个想要寻找的人，一但错过了，就再也不会回来', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613891', '我希望多一点幽默，少一点气急败坏，少一点偏执极端', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613892', '防御敌人比防御朋友容易', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613893', '斩断自己的退路，才能更好地赢得出路', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613894', '哪有人喜欢孤独，只不过是害怕失望罢了', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613895', '一山不能容二虎，除非一公和一母', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613896', '学而不思则惘，思而不学则殆', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613897', '一个人不愿做的事，往往是他应该做的事', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613898', '倾听自己的心声，做自己喜欢做的事情', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613899', '没有过不去的坎，只有不想走的路', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613900', '只要这一秒不绝望，下一秒就有希望', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613901', '当你的才华还无法撑起你的野心的时候，就该静下心来好好读书', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613902', '你自己永远也不知道自己有多优秀', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613903', '沒有了爱的语言，所有的文字都是乏味的', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613904', '沉默是一个人最大的哭泣，微笑是一个人最好的伪装', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613905', '生活不是苦的，苦的是我们想要的太多；人不累，累得太多', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613906', '夕阳西下，雨露春秋，梧桐夜雨，独自哀愁', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613907', '忍不住，想要爱你的冲动', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613908', '牵你的手，每天和你共赏夕阳', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613909', '彼此相爱就是幸福。如此简单，如此难', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613910', '有志者自有千计万计，无志者只感千难万难', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613911', '实现自己既定的目标，必须能耐得住寂寞单干', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613912', '必须从过去的错误学习教训，而非依赖过去的成功', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613913', '失败只是暂时停止成功，假如我不能，我就一定要；假如我要，我就一定能', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613914', '永不言败，是成功者的最佳品格', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613915', '一个人的快乐，不是因为他拥有的多，而是因为他计较的少', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613916', '处事不必求功，无过便是功。为人不必感德，无怨便是德', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613917', '平安是幸，知足是福，清心是禄，寡欲是寿', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613918', '人之心胸，多欲则窄，寡欲则宽', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613919', '宁可清贫自乐，不可浊富多忧', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613920', '受思深处宜先退，得意浓时便可休', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613921', '势不可使尽，福不可享尽，便宜不可占尽，聪明不可用尽', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613922', '滴水穿石，不是力量大，而是功夫深', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613923', '心作良田耕不尽，善为至宝用无穷', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613924', '生命不是要超越别人，而是要超越自己', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613925', '当你停止尝试时，就是失败的时候', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613926', '不同的信念，决定不同的命运', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613927', '平生不做皱眉事，世上应无切齿人', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613928', '人生至恶是善谈人过，人生至愚恶闻己过', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613929', '莫以善小而不为，莫以恶小而为之', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613930', '是非天天有，不听自然无', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613931', '不妄求，则心安，不妄做，则身安', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613932', '不自重者，取辱。不自长者，取祸。不自满者，受益。不自足者，博闻', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613933', '身安不如心安，屋宽不如心宽', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613934', '成功源于不懈的努力', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613935', '成功不是凭梦想和希望，而是凭努力和实践', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613936', '为成功找方法，不为失败找借口', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613937', '努力爱一个人。付出，不一定会有收获；不付出，却一定不会有收获，不要奢望出现奇迹', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613938', '不要试图控制别人，不要要求别人理解你', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613939', '活在当下，别在怀念过去或者憧憬未来中浪费掉你现在的生活', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613940', '不要忘本，任何时候，任何事情', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613941', '花开有时，花落有时。无需留恋，该走的终会走；无需苛求，该来的迟早会来', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613942', '自己要先看得起自己，别人才会看得起你', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613943', '不识货，半世苦；不识人，一世苦', 1, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613944', '任何的限制，都是从自己的内心开始的', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613945', '无论你觉得自己多么的不幸，永远有人比你更加不幸', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613946', '尽量不说谎话，因为总有被拆穿的一天', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613947', '没有什么大不了，只不过摔一跤', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613948', '每一发奋努力的背后，必有加倍的赏赐', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613949', '命运总是光临在那些有准备的人身上', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613950', '爱情是一场没有硝烟的战争', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613951', '当爱已逝去，剩下的又是什么？', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613952', '明知道爱情并不可靠，可我还是拼命往里跳', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613953', '最冷的地方不是南极，是没有你的地方', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613954', '时间产生的不是变化，而是更长久的牵挂与思念', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613955', '再也回不去的时光，永远抹不掉的记忆', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613956', '无法拒绝的是开始 、无法抗拒的 是结束', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613957', '止不住的想你，止不住的念你，可是我却不忍心打搅你', 4, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613958', '多笑笑，会慢慢让自己真的快乐起来', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613959', '人生有很多次如果，但是没有一次但是', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613960', '生活不是林黛玉，不会因为忧伤而风情万种', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613961', '有时候，最好的安慰，就是无言的陪伴', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613962', '读万卷书，行万里路', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613963', '盛年不重来，一日难再晨，及时当勉励，岁月不待人', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613964', '假如人只能自己单独生活，只去考虑自己，他的痛苦将是难以忍受的', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613965', '志不真则心不热，心不热则功不紧', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613966', '人生意义的大小，不在乎外界的变迁，而在乎内心的经验', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613967', '人的影响短暂而微弱，书的影响则广泛而深远', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613968', '头脑醒目一点，处世低调一点，为人谦逊一点，做事积极一点，待人和善一点', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613969', '己所不欲，勿施于人', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613970', '我这个人走得很慢，但是我从不后退', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613971', '少而好学，如日出之阳；壮而好学，如日中之光；志而好学，如炳烛之光', 1, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613972', '人生富贵驹过隙，唯有荣名寿金石', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613973', '人生所贵在知已，四海相逢骨肉亲', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613974', '千里之行，始于足下', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613975', '故立志者，为学之心也；为学者，立志之事也', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613976', '成功者绝不给自己软弱的借口', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613977', '内外相应，言行相称', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613978', '今日应做的事没有做，明天再早也是耽误了', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613979', '业精于勤，荒于嬉；行成于思，毁于随', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613980', '非淡泊无以明志，非宁静无以致远', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613981', '路漫漫其修道远，吾将上下而求索', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613982', '敏而好学，不耻下问', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613983', '不知则问，不能则学', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613984', '合抱之木，生于毫末；九层之台，起于累土；千里之行，始于足下', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613985', '独学而无友则孤陋而寡闻', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613986', '微小的知识使人骄傲，丰富的知识使人谦逊', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613987', '一个人炫耀什么，说明内心缺少什么', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613988', '诚信处世世界大，奸诈为人人格低', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613989', '少壮不努力，老大徒伤悲', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613990', '莫等闲，白了少年头，空悲切', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613991', '旧书不厌百回读，熟读精思子自知', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613992', '读不在三更五鼓，功只怕一曝十寒', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613993', '读书之法，在循序而渐进，熟读而精思', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613994', '过去一切时代的精华尽在书中', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613995', '三更灯火五更鸡，正是男儿读书时。黑发不知勤学早，白首方悔读书迟', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613996', '时间是一味能治百病的良药', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613997', '对时间的慷慨，就等于慢性自杀', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613998', '时间总是转眼而过，回首时，犹如一梦初惊，此身虽在，却非昔日之身', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512613999', '时间就是金钱', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614000', '聪明的人爱得多，说得少', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614001', '爱就是充实了的生命，正如盛满了酒的酒杯', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614002', '爱情往往开始于见面的头一眼', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614003', '没有青春的爱情有何滋味？没有爱情的青春有何意义', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614004', '真正的爱情是不能用言语表达的，行为才是忠心的最好说明', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614005', '没有爱情的人生是什么？是没有黎明的长夜', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614006', '最甜美的是爱情，最苦涩的也是爱情', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614007', '爱是纯洁的，爱的内容里，不能有一点渣滓；爱是至善至诚的，爱的范围里，不能有丝毫私欲', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614008', '爱情献出了一切，却依然富有', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614009', '爱容易轻信', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614010', '有钱，爱情就能长久', 32, 6);
INSERT INTO `sys_inspire`
VALUES ('1748263141512614011', '爱情不是索取，而是给予', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141575528450', '志同道合是爱情的基础', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141575528451', '爱，总是相互的', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722753', '问世间情为何物，直教人生死相许', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722754', '塞翁失马，焉知非福', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722755', '少年辛苦终身事，莫向光阴惰寸功', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722756', '穷则变，变则通，通则久', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722757', '身无彩凤双飞翼，心有灵犀一点通', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722758', '人谁无过，过而能改，善莫大焉', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722759', '沟通心灵的桥是理解，连接心灵的路是信任', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722760', '世界上最快乐的事，莫过于为理想而奋斗', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722761', '人无忠信，不可立于世', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722762', '言必信，行必果', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722763', '信用既是无形的力量，也是无形的财富', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722764', '出污泥而不染，濯清涟而不妖', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722765', '静以修身，俭以养德', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722766', '一份耕耘一份收获', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722767', '活到老，学到老', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722768', '凡事必先难后易', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722769', '人有两只耳朵一张嘴，就是为了多听少说话', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722770', '一息若存，希望不灭', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722771', '双鸟在林不如一鸟在手', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722772', '不勤于始，将悔于终', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722773', '勤是摇钱树，俭是聚宝盆', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722774', '幸福没有明天，也没有昨天，它不怀念过去，也不向往未来；它只有现在', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722775',
        '友谊和爱情之间的区别在于：友谊意味着两个人和世界，然而爱情意味着两个人就是世界。在友谊中一加一等于二;在爱情中一加一还是一',
        0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722776', '不知节制的爱不能持久。它像溢出杯盏的酒浆的泡沫，转瞬便化为乌有', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722777', '沟通，别留下误会；牵手，别轻易放下', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722778', '不要花时间在一个不愿花时间在你身上的人', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722779', '我们看错了世界，却反过来说世界欺骗了我们', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722780', '生命犹如夏花之绚烂，死亡犹如秋叶之静美', 28, 26);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722781', '我只会爱我的家人，你若想要我爱你，那么你就努力地成为我的家人吧', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722782', '善待别人，谅解他人，热爱生命，努力生活', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722783', '好花不常开，好景不常在，也许有天我不再这么傻，我会努力的忘掉她', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722784', '人之所以痛苦，在于追求错误的东西', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722785', '人生能有几回搏！', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722786', '生活本来就很艰苦，容不得你悄悄去懒惰', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722787', '哭就哭了，但请你哭过后笑着说：我很好', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722788', '左右一个人成功的，不是能力，而是选择', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722789', '爱无处不在。只要用心去面对，爱情就会自然而然', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722790', '当你凝视深渊时，深渊也在凝视着你', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722791', '但凡不能杀死你的，最终都会使你更强大', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722792', '人的大脑和肢体一样，多用则灵，不用则废', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722793', '书山有路勤为径，学海无涯苦作舟', 1, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722794', '一饭之恩，当永世不忘', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722795', '爱情是黑夜里的灯塔，让迷失的船找到港湾', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722796', '就算我用尽了力气挽留，该走的还是会走，没有谁会为谁停留', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722797', '自古真情留不住，唯有套路得人心，你还相信真诚吗？', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722798',
        '都说真诚是必杀技，现实中却输得彻底，舔来舔去，丢掉了尊严，还被看不起，思来想去，最终归于一个钱字', 0, 0);
INSERT INTO `sys_inspire`
VALUES ('1748263141579722799', '舔狗，舔到最后一无所有', 0, 0);

-- ----------------------------
-- Table structure for sys_log_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_login`;
CREATE TABLE `sys_log_login`
(
    `id`             varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `user_id`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '用户ID',
    `username`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '用户名',
    `ip_address`     varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '登录IP',
    `login_location` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '登录地点',
    `user_agent`     varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'User Agent',
    `status`         varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '登录状态',
    `operation`      varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '操作信息   0：登录成功   1：退出成功  2：验证码错误  3：账号密码错误',
    `tenant_id`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `login_time`     datetime(0)                                                   NULL DEFAULT NULL COMMENT '访问时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '访问日志'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log_login
-- ----------------------------
INSERT INTO `sys_log_login`
VALUES ('1874357349439778817', '1', 'admin', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-01 15:29:51');
INSERT INTO `sys_log_login`
VALUES ('1874495632115249154', NULL, 'admin', '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', '2', '000000', '2025-01-02 00:39:20');
INSERT INTO `sys_log_login`
VALUES ('1874495691410124802', NULL, 'admin', '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', '2', '000000', '2025-01-02 00:39:34');
INSERT INTO `sys_log_login`
VALUES ('1874495766081318913', NULL, 'admin', '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', '2', '000000', '2025-01-02 00:39:52');
INSERT INTO `sys_log_login`
VALUES ('1874495842212130817', '1', 'admin', '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-02 00:40:10');
INSERT INTO `sys_log_login`
VALUES ('1874495885119860737', '1', 'admin', '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-02 00:40:20');
INSERT INTO `sys_log_login`
VALUES ('1874640430419685377', '1', 'admin', '171.213.56.108', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-02 10:14:42');
INSERT INTO `sys_log_login`
VALUES ('1874694361288028161', '1', 'admin', '221.218.210.51', '北京市 北京市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-02 13:49:01');
INSERT INTO `sys_log_login`
VALUES ('1874724077290139650', '1', 'admin', '219.131.142.146', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '0', '0', '000000', '2025-01-02 15:47:05');
INSERT INTO `sys_log_login`
VALUES ('1875014583589023746', '1', 'admin', '118.81.227.185', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-03 11:01:27');
INSERT INTO `sys_log_login`
VALUES ('1875087081265442818', '1', 'admin', '111.162.170.110', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '0', '0', '000000', '2025-01-03 15:49:32');
INSERT INTO `sys_log_login`
VALUES ('1875536160353169409', '1', 'admin', '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-04 21:34:01');
INSERT INTO `sys_log_login`
VALUES ('1876083873285156866', '1', 'admin', '1.119.169.100', '北京市 北京市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '0', '0', '000000', '2025-01-06 09:50:26');

-- ----------------------------
-- Table structure for sys_log_operate
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_operate`;
CREATE TABLE `sys_log_operate`
(
    `id`             varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'id',
    `module`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名',
    `name`           varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作名',
    `request_uri`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求URI',
    `request_method` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '请求方法',
    `request_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         NULL COMMENT '请求参数',
    `ip`             varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '操作IP',
    `address`        varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '登录地点',
    `user_agent`     varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'User Agent',
    `business_type`  varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '操作类型',
    `duration`       int(11)                                                       NOT NULL COMMENT '执行时长',
    `status`         varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '操作状态',
    `user_id`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '用户ID',
    `username`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '操作人',
    `result_msg`     varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '返回消息',
    `tenant_id`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `oper_time`      datetime(0)                                                   NULL DEFAULT NULL COMMENT '操作时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '操作日志'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log_operate
-- ----------------------------
INSERT INTO `sys_log_operate`
VALUES ('1874357365403299842', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '221.216.149.116',
        '北京市 北京市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:29:50');
INSERT INTO `sys_log_operate`
VALUES ('1874357365436854273', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '221.216.149.116',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:29:51');
INSERT INTO `sys_log_operate`
VALUES ('1874357365466214402', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:29:51');
INSERT INTO `sys_log_operate`
VALUES ('1874357365491380225', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '221.216.149.116', '北京市 北京市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:29:52');
INSERT INTO `sys_log_operate`
VALUES ('1874357365516546049', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 8, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:29:52');
INSERT INTO `sys_log_operate`
VALUES ('1874357407488946178', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '221.216.149.116', '北京市 北京市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:29:58');
INSERT INTO `sys_log_operate`
VALUES ('1874357407526694913', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:29:58');
INSERT INTO `sys_log_operate`
VALUES ('1874357449503289345', '菜单管理', '列表', '/aster-boot/sys/menu/list', 'GET',
        '{\"query\":{\"name\":\"\",\"path\":\"\",\"status\":\"\"}}', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 15, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:08');
INSERT INTO `sys_log_operate`
VALUES ('1874357449536843777', '字典', '字典类型分页查询', '/aster-boot/sys/dict/type/page', 'GET',
        '{\"query\":{\"dictType\":\"\",\"dictName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:10');
INSERT INTO `sys_log_operate`
VALUES ('1874357533460672513', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:25');
INSERT INTO `sys_log_operate`
VALUES ('1874357533498421250', '菜单管理', '列表', '/aster-boot/sys/menu/list', 'GET',
        '{\"query\":{\"name\":\"\",\"path\":\"\",\"status\":\"\"}}', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 7, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:27');
INSERT INTO `sys_log_operate`
VALUES ('1874357533527781377', '字典', '字典类型分页查询', '/aster-boot/sys/dict/type/page', 'GET',
        '{\"query\":{\"dictType\":\"\",\"dictName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:28');
INSERT INTO `sys_log_operate`
VALUES ('1874357533557141506', '菜单管理', '列表', '/aster-boot/sys/menu/list', 'GET',
        '{\"query\":{\"name\":\"\",\"path\":\"\",\"status\":\"\"}}', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 8, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:31');
INSERT INTO `sys_log_operate`
VALUES ('1874357575529541633', '租户', '切换租户', '/aster-boot/sys/tenant/switch/042771', 'GET', '{\"id\":\"042771\"}',
        '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '99', 3, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:43');
INSERT INTO `sys_log_operate`
VALUES ('1874357617573244930', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '221.216.149.116',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:44');
INSERT INTO `sys_log_operate`
VALUES ('1874357617610993665', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '221.216.149.116',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:44');
INSERT INTO `sys_log_operate`
VALUES ('1874357617644548098', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:45');
INSERT INTO `sys_log_operate`
VALUES ('1874357617673908226', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '221.216.149.116', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-01 15:30:45');
INSERT INTO `sys_log_operate`
VALUES ('1874495910910636034', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '123.191.207.210',
        '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-02 00:40:20');
INSERT INTO `sys_log_operate`
VALUES ('1874495910948384769', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '123.191.207.210',
        '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-02 00:40:20');
INSERT INTO `sys_log_operate`
VALUES ('1874495910981939201', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '123.191.207.210',
        '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-02 00:40:21');
INSERT INTO `sys_log_operate`
VALUES ('1874495911007105025', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-02 00:40:23');
INSERT INTO `sys_log_operate`
VALUES ('1874495911057436673', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '123.191.207.210',
        '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-02 00:40:23');
INSERT INTO `sys_log_operate`
VALUES ('1874496078863151105', '系统配置', '分页', '/aster-boot/sys/config/page', 'GET',
        '{\"query\":{\"configName\":\"\",\"configKey\":\"\",\"configType\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-02 00:41:00');
INSERT INTO `sys_log_operate`
VALUES ('1874496162786979842', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '123.191.207.210', '辽宁省 沈阳市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-02 00:41:16');
INSERT INTO `sys_log_operate`
VALUES ('1874640454079754242', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '171.213.56.108',
        '四川省 成都市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-02 10:14:42');
INSERT INTO `sys_log_operate`
VALUES ('1874640454130085890', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '171.213.56.108',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-02 10:14:43');
INSERT INTO `sys_log_operate`
VALUES ('1874640454184611841', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '171.213.56.108',
        '四川省 成都市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-02 10:14:43');
INSERT INTO `sys_log_operate`
VALUES ('1874640454218166274', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '171.213.56.108', '四川省 成都市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 8, '0', '1', 'admin', NULL, '000000', '2025-01-02 10:14:44');
INSERT INTO `sys_log_operate`
VALUES ('1874640454251720705', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '171.213.56.108',
        '四川省 成都市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 879, '0', '1', 'admin', NULL, '000000', '2025-01-02 10:14:44');
INSERT INTO `sys_log_operate`
VALUES ('1874694397531009025', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '221.218.210.51',
        '北京市 北京市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-02 13:49:00');
INSERT INTO `sys_log_operate`
VALUES ('1874694397564563458', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '221.218.210.51',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-02 13:49:00');
INSERT INTO `sys_log_operate`
VALUES ('1874694397593923585', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '221.218.210.51', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 7, '0', '1', 'admin', NULL, '000000', '2025-01-02 13:49:01');
INSERT INTO `sys_log_operate`
VALUES ('1874694397627478018', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '221.218.210.51', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-02 13:49:01');
INSERT INTO `sys_log_operate`
VALUES ('1874694397677809666', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '221.218.210.51', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 877, '0', '1', 'admin', NULL, '000000', '2025-01-02 13:49:01');
INSERT INTO `sys_log_operate`
VALUES ('1874694859101581313', '定时任务调度', '分页', '/aster-boot/quartz/job/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '221.218.210.51', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 51, '0', '1', 'admin', NULL, '000000', '2025-01-02 13:50:50');
INSERT INTO `sys_log_operate`
VALUES ('1874694943025410049', '定时任务调度日志', '分页', '/aster-boot/quartz/log/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"pageNum\":1,\"pageSize\":10}}', '221.218.210.51', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 454, '0', '1', 'admin', NULL, '000000', '2025-01-02 13:51:11');
INSERT INTO `sys_log_operate`
VALUES ('1874724094927187969', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '219.131.142.146',
        '广东省 潮州市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:05');
INSERT INTO `sys_log_operate`
VALUES ('1874724094964936706', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '219.131.142.146',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:06');
INSERT INTO `sys_log_operate`
VALUES ('1874724095002685441', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '219.131.142.146', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 25, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:07');
INSERT INTO `sys_log_operate`
VALUES ('1874724136975085569', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '219.131.142.146', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 7, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:09');
INSERT INTO `sys_log_operate`
VALUES ('1874724137017028609', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '219.131.142.146', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:09');
INSERT INTO `sys_log_operate`
VALUES ('1874724178989428737', '用户管理', '分页', '/aster-boot/sys/user/page', 'GET',
        '{\"query\":{\"name\":\"\",\"orgId\":\"\",\"gender\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '219.131.142.146', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 17, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:28');
INSERT INTO `sys_log_operate`
VALUES ('1874724179022983169', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET', '{\"query\":{}}',
        '219.131.142.146', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 42, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:28');
INSERT INTO `sys_log_operate`
VALUES ('1874724220999577601', '用户管理', '分页', '/aster-boot/sys/user/page', 'GET',
        '{\"query\":{\"name\":\"\",\"orgId\":\"1736569634695766018\",\"gender\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '219.131.142.146', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.6261.95 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-02 15:47:31');
INSERT INTO `sys_log_operate`
VALUES ('1875014607404281858', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '118.81.227.185',
        '山西省 太原市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-03 11:01:27');
INSERT INTO `sys_log_operate`
VALUES ('1875014607454613506', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '118.81.227.185',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-03 11:01:27');
INSERT INTO `sys_log_operate`
VALUES ('1875014607488167937', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '118.81.227.185',
        '山西省 太原市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-03 11:01:28');
INSERT INTO `sys_log_operate`
VALUES ('1875014607538499586', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '118.81.227.185', '山西省 太原市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-03 11:01:29');
INSERT INTO `sys_log_operate`
VALUES ('1875014607567859713', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '118.81.227.185',
        '山西省 太原市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 1081, '0', '1', 'admin', NULL, '000000', '2025-01-03 11:01:29');
INSERT INTO `sys_log_operate`
VALUES ('1875087088806801409', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '111.162.170.110',
        '天津市 天津市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-03 15:49:32');
INSERT INTO `sys_log_operate`
VALUES ('1875087088915853314', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '111.162.170.110',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-03 15:49:32');
INSERT INTO `sys_log_operate`
VALUES ('1875087088953602049', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '111.162.170.110',
        '天津市 天津市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-03 15:49:33');
INSERT INTO `sys_log_operate`
VALUES ('1875087130930196482', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '111.162.170.110', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-03 15:49:34');
INSERT INTO `sys_log_operate`
VALUES ('1875087130963750914', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '111.162.170.110', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '1', 847, '0', '1', 'admin', NULL, '000000', '2025-01-03 15:49:34');
INSERT INTO `sys_log_operate`
VALUES ('1875087172944539650', '定时任务调度', '分页', '/aster-boot/quartz/job/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '111.162.170.110', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '1', 34, '0', '1', 'admin', NULL, '000000', '2025-01-03 15:49:44');
INSERT INTO `sys_log_operate`
VALUES ('1875087172978094081', '定时任务调度日志', '分页', '/aster-boot/quartz/log/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"pageNum\":1,\"pageSize\":10}}', '111.162.170.110', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
        '1', 380, '0', '1', 'admin', NULL, '000000', '2025-01-03 15:49:48');
INSERT INTO `sys_log_operate`
VALUES ('1875536197917356033', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:01');
INSERT INTO `sys_log_operate`
VALUES ('1875536198055768065', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:01');
INSERT INTO `sys_log_operate`
VALUES ('1875536198131265538', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 26, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:02');
INSERT INTO `sys_log_operate`
VALUES ('1875536198206763009', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 47, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:03');
INSERT INTO `sys_log_operate`
VALUES ('1875536198240317442', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 736, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:03');
INSERT INTO `sys_log_operate`
VALUES ('1875536198273871873', '菜单管理', '列表', '/aster-boot/sys/menu/list', 'GET',
        '{\"query\":{\"name\":\"\",\"path\":\"\",\"status\":\"\"}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 8, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:09');
INSERT INTO `sys_log_operate`
VALUES ('1875536240254660609', '字典', '字典类型分页查询', '/aster-boot/sys/dict/type/page', 'GET',
        '{\"query\":{\"dictType\":\"\",\"dictName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:12');
INSERT INTO `sys_log_operate`
VALUES ('1875536240288215041', '系统配置', '分页', '/aster-boot/sys/config/page', 'GET',
        '{\"query\":{\"configName\":\"\",\"configKey\":\"\",\"configType\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:14');
INSERT INTO `sys_log_operate`
VALUES ('1875536240313380865', '通知公告', '分页', '/aster-boot/sys/notice/page', 'GET',
        '{\"query\":{\"title\":\"\",\"type\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:17');
INSERT INTO `sys_log_operate`
VALUES ('1875536282285780993', '通知公告', '详情', '/aster-boot/sys/notice/info/1767440007913811969', 'GET',
        '{\"id\":\"1767440007913811969\"}', '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:20');
INSERT INTO `sys_log_operate`
VALUES ('1875536282369667074', '用户管理', '分页', '/aster-boot/sys/user/page', 'GET',
        '{\"query\":{\"name\":\"\",\"orgId\":\"\",\"gender\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:24');
INSERT INTO `sys_log_operate`
VALUES ('1875536282407415809', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET', '{\"query\":{}}', '36.63.12.201',
        '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 22, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:24');
INSERT INTO `sys_log_operate`
VALUES ('1875536282457747458', '角色管理', '分页', '/aster-boot/sys/role/page', 'GET',
        '{\"query\":{\"roleName\":\"\",\"roleCode\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:29');
INSERT INTO `sys_log_operate`
VALUES ('1875536324459507714', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET',
        '{\"query\":{\"orgName\":\"\",\"status\":\"\"}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:30');
INSERT INTO `sys_log_operate`
VALUES ('1875536324488867841', '岗位管理', '分页', '/aster-boot/sys/post/page', 'GET',
        '{\"query\":{\"postCode\":\"\",\"postName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 28, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:32');
INSERT INTO `sys_log_operate`
VALUES ('1875536324518227970', '租户', '分页', '/aster-boot/sys/tenant/page', 'GET',
        '{\"query\":{\"contactUserName\":\"\",\"companyName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 7, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:37');
INSERT INTO `sys_log_operate`
VALUES ('1875536366507405314', '租户套餐', '分页', '/aster-boot/sys/tenantPackage/page', 'GET',
        '{\"query\":{\"packageName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 30, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:40');
INSERT INTO `sys_log_operate`
VALUES ('1875536366540959746', '租户', '分页', '/aster-boot/sys/tenant/page', 'GET',
        '{\"query\":{\"contactUserName\":\"\",\"companyName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:43');
INSERT INTO `sys_log_operate`
VALUES ('1875536366566125569', '定时任务调度', '分页', '/aster-boot/quartz/job/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 38, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:48');
INSERT INTO `sys_log_operate`
VALUES ('1875536408538525698', '定时任务调度日志', '分页', '/aster-boot/quartz/log/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"pageNum\":1,\"pageSize\":10}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 452, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:34:49');
INSERT INTO `sys_log_operate`
VALUES ('1875536450519314433', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET', '{\"query\":{}}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:35:07');
INSERT INTO `sys_log_operate`
VALUES ('1875536450557063170', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET', '{\"query\":{}}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:35:07');
INSERT INTO `sys_log_operate`
VALUES ('1875536534476697601', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 21:35:23');
INSERT INTO `sys_log_operate`
VALUES ('1875548195291934721', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '36.63.12.201',
        '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:40');
INSERT INTO `sys_log_operate`
VALUES ('1875548195321294850', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:41');
INSERT INTO `sys_log_operate`
VALUES ('1875548195354849282', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:41');
INSERT INTO `sys_log_operate`
VALUES ('1875548195380015105', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 9, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:41');
INSERT INTO `sys_log_operate`
VALUES ('1875548195421958146', '菜单管理', '列表', '/aster-boot/sys/menu/list', 'GET',
        '{\"query\":{\"name\":\"\",\"path\":\"\",\"status\":\"\"}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 11, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:46');
INSERT INTO `sys_log_operate`
VALUES ('1875548195455512578', '字典', '字典类型分页查询', '/aster-boot/sys/dict/type/page', 'GET',
        '{\"query\":{\"dictType\":\"\",\"dictName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:49');
INSERT INTO `sys_log_operate`
VALUES ('1875548237423718402', '系统配置', '分页', '/aster-boot/sys/config/page', 'GET',
        '{\"query\":{\"configName\":\"\",\"configKey\":\"\",\"configType\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:50');
INSERT INTO `sys_log_operate`
VALUES ('1875548237461467137', '通知公告', '分页', '/aster-boot/sys/notice/page', 'GET',
        '{\"query\":{\"title\":\"\",\"type\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}', '36.63.12.201',
        '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:52');
INSERT INTO `sys_log_operate`
VALUES ('1875548237495021570', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET', '{\"query\":{}}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:54');
INSERT INTO `sys_log_operate`
VALUES ('1875548237524381697', '用户管理', '分页', '/aster-boot/sys/user/page', 'GET',
        '{\"query\":{\"name\":\"\",\"orgId\":\"\",\"gender\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:54');
INSERT INTO `sys_log_operate`
VALUES ('1875548237562130433', '角色管理', '分页', '/aster-boot/sys/role/page', 'GET',
        '{\"query\":{\"roleName\":\"\",\"roleCode\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:55');
INSERT INTO `sys_log_operate`
VALUES ('1875548237595684866', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET',
        '{\"query\":{\"orgName\":\"\",\"status\":\"\"}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:56');
INSERT INTO `sys_log_operate`
VALUES ('1875548237650210818', '岗位管理', '分页', '/aster-boot/sys/post/page', 'GET',
        '{\"query\":{\"postCode\":\"\",\"postName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 5, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:21:57');
INSERT INTO `sys_log_operate`
VALUES ('1875548279626805250', '租户', '分页', '/aster-boot/sys/tenant/page', 'GET',
        '{\"query\":{\"contactUserName\":\"\",\"companyName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 12, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:22:02');
INSERT INTO `sys_log_operate`
VALUES ('1875548279660359682', '租户套餐', '分页', '/aster-boot/sys/tenantPackage/page', 'GET',
        '{\"query\":{\"packageName\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 9, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:22:04');
INSERT INTO `sys_log_operate`
VALUES ('1875548279689719810', '菜单管理', '列表', '/aster-boot/sys/menu/list', 'GET', '{\"query\":{}}', '36.63.12.201',
        '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 7, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:22:08');
INSERT INTO `sys_log_operate`
VALUES ('1875548279714885633', '租户套餐', '详情', '/aster-boot/sys/tenantPackage/info/1772906769732083714', 'GET',
        '{\"id\":\"1772906769732083714\"}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 6, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:22:09');
INSERT INTO `sys_log_operate`
VALUES ('1875548321691480065', '定时任务调度', '分页', '/aster-boot/quartz/job/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"status\":\"\",\"pageNum\":1,\"pageSize\":10}}',
        '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:22:17');
INSERT INTO `sys_log_operate`
VALUES ('1875548321741811714', '定时任务调度日志', '分页', '/aster-boot/quartz/log/page', 'GET',
        '{\"query\":{\"jobName\":\"\",\"jobGroup\":\"\",\"pageNum\":1,\"pageSize\":10}}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 129, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:22:18');
INSERT INTO `sys_log_operate`
VALUES ('1875548615385034754', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET', '{\"query\":{}}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:23:24');
INSERT INTO `sys_log_operate`
VALUES ('1875548615435366402', '组织机构', '列表', '/aster-boot/sys/org/list', 'GET', '{\"query\":{}}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:23:24');
INSERT INTO `sys_log_operate`
VALUES ('1875554529714384898', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:46:58');
INSERT INTO `sys_log_operate`
VALUES ('1875554529747939330', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:46:59');
INSERT INTO `sys_log_operate`
VALUES ('1875554571720339458', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '36.63.12.201', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-04 22:47:00');
INSERT INTO `sys_log_operate`
VALUES ('1875562163825815554', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '36.63.12.201',
        '安徽省 安庆市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 23:17:15');
INSERT INTO `sys_log_operate`
VALUES ('1875562163876147201', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '36.63.12.201',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-04 23:17:16');
INSERT INTO `sys_log_operate`
VALUES ('1876083878339293186', '认证管理', '菜单权限', '/aster-boot/sys/auth/menu', 'GET', '{}', '1.119.169.100',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 3, '0', '1', 'admin', NULL, '000000', '2025-01-06 09:50:26');
INSERT INTO `sys_log_operate`
VALUES ('1876083878368653314', '认证管理', '按钮权限', '/aster-boot/sys/auth/authority', 'GET', '{}', '1.119.169.100',
        'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-06 09:50:26');
INSERT INTO `sys_log_operate`
VALUES ('1876083878414790658', '字典', '全部字典数据', '/aster-boot/sys/dict', 'GET', '{}', '1.119.169.100', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 28, '0', '1', 'admin', NULL, '000000', '2025-01-06 09:50:26');
INSERT INTO `sys_log_operate`
VALUES ('1876083920403968001', '通知公告', '获取公告信息', '/aster-boot/sys/notice/announcement', 'GET', '{}',
        '1.119.169.100', 'XX XX',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 4, '0', '1', 'admin', NULL, '000000', '2025-01-06 09:50:27');
INSERT INTO `sys_log_operate`
VALUES ('1876083920445911041', '激励', '全部', '/aster-boot/sys/inspire/list', 'GET', '{}', '1.119.169.100',
        '北京市 北京市',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36',
        '1', 1080, '0', '1', 'admin', NULL, '000000', '2025-01-06 09:50:27');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `id`            varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `pid`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '上级ID，一级菜单为0',
    `name`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
    `en_name`       varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '英文名称',
    `menu_path`     varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
    `component`     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件地址',
    `perms`         varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权标识(多个用逗号分隔，如：sys:menu:list,sys:menu:save)',
    `menu_type`     varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
    `open_style`    tinyint(4)                                                    NULL DEFAULT NULL COMMENT '打开方式   0：内部   1：外部',
    `icon`          varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '菜单图标',
    `is_keep_alive` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '1' COMMENT '是否缓存',
    `is_affix`      varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '1' COMMENT '是否固定',
    `is_full`       varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '1' COMMENT '是否全屏',
    `is_hide`       varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '1' COMMENT '是否隐藏',
    `sort`          int(4)                                                        NULL DEFAULT NULL COMMENT '菜单排序',
    `version`       int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time`   datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time`   datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`     varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`        varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`    varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`        varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_pid` (`pid`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '菜单'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu`
VALUES ('1747187501363408897', '0', '首页', 'home', '/home', '/home', NULL, '0', 0, 'iconfont icon-SmartHome', '0', '0',
        '1', '1', 1, 0, '2024-01-16 17:22:16', NULL, '2024-03-01 17:35:27', '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747188066025779201', '0', '系统设置', 'setting', '/setting', NULL, NULL, '0', 0, 'iconfont icon-shezhi', '1',
        '1', '1', '1', 2, 0, '2024-01-16 17:24:31', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747189484476477441', '1747188066025779201', '菜单管理', 'menu', '/system/menu', '/system/menu/index', NULL,
        '1', 0, 'iconfont icon-caidan', '1', '1', '1', '1', 1, 0, '2024-01-16 17:30:09', NULL, '2024-02-07 17:03:08',
        '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747189992922591234', '1747189484476477441', '查询', NULL, NULL, NULL, 'sys:menu:list', '2', 0, NULL, '1', '1',
        '1', '1', 1, 0, '2024-01-16 17:32:10', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747190305205301250', '1747189484476477441', '新增', NULL, NULL, NULL, 'sys:menu:add', '2', 0, NULL, '1', '1',
        '1', '1', 2, 0, '2024-01-16 17:33:25', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747190567236055041', '1747189484476477441', '编辑', NULL, NULL, NULL, 'sys:menu:edit', '2', 0, NULL, '1', '1',
        '1', '1', 3, 0, '2024-01-16 17:34:27', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747190750350979073', '1747189484476477441', '删除', NULL, NULL, NULL, 'sys:menu:delete', '2', 0, NULL, '1',
        '1', '1', '1', 4, 0, '2024-01-16 17:35:11', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747191542764056578', '1747188066025779201', '数据字典', 'dict', '/system/dict', '/system/dict/index', NULL,
        '1', 0, 'iconfont icon-zidianguanli', '1', '1', '1', '1', 2, 0, '2024-01-16 17:38:20', NULL,
        '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747192086761091073', '1766018358878502914', '定时任务', 'quartz', '/quartz/job', '/quartz/job/index', NULL,
        '1', 0, 'iconfont icon-renwu', '1', '1', '1', '1', 1, 0, '2024-01-16 17:40:29', NULL, '2024-03-08 16:30:29',
        '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747192570699886594', '0', '权限管理', 'auth', '/auth', NULL, NULL, '0', 0, 'iconfont icon-shenqingquanxian',
        '1', '1', '1', '1', 3, 0, '2024-01-16 17:42:25', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747192878700212226', '1747192570699886594', '用户管理', 'user', '/auth/user', '/auth/user/index', NULL, '1',
        0, 'iconfont icon-geren', '1', '1', '1', '1', 1, 0, '2024-01-16 17:43:38', NULL, '2024-02-07 17:03:08', NULL,
        '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747192956747821057', '1747192570699886594', '角色管理', 'role', '/auth/role', '/auth/role/index', NULL, '1',
        0, 'iconfont icon-jiaoseguanli', '1', '1', '1', '1', 2, 0, '2024-01-16 17:43:57', NULL, '2024-02-07 17:03:08',
        NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747193110250958850', '1747192570699886594', '部门管理', 'dept', '/auth/dept', '/auth/dept/index', NULL, '1',
        0, 'iconfont icon-bumen', '1', '1', '1', '1', 3, 0, '2024-01-16 17:44:33', NULL, '2024-02-07 17:03:08', NULL,
        '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747193271865880578', '1747192570699886594', '岗位管理', 'post', '/auth/post', '/auth/post/index', NULL, '1',
        0, 'iconfont icon-gangweishouquan', '1', '1', '1', '1', 4, 0, '2024-01-16 17:45:12', NULL,
        '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747193498798698498', '0', '日志管理', 'log', '/log', NULL, NULL, '0', 0, 'iconfont icon-rizhi', '1', '1', '1',
        '1', 4, 0, '2024-01-16 17:46:06', NULL, '2024-02-07 17:03:08', '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747194331707781121', '1747193498798698498', '访问日志', 'access', '/log/access', '/log/access/index', NULL,
        '1', 0, 'iconfont icon-fangwen', '1', '1', '1', '1', 1, 0, '2024-01-16 17:49:25', NULL, '2024-02-07 17:03:08',
        '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747194426356445185', '1747193498798698498', '操作日志', 'operation', '/log/operation', '/log/operation/index',
        NULL, '1', 0, 'iconfont icon-caozuo', '1', '1', '1', '1', 2, 0, '2024-01-16 17:49:47', NULL,
        '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747196006048772098', '1747189484476477441', '详情', NULL, NULL, NULL, 'sys:menu:info', '2', 0, NULL, '1', '1',
        '1', '1', 5, 0, '2024-01-16 17:56:04', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747196350107529218', '1747191542764056578', '全部字典', NULL, NULL, NULL, 'sys:dict:all', '2', 0, NULL, '1',
        '1', '1', '1', 1, 0, '2024-01-16 17:57:26', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747196680379609090', '1747191542764056578', '查询', NULL, NULL, NULL, 'sys:dict:list', '2', 0, NULL, '1', '1',
        '1', '1', 2, 0, '2024-01-16 17:58:45', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747196799351042050', '1747191542764056578', '详情', NULL, NULL, NULL, 'sys:dict:info', '2', 0, NULL, '1', '1',
        '1', '1', 3, 0, '2024-01-16 17:59:13', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747196893399920642', '1747191542764056578', '新增', NULL, NULL, NULL, 'sys:dict:add', '2', 0, NULL, '1', '1',
        '1', '1', 4, 0, '2024-01-16 17:59:35', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747197016569851905', '1747191542764056578', '编辑', NULL, NULL, NULL, 'sys:dict:edit', '2', 0, NULL, '1', '1',
        '1', '1', 5, 0, '2024-01-16 18:00:05', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747197128121561090', '1747191542764056578', '删除', NULL, NULL, NULL, 'sys:dict:delete', '2', 0, NULL, '1',
        '1', '1', '1', 6, 0, '2024-01-16 18:00:31', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747198903130042369', '1747192878700212226', '查询', NULL, NULL, NULL, 'sys:user:list', '2', 0, NULL, '1', '1',
        '1', '1', 1, 0, '2024-01-16 18:07:35', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747199276607647746', '1747192878700212226', '详情', NULL, NULL, NULL, 'sys:user:info', '2', 0, NULL, '1', '1',
        '1', '1', 2, 0, '2024-01-16 18:09:04', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747199310254354433', '1747192878700212226', '新增', NULL, NULL, NULL, 'sys:user:add', '2', 0, NULL, '1', '1',
        '1', '1', 3, 0, '2024-01-16 18:09:12', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747199339807420417', '1747192878700212226', '编辑', NULL, NULL, NULL, 'sys:user:edit', '2', 0, NULL, '1', '1',
        '1', '1', 4, 0, '2024-01-16 18:09:19', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747199371868680194', '1747192878700212226', '删除', NULL, NULL, NULL, 'sys:user:delete', '2', 0, NULL, '1',
        '1', '1', '1', 5, 0, '2024-01-16 18:09:26', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747199403221102593', '1747192878700212226', '导入', NULL, NULL, NULL, 'sys:user:import', '2', 0, NULL, '1',
        '1', '1', '1', 6, 0, '2024-01-16 18:09:34', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747199434363809793', '1747192878700212226', '导出', NULL, NULL, NULL, 'sys:user:export', '2', 0, NULL, '1',
        '1', '1', '1', 7, 0, '2024-01-16 18:09:41', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205571012325378', '1747192956747821057', '查询', NULL, NULL, NULL, 'sys:role:list', '2', 0, NULL, '1', '1',
        '1', '1', 1, 0, '2024-01-16 18:34:04', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205683738439682', '1747192956747821057', '详情', NULL, NULL, NULL, 'sys:role:info', '2', 0, NULL, '1', '1',
        '1', '1', 2, 0, '2024-01-16 18:34:31', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205712855298050', '1747192956747821057', '新增', NULL, NULL, NULL, 'sys:role:add', '2', 0, NULL, '1', '1',
        '1', '1', 3, 0, '2024-01-16 18:34:38', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205740600619009', '1747192956747821057', '编辑', NULL, NULL, NULL, 'sys:role:edit', '2', 0, NULL, '1', '1',
        '1', '1', 4, 0, '2024-01-16 18:34:45', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205771273564161', '1747192956747821057', '删除', NULL, NULL, NULL, 'sys:role:delete', '2', 0, NULL, '1',
        '1', '1', '1', 5, 0, '2024-01-16 18:34:52', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205803280297986', '1747192956747821057', '数据权限', NULL, NULL, NULL, 'sys:role:update', '2', 0, NULL,
        '1', '1', '1', '1', 6, 0, '2024-01-16 18:35:00', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205875762065410', '1747192956747821057', '角色菜单', NULL, NULL, NULL, 'sys:role:menu', '2', 0, NULL, '1',
        '1', '1', '1', 7, 0, '2024-01-16 18:35:17', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747205945957937153', '1747192956747821057', '角色用户', NULL, NULL, NULL, 'sys:role:user', '2', 0, NULL, '1',
        '1', '1', '1', 8, 0, '2024-01-16 18:35:34', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206206269026306', '1747193110250958850', '查询', NULL, NULL, NULL, 'sys:org:list', '2', 0, NULL, '1', '1',
        '1', '1', 1, 0, '2024-01-16 18:36:36', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206428973985793', '1747193110250958850', '详情', NULL, NULL, NULL, 'sys:org:info', '2', 0, NULL, '1', '1',
        '1', '1', 2, 0, '2024-01-16 18:37:29', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206455591038977', '1747193110250958850', '新增', NULL, NULL, NULL, 'sys:org:add', '2', 0, NULL, '1', '1',
        '1', '1', 3, 0, '2024-01-16 18:37:35', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206484552708098', '1747193110250958850', '编辑', NULL, NULL, NULL, 'sys:org:edit', '2', 0, NULL, '1', '1',
        '1', '1', 4, 0, '2024-01-16 18:37:42', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206510939074561', '1747193110250958850', '删除', NULL, NULL, NULL, 'sys:org:delete', '2', 0, NULL, '1',
        '1', '1', '1', 5, 0, '2024-01-16 18:37:48', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206775738068994', '1747193271865880578', '查询', NULL, NULL, NULL, 'sys:post:list', '2', 0, NULL, '1', '1',
        '1', '1', 1, 0, '2024-01-16 18:38:52', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206811968466945', '1747193271865880578', '详情', NULL, NULL, NULL, 'sys:post:info', '2', 0, NULL, '1', '1',
        '1', '1', 2, 0, '2024-01-16 18:39:00', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206840011583489', '1747193271865880578', '新增', NULL, NULL, NULL, 'sys:post:add', '2', 0, NULL, '1', '1',
        '1', '1', 3, 0, '2024-01-16 18:39:07', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206867232616450', '1747193271865880578', '编辑', NULL, NULL, NULL, 'sys:post:edit', '2', 0, NULL, '1', '1',
        '1', '1', 4, 0, '2024-01-16 18:39:13', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747206898132054017', '1747193271865880578', '删除', NULL, NULL, NULL, 'sys:post:delete', '2', 0, NULL, '1',
        '1', '1', '1', 5, 0, '2024-01-16 18:39:21', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747207269462175746', '1747194331707781121', '查询', NULL, NULL, NULL, 'sys:log:login', '2', 0, NULL, '1', '1',
        '1', '1', 1, 0, '2024-01-16 18:40:49', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1747207357441896449', '1747194426356445185', '查询', NULL, NULL, NULL, 'sys:log:operate', '2', 0, NULL, '1',
        '1', '1', '1', 1, 0, '2024-01-16 18:41:10', NULL, '2024-02-07 17:03:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1748270065033433089', '0', '常用组件', 'assembly', '/assembly', '', '', '0', NULL, 'iconfont icon-zujian', '1',
        '1', '1', '1', 9, 0, '2024-01-19 17:03:59', '1', '2024-03-26 15:37:20', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1748270372136177666', '1748270065033433089', '引导页', 'guide', '/assembly/guide', '/assembly/guide/index', '',
        '1', NULL, 'iconfont icon-yonghuyindao', '1', '1', '1', '1', 1, 0, '2024-01-19 17:05:13', '1',
        '2024-03-26 15:37:17', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1748270579791974402', '1748270065033433089', '图标选择器', 'icon-select', '/assembly/icon-select',
        '/assembly/icon-select/index', '', '1', NULL, 'iconfont icon-gengduo', '1', '1', '1', '1', 2, 0,
        '2024-01-19 17:06:02', '1', '2024-03-26 15:37:26', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1748270727033016321', '1748270065033433089', '树形筛选器', 'tree-filter', '/assembly/tree-filter',
        '/assembly/tree-filter/index', '', '1', NULL, 'iconfont icon-zhiwu', '1', '1', '1', '1', 3, 0,
        '2024-01-19 17:06:37', '1', '2024-03-26 15:37:31', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1748270831601209345', '1748270065033433089', '富文本编辑器', 'editor', '/assembly/editor',
        '/assembly/editor/index', '', '1', NULL, 'iconfont icon-bianji', '1', '1', '1', '1', 4, 0,
        '2024-01-19 17:07:02', '1', '2024-03-26 15:37:34', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1748270936337174530', '1748270065033433089', '页面打印', 'print', '/assembly/print', '/assembly/print/index',
        '', '1', NULL, 'iconfont icon-dayinji', '1', '1', '1', '1', 5, 0, '2024-01-19 17:07:27', '1',
        '2024-03-26 15:37:37', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1748271059641323522', '1748270065033433089', '自定义指令', 'directives', '/assembly/directives',
        '/assembly/directives/index', '', '1', NULL, 'iconfont icon-anniu', '1', '1', '1', '1', 6, 0,
        '2024-01-19 17:07:57', '1', '2024-03-26 15:37:42', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1750831092606521346', '1747192878700212226', '重置密码', NULL, '', '', 'sys:user:reset', '2', NULL, '', '1',
        '1', '1', '1', 8, 0, '2024-01-26 18:40:36', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1751807171982802946', '1748270065033433089', '外部链接', NULL, 'https://gitee.com/lp1791803611/aster-admin',
        '', '', '1', 1, 'iconfont icon-chaosongwode', '1', '1', '1', '1', 7, 0, '2024-01-29 11:19:11', '1',
        '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752190241453940737', '1747188066025779201', '参数配置', 'config', '/system/config', '/system/config/index',
        '', '1', 0, 'iconfont icon-jiemianguanli', '1', '1', '1', '1', 3, 0, '2024-01-30 12:41:22', '1',
        '2024-03-26 15:36:53', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752192636825149442', '1752190241453940737', '查询', NULL, '', '/system/config/index', 'sys:config:list', '2',
        0, '', '1', '1', '1', '1', 1, 0, '2024-01-30 12:41:22', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752192707331399681', '1752190241453940737', '新增', NULL, '', '/system/config/index', 'sys:config:add', '2',
        0, '', '1', '1', '1', '1', 2, 0, '2024-01-30 12:41:22', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752192882514894850', '1752190241453940737', '编辑', NULL, '', '/system/config/index', 'sys:config:edit', '2',
        0, '', '1', '1', '1', '1', 3, 0, '2024-01-30 12:41:22', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752192936898240513', '1752190241453940737', '删除', NULL, '', '/system/config/index', 'sys:config:delete',
        '2', 0, '', '1', '1', '1', '1', 4, 0, '2024-01-30 12:41:22', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752193014677413890', '1752190241453940737', '详情', NULL, '', '/system/config/index', 'sys:config:info', '2',
        0, '', '1', '1', '1', '1', 5, 0, '2024-01-30 12:41:22', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752968147641925633', '1748270065033433089', '阴阳历', 'lunar', '/assembly/lunar', '/assembly/lunar/index', '',
        '1', 0, 'iconfont icon-zhouqi', '1', '1', '1', '1', 8, 0, '2024-02-01 16:12:30', '1', '2024-03-26 15:36:45',
        '1', '0', '0', '阴历、阳历');
INSERT INTO `sys_menu`
VALUES ('1752982543650881537', '0', '系统监控', NULL, '/monitor', '', '', '0', NULL, 'iconfont icon-jiankong', '1', '1',
        '1', '1', 6, 0, '2024-02-01 17:09:42', '1', '2024-03-26 10:54:56', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752984055445815298', '1752982543650881537', '在线用户', 'online', '/monitor/online', '/monitor/online/index',
        '', '1', 0, 'iconfont icon-zaixianyonghu', '1', '1', '1', '1', 1, 0, '2024-02-01 17:15:42', '1',
        '2024-03-26 15:36:40', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752984411114405889', '1752984055445815298', '强退', NULL, '', '', 'monitor:online:force', '2', NULL, '', '1',
        '1', '1', '1', 2, 0, '2024-02-01 17:17:07', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752984696369020929', '1752982543650881537', '服务监控', 'server', '/monitor/server', '/monitor/server/index',
        '', '1', 0, 'iconfont icon-fuwujiankong', '1', '1', '1', '1', 3, 0, '2024-02-01 17:18:15', '1',
        '2024-03-26 15:36:37', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752984857195413506', '1752982543650881537', '缓存监控', 'cache', '/monitor/cache', '/monitor/cache/index', '',
        '1', 0, 'iconfont icon-huancunjiankong', '1', '1', '1', '1', 4, 0, '2024-02-01 17:18:54', '1',
        '2024-03-26 15:36:33', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1752991362917433346', '1752984055445815298', '查询', NULL, '', '', 'monitor:online:list', '2', NULL, '', '1',
        '1', '1', '1', 1, 0, '2024-02-01 17:17:07', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1753310523082170369', '1752984696369020929', '服务器信息', NULL, '', '', 'monitor:server:all', '2', NULL, '',
        '1', '1', '1', '1', 1, 0, '2024-02-02 14:52:58', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1754686066566455298', '1752984857195413506', '查询', NULL, '', '', 'monitor:cache:all', '2', 0, '', '1', '1',
        '1', '1', 1, 0, '2024-02-01 17:18:54', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1754686188859777025', '1752984857195413506', '删除', NULL, '', '', 'monitor:cache:delete', '2', 0, '', '1',
        '1', '1', '1', 2, 0, '2024-02-01 17:18:54', '1', '2024-02-07 17:03:08', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1763092268702851074', '0', '代码生成', 'generator', '/generator', '', '', '0', NULL,
        'iconfont icon-daimashengcheng', '1', '1', '1', '1', 7, 0, '2024-02-29 14:42:08', '1', '2024-03-26 15:36:22',
        '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1763093740022751234', '1763092268702851074', '代码生成', 'code', '/generator/code', '/generator/code/index',
        '', '1', 0, 'iconfont icon-generator', '1', '1', '1', '1', 1, 0, '2024-02-29 14:42:08', '1',
        '2024-03-26 15:37:56', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1763093962853539841', '1763092268702851074', '数据源管理', 'database', '/generator/database',
        '/generator/database/index', '', '1', 0, 'iconfont icon-shujuyuanguanli', '1', '1', '1', '1', 2, 0,
        '2024-02-29 14:42:08', '1', '2024-03-26 15:35:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1763094150603169793', '1763092268702851074', '字段类型映射', 'fieldtype', '/generator/fieldtype',
        '/generator/fieldtype/index', '', '1', 0, 'iconfont icon-ziduanleixingguanli', '1', '1', '1', '1', 3, 0,
        '2024-02-29 14:42:08', '1', '2024-03-26 15:35:42', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1763094323748233218', '1763092268702851074', '基类管理', 'baseclass', '/generator/baseclass',
        '/generator/baseclass/index', '', '1', 0, 'iconfont icon-jibenxinxi', '1', '1', '1', '1', 4, 0,
        '2024-02-29 14:42:08', '1', '2024-03-26 15:35:37', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942040837038081', '1747192086761091073', '任务查询', NULL, '', NULL, 'quartz:job:list', '2', 0, '', '1',
        '1', '1', '1', 1, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:45', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942129626259458', '1747192086761091073', '任务详情', NULL, '', NULL, 'quartz:job:info', '2', 0, '', '1',
        '1', '1', '1', 2, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:45', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942213998878722', '1747192086761091073', '任务新增', NULL, '', NULL, 'quartz:job:add', '2', 0, '', '1',
        '1', '1', '1', 3, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:45', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942271465037825', '1747192086761091073', '任务编辑', NULL, '', NULL, 'quartz:job:edit', '2', 0, '', '1',
        '1', '1', '1', 4, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942385961148418', '1747192086761091073', '任务删除', NULL, '', NULL, 'quartz:job:delete', '2', 0, '', '1',
        '1', '1', '1', 5, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942490130882561', '1747192086761091073', '执行任务', NULL, '', NULL, 'quartz:job:run', '2', 0, '', '1',
        '1', '1', '1', 6, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942693386854401', '1747192086761091073', '日志查询', NULL, '', NULL, 'quartz:log:list', '2', 0, '', '1',
        '1', '1', '1', 7, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942830095998977', '1747192086761091073', '日志详情', NULL, '', NULL, 'quartz:log:info', '2', 0, '', '1',
        '1', '1', '1', 8, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765942942654341122', '1747192086761091073', '日志删除', NULL, '', NULL, 'quartz:log:delete', '2', 0, '', '1',
        '1', '1', '1', 9, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1765943422306557954', '1747192086761091073', '日志清空', NULL, '', NULL, 'quartz:log:clear', '2', 0, '', '1',
        '1', '1', '1', 10, 0, '2024-01-16 17:40:29', '1', '2024-03-11 12:19:46', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1766015750667362305', '1766018358878502914', '任务日志', 'log', '/quartz/log', '/quartz/log/index', '', '1', 0,
        'iconfont icon-rizhi', '1', '1', '1', '1', 2, 0, '2024-03-08 16:19:01', '1', '2024-03-26 15:38:03', '1', '0',
        '0', '');
INSERT INTO `sys_menu`
VALUES ('1766018358878502914', '1752982543650881537', '任务调度', 'quartz', '/quartz', '', NULL, '0', 0,
        'iconfont icon-rizhi', '1', '1', '1', '1', 2, 0, '2024-01-16 17:40:29', '1', '2024-03-26 15:35:17', '1', '0',
        '0', '');
INSERT INTO `sys_menu`
VALUES ('1767396853105938433', '1747188066025779201', '通知公告', 'notice', '/system/notice', '/system/notice/index',
        NULL, '1', 0, 'iconfont icon-shezhi', '1', '1', '1', '1', 4, 0, '2024-03-12 11:50:44', NULL,
        '2024-03-12 11:55:10', '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1767396853105938434', '1767396853105938433', '查询', NULL, NULL, NULL, 'sys:notice:list', '2', 0, NULL, '1',
        '1', '1', '1', 1, 0, '2024-03-12 11:50:52', NULL, '2024-03-12 11:50:52', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1767396853105938435', '1767396853105938433', '详情', NULL, NULL, NULL, 'sys:notice:info', '2', 0, NULL, '1',
        '1', '1', '1', 2, 0, '2024-03-12 11:51:00', NULL, '2024-03-12 11:51:00', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1767396853105938436', '1767396853105938433', '新增', NULL, NULL, NULL, 'sys:notice:add', '2', 0, NULL, '1',
        '1', '1', '1', 3, 0, '2024-03-12 11:51:04', NULL, '2024-03-12 11:51:04', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1767396853105938437', '1767396853105938433', '编辑', NULL, NULL, NULL, 'sys:notice:edit', '2', 0, NULL, '1',
        '1', '1', '1', 4, 0, '2024-03-12 11:51:08', NULL, '2024-03-12 11:51:08', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1767396853105938438', '1767396853105938433', '删除', NULL, NULL, NULL, 'sys:notice:delete', '2', 0, NULL, '1',
        '1', '1', '1', 5, 0, '2024-03-12 11:51:11', NULL, '2024-03-12 11:51:11', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772456890199027713', '0', '租户管理', 'tenant', '/tenant', '', '', '0', NULL, 'iconfont icon-navicon-zhgl',
        '1', '1', '1', '1', 5, 0, '2024-03-26 15:27:07', '1', '2024-03-26 15:38:18', '1', '0', '0', '');
INSERT INTO `sys_menu`
VALUES ('1772456890199027714', '1772456890199027713', '租户', 'tenant', '/system/tenant', '/system/tenant/index', NULL,
        '1', 0, 'iconfont icon-navicon-zhgnsz', '1', '1', '1', '1', 1, 0, '2024-03-26 15:32:17', NULL,
        '2024-03-26 15:34:10', '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772456890199027715', '1772456890199027714', '查询', NULL, NULL, NULL, 'sys:tenant:list', '2', 0, NULL, '1',
        '1', '1', '1', 1, 0, '2024-03-26 15:33:34', NULL, '2024-03-26 15:33:34', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772456890199027716', '1772456890199027714', '详情', NULL, NULL, NULL, 'sys:tenant:info', '2', 0, NULL, '1',
        '1', '1', '1', 2, 0, '2024-03-26 15:33:34', NULL, '2024-03-26 15:33:34', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772456890199027717', '1772456890199027714', '新增', NULL, NULL, NULL, 'sys:tenant:add', '2', 0, NULL, '1',
        '1', '1', '1', 3, 0, '2024-03-26 15:33:34', NULL, '2024-03-26 15:33:34', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772456890199027718', '1772456890199027714', '编辑', NULL, NULL, NULL, 'sys:tenant:edit', '2', 0, NULL, '1',
        '1', '1', '1', 4, 0, '2024-03-26 15:33:34', NULL, '2024-03-26 15:33:34', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772456890199027719', '1772456890199027714', '删除', NULL, NULL, NULL, 'sys:tenant:delete', '2', 0, NULL, '1',
        '1', '1', '1', 5, 0, '2024-03-26 15:33:34', NULL, '2024-03-26 15:33:34', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772527513688625153', '1772456890199027713', '租户套餐', 'tenantPackage', '/system/tenantPackage',
        '/system/tenantPackage/index', NULL, '1', 0, 'iconfont icon-taocan', '1', '1', '1', '1', 1, 0,
        '2024-03-26 15:34:49', NULL, '2024-03-26 15:40:04', '1', '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772527513688625154', '1772527513688625153', '查询', NULL, NULL, NULL, 'sys:tenantPackage:list', '2', 0, NULL,
        '1', '1', '1', '1', 1, 0, '2024-03-26 15:34:49', NULL, '2024-03-26 15:34:49', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772527513688625155', '1772527513688625153', '详情', NULL, NULL, NULL, 'sys:tenantPackage:info', '2', 0, NULL,
        '1', '1', '1', '1', 2, 0, '2024-03-26 15:34:50', NULL, '2024-03-26 15:34:50', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772527513688625156', '1772527513688625153', '新增', NULL, NULL, NULL, 'sys:tenantPackage:add', '2', 0, NULL,
        '1', '1', '1', '1', 3, 0, '2024-03-26 15:34:50', NULL, '2024-03-26 15:34:50', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772527513688625157', '1772527513688625153', '编辑', NULL, NULL, NULL, 'sys:tenantPackage:edit', '2', 0, NULL,
        '1', '1', '1', '1', 4, 0, '2024-03-26 15:34:50', NULL, '2024-03-26 15:34:50', NULL, '0', '0', NULL);
INSERT INTO `sys_menu`
VALUES ('1772527513688625158', '1772527513688625153', '删除', NULL, NULL, NULL, 'sys:tenantPackage:delete', '2', 0,
        NULL, '1', '1', '1', '1', 5, 0, '2024-03-26 15:34:50', NULL, '2024-03-26 15:34:50', NULL, '0', '0', NULL);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`
(
    `id`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '公告ID',
    `title`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '公告标题',
    `notice_type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL COMMENT '公告类型（1通知 2公告）',
    `content`     text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         NULL COMMENT '公告内容',
    `tenant_id`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`     int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time` datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time` datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`      varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`  varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`      varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '通知公告'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice`
VALUES ('1767440007913811969', '2024-03-20 00:00:00 系统维护', '1',
        '<p><strong>维护通知：2024年3月20日 00:00:00 系统维护，请大家提前做好准备！</strong></p>', '000000', 5,
        '2024-03-12 14:38:30', '1', '2024-03-26 15:06:29', '1', '0', '0', '测试通知');
INSERT INTO `sys_notice`
VALUES ('1770012938825113601', '版本更新v1.0.0', '2',
        '<p style=\"text-align: left; line-height: 1;\">版来更新内容如下：</p><p style=\"text-indent: 2em; text-align: left;\"><strong>系统设置</strong>：<span style=\"color: rgb(255, 122, 69);\">菜单管理、数据字典、参数配置、通知公告</span></p><p style=\"text-indent: 2em; text-align: left;\"><strong>权限管理</strong>：<span style=\"color: rgb(255, 169, 64);\">用户管理、角色管理、部门管理、岗位管理</span></p><p style=\"text-indent: 2em; text-align: left;\"><strong>日志管理</strong>：<span style=\"color: rgb(115, 209, 61);\">访问日志、操作日志</span></p><p style=\"text-indent: 2em; text-align: left;\"><strong>系统监控</strong>：<span style=\"color: rgb(54, 207, 201);\">在线用户、任务调度、服务监控、缓存监控</span></p><p style=\"text-indent: 2em; text-align: left;\"><strong>代码生成</strong>：<span style=\"color: rgb(64, 169, 255);\">代码生成、数据源管理、字段类型映射、基类管理</span></p><p style=\"text-indent: 2em; text-align: left;\"><strong>常用组件</strong>：<span style=\"color: rgb(191, 191, 191);\">引导页、图标选择器、树形选择器、富文本编辑器、</span></p><p style=\"text-indent: 2em; text-align: left;\"><span style=\"color: rgb(191, 191, 191);\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 页面打印、自定义指令、外部链接、阴阳历</span></p>',
        '000000', 10, '2024-03-19 17:02:25', '1', '2024-03-26 15:06:29', '1', '0', '0', '');

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org`
(
    `id`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `pid`         varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT '0' COMMENT '上级ID',
    `org_name`    varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '机构名称',
    `sort`        int(4)                                                        NULL DEFAULT NULL COMMENT '排序',
    `tenant_id`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`     int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time` datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time` datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`      varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`  varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`      varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_pid` (`pid`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '机构管理'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_org
-- ----------------------------
INSERT INTO `sys_org`
VALUES ('1736567503762767874', '0', 'Aster科技', 0, '000000', 0, '2023-12-18 10:02:11', '1', '2024-03-27 14:57:36', '1',
        '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736569407288991746', '1736567503762767874', '北京总公司', 1, '000000', 0, '2023-12-18 10:09:45', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736569445855617025', '1736567503762767874', '青岛分公司', 2, '000000', 0, '2023-12-18 10:09:54', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736569566064369666', '1736569407288991746', '研发部', 1, '000000', 0, '2023-12-18 10:10:23', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736569595705516033', '1736569407288991746', '商务部', 2, '000000', 0, '2023-12-18 10:10:30', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736569634695766018', '1736569407288991746', '财务部', 3, '000000', 0, '2023-12-18 10:10:39', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736570161236107265', '1736569407288991746', '运维部', 4, '000000', 0, '2023-12-18 10:12:45', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736570251463974913', '1736569445855617025', '运维部', 1, '000000', 0, '2023-12-18 10:13:07', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1736570293105025025', '1736569445855617025', '研发部', 2, '000000', 0, '2023-12-18 10:13:16', '1',
        '2024-03-27 14:57:36', '1', '0', '0', '');
INSERT INTO `sys_org`
VALUES ('1772916511707758593', '0', '飞龙在天集团', 1, '042771', 0, '2024-03-27 17:20:10', '1', '2024-03-27 17:20:10',
        '1', '0', '0', NULL);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`
(
    `id`          varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `post_code`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位编码',
    `post_name`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
    `sort`        int(11)                                                       NULL DEFAULT NULL COMMENT '排序',
    `tenant_id`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`     int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time` datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time` datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`   varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`      varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`  varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`      varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '岗位'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post`
VALUES ('1749724105839837185', 'Chairman', '董事长', 1, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42',
        '1', '0', '0', NULL);
INSERT INTO `sys_post`
VALUES ('1749724627078578178', 'CEO', '首席执行官', 2, '000000', 0, '2024-01-23 17:23:54', '1', '2024-03-26 15:06:42',
        '1', '0', '1', NULL);
INSERT INTO `sys_post`
VALUES ('1749724761321472002', 'CTO', '首席技术官', 3, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42',
        '1', '0', '1', NULL);
INSERT INTO `sys_post`
VALUES ('1749724832465256450', 'CFO', '首席财务官', 4, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42',
        '1', '0', '1', NULL);
INSERT INTO `sys_post`
VALUES ('1749725124091019265', 'GM', '总经理', 2, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42', '1',
        '0', '0', NULL);
INSERT INTO `sys_post`
VALUES ('1749725602887598082', 'OD', '运营总监', 3, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42', '1',
        '0', '1', NULL);
INSERT INTO `sys_post`
VALUES ('1749725693002219521', 'MD', '市场总监', 4, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42', '1',
        '0', '1', NULL);
INSERT INTO `sys_post`
VALUES ('1749726190346010626', 'BM', '部门经理', 3, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42', '1',
        '0', '0', NULL);
INSERT INTO `sys_post`
VALUES ('1749726238597283841', 'PM', '项目经理', 4, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42', '1',
        '0', '0', NULL);
INSERT INTO `sys_post`
VALUES ('1749727093518073858', 'PL', '项目组长', 5, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42', '1',
        '0', '0', NULL);
INSERT INTO `sys_post`
VALUES ('1749728675089448962', 'FTE', '正式员工', 6, '000000', 0, '2024-01-23 17:21:50', '1', '2024-03-26 15:06:42',
        '1', '0', '0', NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `id`                  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `role_name`           varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
    `role_code`           varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色代码',
    `data_scope`          varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '数据范围  0：全部数据  1：本机构及子机构数据  2：本机构数据  3：本人数据  4：自定义数据',
    `menu_check_strictly` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '菜单树选择项是否关联显示',
    `org_id`              varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '机构ID',
    `sort`                int(4)                                                        NULL DEFAULT NULL COMMENT '排序',
    `tenant_id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`             int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time`         datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`           varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time`         datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`           varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`          varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`              varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_org_id` (`org_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '角色'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role`
VALUES ('1736570849450090497', '超级管理员', 'super_admin', '0', '0', '1736567503762767874', 1, '000000', 0,
        '2023-12-18 10:15:29', '1', '2024-03-26 15:06:51', '1', '0', '0', '');
INSERT INTO `sys_role`
VALUES ('1736571045886124034', '北京管理员', 'bj_admin', '1', '0', '1736569407288991746', 5, '000000', 0,
        '2023-12-18 10:16:16', '1', '2024-03-26 15:06:51', '1', '0', '0', '');
INSERT INTO `sys_role`
VALUES ('1736571137007378433', '青岛管理员', 'qd_admin', '1', '0', '1736569445855617025', 6, '000000', 0,
        '2023-12-18 10:16:38', '1', '2024-03-26 15:06:51', '1', '0', '0', '');
INSERT INTO `sys_role`
VALUES ('1736575482516037633', '普通角色', 'common', '4', '0', '1736567503762767874', 7, '000000', 0,
        '2023-12-18 10:33:54', '1', '2024-03-26 15:06:51', '1', '0', '0', '');
INSERT INTO `sys_role`
VALUES ('1750055847205289985', '安全管理员', 'security_admin', '0', '0', '1736567503762767874', 3, '000000', 1,
        '2023-12-18 10:33:54', '1', '2024-03-26 15:06:51', '1', '0', '0', '三元之安全管理员');
INSERT INTO `sys_role`
VALUES ('1750058351049646082', '系统管理员', 'system_admin', '0', '0', '1736567503762767874', 2, '000000', 3,
        '2023-12-18 10:33:54', '1', '2024-03-26 15:06:51', '1', '0', '0', '三元之系统管理员');
INSERT INTO `sys_role`
VALUES ('1750058629811478529', '审计管理员', 'audit_admin', '0', '0', '1736567503762767874', 4, '000000', 2,
        '2023-12-18 10:33:54', '1749738178832777218', '2024-03-26 15:06:51', '1', '0', '0', '三元之审计管理员');
INSERT INTO `sys_role`
VALUES ('1772916509186981889', '管理员', 'admin', '0', '0', '1772916511707758593', 1, '042771', 0,
        '2024-03-27 17:20:10', '1', '2024-03-28 15:55:19', '1772916698371063810', '0', '0', NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`
(
    `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'roleId',
    `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'menuId',
    `version` int(11)                                                      NULL DEFAULT NULL COMMENT '版本号',
    PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '角色菜单关系'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747187501363408897', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747188066025779201', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747189484476477441', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747189992922591234', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747190305205301250', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747190567236055041', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747190750350979073', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747191542764056578', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747192086761091073', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747192570699886594', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747192878700212226', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747192956747821057', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747193110250958850', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747193271865880578', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747193498798698498', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747194331707781121', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747194426356445185', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747196006048772098', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747196350107529218', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747196680379609090', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747196799351042050', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747196893399920642', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747197016569851905', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747197128121561090', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747198903130042369', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747199276607647746', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747199310254354433', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747199339807420417', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747199371868680194', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747199403221102593', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747199434363809793', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205571012325378', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205683738439682', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205712855298050', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205740600619009', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205771273564161', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205803280297986', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205875762065410', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747205945957937153', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206206269026306', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206428973985793', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206455591038977', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206484552708098', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206510939074561', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206775738068994', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206811968466945', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206840011583489', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206867232616450', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747206898132054017', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747207269462175746', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1747207357441896449', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1748270065033433089', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1748270372136177666', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1748270579791974402', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1748270727033016321', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1748270831601209345', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1748270936337174530', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1748271059641323522', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1750831092606521346', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1751807171982802946', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752190241453940737', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752192636825149442', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752192707331399681', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752192882514894850', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752192936898240513', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752193014677413890', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752968147641925633', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752982543650881537', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752984055445815298', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752984411114405889', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752984696369020929', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752984857195413506', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1752991362917433346', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1753310523082170369', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1754686066566455298', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1754686188859777025', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1763092268702851074', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1763093740022751234', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1763093962853539841', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1763094150603169793', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1763094323748233218', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942040837038081', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942129626259458', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942213998878722', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942271465037825', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942385961148418', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942490130882561', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942693386854401', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942830095998977', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765942942654341122', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1765943422306557954', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1766015750667362305', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1766018358878502914', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1767396853105938433', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1767396853105938434', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1767396853105938435', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1767396853105938436', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1767396853105938437', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736570849450090497', '1767396853105938438', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736575482516037633', '1747194681466597377', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736575482516037633', '1747195170950262785', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736575482516037633', '1747195260871946241', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736575482516037633', '1747195333148192770', 0);
INSERT INTO `sys_role_menu`
VALUES ('1736575482516037633', '1747195412097576962', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747187501363408897', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747192570699886594', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747192878700212226', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747192956747821057', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747193110250958850', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747193271865880578', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747198903130042369', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747199276607647746', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747199310254354433', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747199339807420417', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747199371868680194', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747199403221102593', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747199434363809793', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205571012325378', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205683738439682', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205712855298050', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205740600619009', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205771273564161', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205803280297986', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205875762065410', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747205945957937153', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206206269026306', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206428973985793', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206455591038977', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206484552708098', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206510939074561', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206775738068994', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206811968466945', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206840011583489', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206867232616450', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1747206898132054017', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750055847205289985', '1750831092606521346', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747187501363408897', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747188066025779201', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747189484476477441', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747189992922591234', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747190305205301250', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747190567236055041', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747190750350979073', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747191542764056578', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747196006048772098', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747196350107529218', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747196680379609090', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747196799351042050', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747196893399920642', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747197016569851905', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1747197128121561090', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1752190241453940737', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1752192636825149442', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1752192707331399681', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1752192882514894850', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1752192936898240513', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1752193014677413890', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1767396853105938433', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1767396853105938434', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1767396853105938435', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1767396853105938436', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1767396853105938437', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058351049646082', '1767396853105938438', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058629811478529', '1747187501363408897', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058629811478529', '1747193498798698498', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058629811478529', '1747194331707781121', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058629811478529', '1747194426356445185', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058629811478529', '1747207269462175746', 0);
INSERT INTO `sys_role_menu`
VALUES ('1750058629811478529', '1747207357441896449', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747187501363408897', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747188066025779201', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747189484476477441', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747189992922591234', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747191542764056578', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747192086761091073', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747192570699886594', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747192878700212226', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747192956747821057', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747193110250958850', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747193271865880578', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747193498798698498', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747194331707781121', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747194426356445185', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747196006048772098', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747196350107529218', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747196680379609090', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747196799351042050', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747196893399920642', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747197016569851905', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747197128121561090', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747198903130042369', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747199276607647746', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747199310254354433', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747199339807420417', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747199371868680194', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747199403221102593', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747199434363809793', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205571012325378', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205683738439682', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205712855298050', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205740600619009', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205771273564161', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205803280297986', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205875762065410', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747205945957937153', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206206269026306', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206428973985793', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206455591038977', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206484552708098', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206510939074561', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206775738068994', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206811968466945', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206840011583489', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206867232616450', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747206898132054017', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747207269462175746', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1747207357441896449', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1750831092606521346', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752190241453940737', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752192636825149442', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752192707331399681', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752192882514894850', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752192936898240513', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752193014677413890', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752982543650881537', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752984696369020929', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1752984857195413506', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1753310523082170369', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1754686066566455298', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1754686188859777025', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942040837038081', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942129626259458', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942213998878722', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942271465037825', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942385961148418', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942490130882561', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942693386854401', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942830095998977', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765942942654341122', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1765943422306557954', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1766015750667362305', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1766018358878502914', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1767396853105938433', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1767396853105938434', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1767396853105938435', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1767396853105938436', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1767396853105938437', 0);
INSERT INTO `sys_role_menu`
VALUES ('1772916509186981889', '1767396853105938438', 0);

-- ----------------------------
-- Table structure for sys_role_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_org`;
CREATE TABLE `sys_role_org`
(
    `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
    `org_id`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机构ID',
    `version` int(11)                                                      NULL DEFAULT NULL COMMENT '版本号',
    PRIMARY KEY (`role_id`, `org_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '角色机构关系'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_org
-- ----------------------------
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736567503762767874', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736569407288991746', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736569445855617025', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736569566064369666', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736569595705516033', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736569634695766018', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736570161236107265', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736570251463974913', 0);
INSERT INTO `sys_role_org`
VALUES ('1736575482516037633', '1736570293105025025', 0);
INSERT INTO `sys_role_org`
VALUES ('1772916509186981889', '1772916511707758593', 0);

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`
(
    `id`                varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'id',
    `contact_user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '联系人',
    `contact_phone`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '联系电话',
    `company_name`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '企业名称',
    `license_number`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '统一社会信用代码',
    `address`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
    `intro`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业简介',
    `tenant_domain`     varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '域名',
    `package_id`        varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户套餐编号',
    `expire_time`       datetime(0)                                                   NULL DEFAULT NULL COMMENT '过期时间',
    `account_count`     int(11)                                                       NULL DEFAULT -1 COMMENT '用户数量（-1不限制）',
    `version`           int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `create_time`       datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `update_time`       datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`         varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `status`            varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`        varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    `remark`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '租户'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant`
VALUES ('000000', '管理组', '17888888888', 'Aster科技', NULL, NULL, '多租户通用后台管理管理系统', 'admin.aster.vip',
        NULL, NULL, -1, 0, '2024-03-25 14:19:04', '1', '2024-03-25 14:19:04', '1', '0', '0', NULL);
INSERT INTO `sys_tenant`
VALUES ('042771', '龙傲天', '18888888888', '飞龙在天集团', '111111111111111', '北京市海淀区上地街道傲天科技园',
        '飞龙在天，不服就干', '', '1772906769732083714', '2025-04-01 00:00:00', 2, 0, '2024-03-27 17:20:10', '1',
        '2024-03-28 15:04:25', '1', '0', '0', '');

-- ----------------------------
-- Table structure for sys_tenant_package
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_package`;
CREATE TABLE `sys_tenant_package`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NOT NULL COMMENT '租户套餐id',
    `package_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '套餐名称',
    `menu_ids`     varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联菜单id',
    `version`      int(11)                                                        NULL DEFAULT NULL COMMENT '版本号',
    `create_time`  datetime(0)                                                    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '创建者',
    `update_time`  datetime(0)                                                    NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `update_by`    varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '修改者',
    `status`       varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0' COMMENT '删除状态',
    `remark`       varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '租户套餐'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant_package
-- ----------------------------
INSERT INTO `sys_tenant_package`
VALUES ('1772906769732083714', '基本套餐',
        '1747188066025779201,1747189484476477441,1752982543650881537,1747187501363408897,1747189992922591234,1747196006048772098,1747191542764056578,1747196350107529218,1747196680379609090,1747196799351042050,1747196893399920642,1747197016569851905,1747197128121561090,1752190241453940737,1752192636825149442,1752192707331399681,1752192882514894850,1752192936898240513,1752193014677413890,1767396853105938433,1767396853105938434,1767396853105938435,1767396853105938436,1767396853105938437,1767396853105938438,1747192570699886594,1747192878700212226,1747198903130042369,1747199276607647746,1747199310254354433,1747199339807420417,1747199371868680194,1747199403221102593,1747199434363809793,1750831092606521346,1747192956747821057,1747205571012325378,1747205683738439682,1747205712855298050,1747205740600619009,1747205771273564161,1747205803280297986,1747205875762065410,1747205945957937153,1747193110250958850,1747206206269026306,1747206428973985793,1747206455591038977,1747206484552708098,1747206510939074561,1747193271865880578,1747206775738068994,1747206811968466945,1747206840011583489,1747206867232616450,1747206898132054017,1747193498798698498,1747194331707781121,1747207269462175746,1747194426356445185,1747207357441896449,1766018358878502914,1747192086761091073,1765942040837038081,1765942129626259458,1765942213998878722,1765942271465037825,1765942385961148418,1765942490130882561,1765942693386854401,1765942830095998977,1765942942654341122,1765943422306557954,1766015750667362305,1752984696369020929,1753310523082170369,1752984857195413506,1754686066566455298,1754686188859777025',
        0, '2024-03-27 16:41:28', '1', '2024-03-28 15:54:34', '1', '0', '0', '');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `id`                       varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'id',
    `username`                 varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '用户名',
    `password`                 varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
    `org_id`                   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '机构ID',
    `nick_name`                varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '昵称',
    `avatar`                   varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
    `gender`                   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '性别   0：男   1：女   2：未知',
    `email`                    varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
    `mobile`                   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '手机号',
    `real_name`                varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '姓名',
    `id_number`                varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '身份证号',
    `super_admin`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '超级管理员   0：是   1：否',
    `tenant_id`                varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '租户ID',
    `version`                  int(11)                                                       NULL DEFAULT NULL COMMENT '版本号',
    `login_number`             bigint(11)                                                    NULL DEFAULT NULL COMMENT '登录次数',
    `last_login_time`          datetime(0)                                                   NULL DEFAULT NULL COMMENT '最后登录时间',
    `mobile_verification_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '手机验证码',
    `email_verification_code`  varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '邮箱验证码',
    `salt`                     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '密钥',
    `birthday`                 varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '生日',
    `area`                     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地区',
    `signature`                varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '签名',
    `create_by`                varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建者',
    `create_time`              datetime(0)                                                   NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`                varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '修改者',
    `update_time`              datetime(0)                                                   NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
    `status`                   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '启用状态',
    `is_deleted`               varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT '0' COMMENT '删除状态',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '用户'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user`
VALUES ('1', 'admin', '{bcrypt}$2a$10$Q5C1XmubsO0CpXCg4QvHeuhaKoLPI.5bD57q7XJkIu3UxWroW5llu', '1736567503762767874',
        'admin', 'http://minio.aster.vip:9000/aster-minio/20240305/avatar_36718.png', '0', NULL, '15311111111', NULL,
        NULL, '0', '000000', 0, 1, '2024-01-18 10:40:35', NULL, NULL, NULL, '1992-10-29', NULL, '是谁偷走，偷走我的心',
        '1', '2024-01-18 10:40:51', '1', '2024-10-31 17:16:23', '0', '0');
INSERT INTO `sys_user`
VALUES ('1749738178832777218', 'aster', '{bcrypt}$2a$10$kBidN3Du0PMqgrZsRBPv/OgsdFT2O9c4RKUlnNiCoeYo5OQovE9t6',
        '1736569566064369666', 'aster', 'http://minio.aster.vip:9000/aster-minio/20240320/大熊猫_65561.jpg', '1', NULL,
        '15333333333', '', '', '1', '000000', 0, NULL, NULL, NULL, NULL, NULL, '', NULL, '风吹裤裆pp凉', '1',
        '2024-01-23 18:17:45', '1', '2024-10-31 17:16:27', '0', '0');
INSERT INTO `sys_user`
VALUES ('1772916698371063810', 'long', '{bcrypt}$2a$10$zXe3.H8vxp1MzR/bwcJBkOsv153W3lI7ASTaYEZIDGQgqglh8n37u',
        '1772916511707758593', '龙傲天', 'http://minio.aster.vip:9000/aster-minio/20240305/avatar_36718.png', '0', NULL,
        '188****8888', NULL, NULL, '1', '042771', 1, NULL, NULL, NULL, NULL, NULL, '2024-03-25', NULL,
        '飞龙在天，不服就干', '1', '2024-03-27 17:20:55', '1772916698371063810', '2024-10-31 17:16:39', '0', '0');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`
(
    `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
    `post_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位ID',
    `version` int(11)                                                      NULL DEFAULT NULL COMMENT '版本号',
    PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '用户岗位关系'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post`
VALUES ('1', '1749726238597283841', 0);
INSERT INTO `sys_user_post`
VALUES ('1749738178832777218', '1749728675089448962', 0);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
    `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
    `version` int(11)                                                      NULL DEFAULT NULL COMMENT '版本号',
    PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '用户角色关系'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role`
VALUES ('1', '1736570849450090497', 0);
INSERT INTO `sys_user_role`
VALUES ('1749738178832777218', '1750055847205289985', 0);
INSERT INTO `sys_user_role`
VALUES ('1772916698371063810', '1772916509186981889', 0);
INSERT INTO `sys_user_role`
VALUES ('1772928696412561410', '1772916509186981889', 0);
INSERT INTO `sys_user_role`
VALUES ('1773183468537401345', '1772916509186981889', 0);

SET FOREIGN_KEY_CHECKS = 1;
