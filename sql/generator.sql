/*
 Navicat Premium Data Transfer

 Source Server         : 腾讯云主
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 82.157.149.166:3306
 Source Schema         : aster_boot

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 08/03/2024 17:32:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_base_class
-- ----------------------------
DROP TABLE IF EXISTS `gen_base_class`;
CREATE TABLE `gen_base_class`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '基类编码',
  `fields` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '基类字段，多个用英文逗号分隔',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基类管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_base_class
-- ----------------------------
INSERT INTO `gen_base_class` VALUES ('1', 'BaseEntity', 'create_time,create_by,update_time,update_by,version,is_deleted', '使用该基类，则需要表里有这些字段', '2024-02-22 11:25:33');

-- ----------------------------
-- Table structure for gen_datasource
-- ----------------------------
DROP TABLE IF EXISTS `gen_datasource`;
CREATE TABLE `gen_datasource`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `db_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库类型',
  `conn_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '连接名',
  `conn_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据源管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for gen_field_type
-- ----------------------------
DROP TABLE IF EXISTS `gen_field_type`;
CREATE TABLE `gen_field_type`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `column_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `attr_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性包名',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `column_type`(`column_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字段类型管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_field_type
-- ----------------------------
INSERT INTO `gen_field_type` VALUES ('1', 'datetime', 'LocalDateTime', 'java.time.LocalDateTime', '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('10', 'double', 'Double', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('11', 'decimal', 'BigDecimal', 'java.math.BigDecimal', '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('12', 'bit', 'Boolean', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('13', 'char', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('14', 'varchar', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('15', 'tinytext', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('16', 'text', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('17', 'mediumtext', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('18', 'longtext', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('19', 'timestamp', 'LocalDateTime', 'java.time.LocalDateTime', '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('2', 'date', 'LocalDateTime', 'java.time.LocalDateTime', '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('20', 'NUMBER', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('21', 'BINARY_INTEGER', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('22', 'BINARY_FLOAT', 'Double', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('23', 'BINARY_DOUBLE', 'Double', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('24', 'VARCHAR2', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('25', 'NVARCHAR', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('26', 'NVARCHAR2', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('27', 'CLOB', 'String', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('28', 'int8', 'Long', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('29', 'int4', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('3', 'tinyint', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('30', 'int2', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('31', 'numeric', 'BigDecimal', 'java.math.BigDecimal', '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('4', 'smallint', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('5', 'mediumint', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('6', 'int', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('7', 'integer', 'Integer', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('8', 'bigint', 'Long', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_field_type` VALUES ('9', 'float', 'Double', NULL, '2024-02-22 14:58:22');

-- ----------------------------
-- Table structure for gen_project_modify
-- ----------------------------
DROP TABLE IF EXISTS `gen_project_modify`;
CREATE TABLE `gen_project_modify`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `project_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名',
  `project_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目标识',
  `project_package` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目包名',
  `project_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目路径',
  `modify_project_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更项目名',
  `modify_project_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更标识',
  `modify_project_package` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更包名',
  `exclusions` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '排除文件',
  `modify_suffix` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更文件',
  `modify_tmp_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更临时路径',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '项目名变更' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_project_modify
-- ----------------------------
INSERT INTO `gen_project_modify` VALUES ('1', 'maku-boot', 'maku', 'net.maku', 'D:/makunet/maku-boot', 'baba-boot', 'baba', 'com.baba', '.git,.idea,target,logs', 'java,xml,yml,txt', NULL, '2024-02-22 14:58:22');
INSERT INTO `gen_project_modify` VALUES ('2', 'maku-cloud', 'maku', 'net.maku', 'D:/makunet/maku-cloud', 'baba-cloud', 'baba', 'com.baba', '.git,.idea,target,logs', 'java,xml,yml,txt', NULL, '2024-02-22 14:58:22');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表名',
  `class_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类名',
  `table_comment` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `author` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者',
  `email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `package_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目包名',
  `version` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目版本号',
  `generator_type` tinyint(4) NULL DEFAULT NULL COMMENT '生成方式  0：zip压缩包   1：自定义目录',
  `backend_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后端生成路径',
  `frontend_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '前端生成路径',
  `module_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名',
  `module_simple` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名简写',
  `function_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '功能名',
  `form_layout` tinyint(4) NULL DEFAULT NULL COMMENT '表单布局  1：一列   2：两列',
  `datasource_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据源ID',
  `base_class_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '基类ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `table_name`(`table_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('1763104911811440641', 'sys_post', 'SysPost', '岗位', 'Aster', 'lipian1004@163.com', 'vip.aster', '1.0.0', 1, 'D:\\generator\\aster-boot', 'D:\\generator\\aster-admin', 'system', 'sys', 'post', 2, NULL, '', '2024-02-29 15:32:23');
INSERT INTO `gen_table` VALUES ('1763384682751475714', 'sys_role', 'SysRole', '角色', 'Aster', 'lipian1004@163.com', 'vip.aster', '1.0.0', 0, 'D:\\generator\\aster-boot', 'D:\\generator\\aster-admin', 'system', 'sys', 'role', 1, NULL, '1', '2024-03-01 10:04:05');
INSERT INTO `gen_table` VALUES ('1764543599514873857', 'sys_config', 'SysConfig', '系统配置', 'Aster', 'lipian1004@163.com', 'vip.aster', '1.0.0', 1, 'D:\\generator\\aster-boot', 'D:\\generator\\aster-admin', 'sys', 'sys', 'config', 1, '1764539297773740033', '', '2024-03-04 14:49:12');
INSERT INTO `gen_table` VALUES ('1765199949147992065', 'schedule_job', 'ScheduleJob', '定时任务调度', 'Aster', 'lipian1004@163.com', 'vip.aster', '1.0.0', 0, 'D:\\generator\\aster-boot', 'D:\\generator\\aster-admin', 'quartz', 'quartz', 'job', 1, NULL, NULL, '2024-03-06 10:17:18');
INSERT INTO `gen_table` VALUES ('1765199950553083905', 'schedule_job_log', 'ScheduleJobLog', '定时任务调度日志', 'Aster', 'lipian1004@163.com', 'vip.aster', '1.0.0', 0, 'D:\\generator\\aster-boot', 'D:\\generator\\aster-admin', 'quartz', 'quartz', 'log', 1, NULL, NULL, '2024-03-06 10:17:19');

-- ----------------------------
-- Table structure for gen_table_field
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_field`;
CREATE TABLE `gen_table_field`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id',
  `table_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表ID',
  `field_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段名称',
  `field_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `field_comment` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段说明',
  `attr_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性名',
  `attr_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性包名',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `auto_fill` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '自动填充  DEFAULT、INSERT、UPDATE、INSERT_UPDATE',
  `primary_pk` tinyint(4) NULL DEFAULT NULL COMMENT '主键 0：否  1：是',
  `base_field` tinyint(4) NULL DEFAULT NULL COMMENT '基类字段 0：否  1：是',
  `form_item` tinyint(4) NULL DEFAULT NULL COMMENT '表单项 0：否  1：是',
  `form_required` tinyint(4) NULL DEFAULT NULL COMMENT '表单必填 0：否  1：是',
  `form_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单类型',
  `form_dict` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单字典类型',
  `form_validator` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单效验',
  `grid_item` tinyint(4) NULL DEFAULT NULL COMMENT '列表项 0：否  1：是',
  `grid_sort` tinyint(4) NULL DEFAULT NULL COMMENT '列表排序 0：否  1：是',
  `query_item` tinyint(4) NULL DEFAULT NULL COMMENT '查询项 0：否  1：是',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询方式',
  `query_form_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询表单类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_field
-- ----------------------------
INSERT INTO `gen_table_field` VALUES ('1763104912335728642', '1763104911811440641', 'id', 'varchar', 'ID', 'id', 'String', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104912532860929', '1763104911811440641', 'post_code', 'varchar', '岗位编码', 'postCode', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104912725798913', '1763104911811440641', 'post_name', 'varchar', '岗位名称', 'postName', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104912990040066', '1763104911811440641', 'sort', 'int', '排序', 'sort', 'Integer', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104913321390081', '1763104911811440641', 'tenant_id', 'varchar', '租户ID', 'tenantId', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104913514328065', '1763104911811440641', 'version', 'int', '版本号', 'version', 'Integer', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104913841483778', '1763104911811440641', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104914038616065', '1763104911811440641', 'create_by', 'varchar', '创建者', 'createBy', 'String', NULL, 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104914235748354', '1763104911811440641', 'update_time', 'datetime', '更新时间', 'updateTime', 'LocalDateTime', 'java.time.LocalDateTime', 8, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104914428686338', '1763104911811440641', 'update_by', 'varchar', '修改者', 'updateBy', 'String', NULL, 9, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104914822950914', '1763104911811440641', 'status', 'varchar', '启用状态', 'status', 'String', NULL, 10, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104915141718018', '1763104911811440641', 'is_deleted', 'varchar', '删除状态', 'isDeleted', 'String', NULL, 11, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763104915334656001', '1763104911811440641', 'remark', 'varchar', '备注信息', 'remark', 'String', NULL, 12, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384682944413698', '1763384682751475714', 'id', 'varchar', 'ID', 'id', 'String', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384683074437121', '1763384682751475714', 'role_name', 'varchar', '角色名称', 'roleName', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384683137351682', '1763384682751475714', 'role_code', 'varchar', '角色代码', 'roleCode', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384683267375106', '1763384682751475714', 'data_scope', 'varchar', '数据范围  0：全部数据  1：本机构及子机构数据  2：本机构数据  3：本人数据  4：自定义数据', 'dataScope', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384683657445377', '1763384682751475714', 'org_id', 'varchar', '机构ID', 'orgId', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384683858771969', '1763384682751475714', 'sort', 'int', '排序', 'sort', 'Integer', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384683921686529', '1763384682751475714', 'tenant_id', 'varchar', '租户ID', 'tenantId', 'String', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684051709954', '1763384682751475714', 'version', 'int', '版本号', 'version', 'Integer', NULL, 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684123013121', '1763384682751475714', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 8, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684248842241', '1763384682751475714', 'create_by', 'varchar', '创建者', 'createBy', 'String', NULL, 9, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684383059970', '1763384682751475714', 'update_time', 'datetime', '更新时间', 'updateTime', 'LocalDateTime', 'java.time.LocalDateTime', 10, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684513083393', '1763384682751475714', 'update_by', 'varchar', '修改者', 'updateBy', 'String', NULL, 11, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684647301121', '1763384682751475714', 'status', 'varchar', '启用状态', 'status', 'String', NULL, 12, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684701827074', '1763384682751475714', 'is_deleted', 'varchar', '删除状态', 'isDeleted', 'String', NULL, 13, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1763384684831850497', '1763384682751475714', 'remark', 'varchar', '备注信息', 'remark', 'String', NULL, 14, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543599774920706', '1764543599514873857', 'id', 'varchar', '主键', 'id', 'String', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543599904944130', '1764543599514873857', 'config_name', 'varchar', '参数名称', 'configName', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543599967858690', '1764543599514873857', 'config_key', 'varchar', '参数键名', 'configKey', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600097882114', '1764543599514873857', 'config_value', 'varchar', '参数键值', 'configValue', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600160796674', '1764543599514873857', 'config_type', 'varchar', '系统内置,\'0\'-\'是\',\'1\'-‘否’', 'configType', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600290820098', '1764543599514873857', 'tenant_id', 'varchar', '租户ID', 'tenantId', 'String', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600353734658', '1764543599514873857', 'version', 'int', '版本号', 'version', 'Integer', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600483758082', '1764543599514873857', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600550866945', '1764543599514873857', 'create_by', 'varchar', '创建者', 'createBy', 'String', NULL, 8, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600680890369', '1764543599514873857', 'update_time', 'datetime', '更新时间', 'updateTime', 'LocalDateTime', 'java.time.LocalDateTime', 9, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600743804930', '1764543599514873857', 'update_by', 'varchar', '修改者', 'updateBy', 'String', NULL, 10, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543600878022658', '1764543599514873857', 'status', 'varchar', '启用状态', 'status', 'String', NULL, 11, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543601008046082', '1764543599514873857', 'is_deleted', 'varchar', '删除状态', 'isDeleted', 'String', NULL, 12, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1764543601133875202', '1764543599514873857', 'remark', 'varchar', '备注信息', 'remark', 'String', NULL, 13, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196668199825410', '1765196668065607681', 'job_id', 'bigint', '任务ID', 'jobId', 'Long', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196668266934274', '1765196668065607681', 'job_name', 'varchar', '任务名称', 'jobName', 'String', NULL, 1, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196668359208961', '1765196668065607681', 'job_group', 'varchar', '任务组名', 'jobGroup', 'String', NULL, 2, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196668459872257', '1765196668065607681', 'invoke_target', 'varchar', '调用目标字符串', 'invokeTarget', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196668539564033', '1765196668065607681', 'cron_expression', 'varchar', 'cron执行表达式', 'cronExpression', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196668619255809', '1765196668065607681', 'misfire_policy', 'varchar', '计划执行错误策略（1立即执行 2执行一次 3放弃执行）', 'misfirePolicy', 'String', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196668703141889', '1765196668065607681', 'concurrent', 'char', '是否并发执行（0允许 1禁止）', 'concurrent', 'String', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669000937473', '1765196668065607681', 'status', 'char', '状态（0正常 1暂停）', 'status', 'String', NULL, 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669097406466', '1765196668065607681', 'create_by', 'varchar', '创建者', 'createBy', 'String', NULL, 8, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669219041282', '1765196668065607681', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 9, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669281955842', '1765196668065607681', 'update_by', 'varchar', '更新者', 'updateBy', 'String', NULL, 10, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669357453314', '1765196668065607681', 'update_time', 'datetime', '更新时间', 'updateTime', 'LocalDateTime', 'java.time.LocalDateTime', 11, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669407784962', '1765196668065607681', 'remark', 'varchar', '备注信息', 'remark', 'String', NULL, 12, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669596528641', '1765196669516836866', 'job_log_id', 'bigint', '任务日志ID', 'jobLogId', 'Long', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669672026114', '1765196669516836866', 'job_name', 'varchar', '任务名称', 'jobName', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669743329282', '1765196669516836866', 'job_group', 'varchar', '任务组名', 'jobGroup', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669864964097', '1765196669516836866', 'invoke_target', 'varchar', '调用目标字符串', 'invokeTarget', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669940461569', '1765196669516836866', 'job_message', 'varchar', '日志信息', 'jobMessage', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196669994987521', '1765196669516836866', 'status', 'char', '执行状态（0正常 1失败）', 'status', 'String', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196670045319170', '1765196669516836866', 'exception_info', 'varchar', '异常信息', 'exceptionInfo', 'String', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765196670150176770', '1765196669516836866', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543449743362', '1765199543370051585', 'id', 'varchar', '任务ID', 'id', 'String', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543500075009', '1765199543370051585', 'job_name', 'varchar', '任务名称', 'jobName', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543562989570', '1765199543370051585', 'job_group', 'varchar', '任务组名', 'jobGroup', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543638487042', '1765199543370051585', 'invoke_target', 'varchar', '调用目标字符串', 'invokeTarget', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543747538945', '1765199543370051585', 'cron_expression', 'varchar', 'cron执行表达式', 'cronExpression', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543810453506', '1765199543370051585', 'misfire_policy', 'varchar', '计划执行错误策略（1立即执行 2执行一次 3放弃执行）', 'misfirePolicy', 'String', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543894339586', '1765199543370051585', 'concurrent', 'char', '是否并发执行（0允许 1禁止）', 'concurrent', 'String', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199543957254145', '1765199543370051585', 'version', 'int', '版本号', 'version', 'Integer', NULL, 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544032751617', '1765199543370051585', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 8, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544087277570', '1765199543370051585', 'create_by', 'varchar', '创建者', 'createBy', 'String', NULL, 9, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544154386433', '1765199543370051585', 'update_time', 'datetime', '更新时间', 'updateTime', 'LocalDateTime', 'java.time.LocalDateTime', 10, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544225689602', '1765199543370051585', 'update_by', 'varchar', '修改者', 'updateBy', 'String', NULL, 11, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544301187073', '1765199543370051585', 'status', 'varchar', '启用状态', 'status', 'String', NULL, 12, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544351518721', '1765199543370051585', 'is_deleted', 'varchar', '删除状态', 'isDeleted', 'String', NULL, 13, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544422821889', '1765199543370051585', 'remark', 'varchar', '备注信息', 'remark', 'String', NULL, 14, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544624148482', '1765199544536068098', 'id', 'varchar', '任务日志ID', 'id', 'String', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544791920641', '1765199544536068098', 'job_name', 'varchar', '任务名称', 'jobName', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544867418113', '1765199544536068098', 'job_group', 'varchar', '任务组名', 'jobGroup', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544934526977', '1765199544536068098', 'invoke_target', 'varchar', '调用目标字符串', 'invokeTarget', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199544997441538', '1765199544536068098', 'job_message', 'varchar', '日志信息', 'jobMessage', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199545098104833', '1765199544536068098', 'status', 'varchar', '执行状态（0正常 1失败）', 'status', 'String', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199545148436482', '1765199544536068098', 'exception_info', 'varchar', '异常信息', 'exceptionInfo', 'String', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199545215545345', '1765199544536068098', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949278015489', '1765199949147992065', 'id', 'varchar', '任务ID', 'id', 'String', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949357707265', '1765199949147992065', 'job_name', 'varchar', '任务名称', 'jobName', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949416427522', '1765199949147992065', 'job_group', 'varchar', '任务组名', 'jobGroup', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949475147778', '1765199949147992065', 'invoke_target', 'varchar', '调用目标字符串', 'invokeTarget', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949533868033', '1765199949147992065', 'cron_expression', 'varchar', 'cron执行表达式', 'cronExpression', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949596782594', '1765199949147992065', 'misfire_policy', 'varchar', '计划执行错误策略（1立即执行 2执行一次 3放弃执行）', 'misfirePolicy', 'String', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949684862978', '1765199949147992065', 'concurrent', 'char', '是否并发执行（0允许 1禁止）', 'concurrent', 'String', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949777137666', '1765199949147992065', 'version', 'int', '版本号', 'version', 'Integer', NULL, 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199949890383873', '1765199949147992065', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 8, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950041378817', '1765199949147992065', 'create_by', 'varchar', '创建者', 'createBy', 'String', NULL, 9, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950125264898', '1765199949147992065', 'update_time', 'datetime', '更新时间', 'updateTime', 'LocalDateTime', 'java.time.LocalDateTime', 10, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950263676929', '1765199949147992065', 'update_by', 'varchar', '修改者', 'updateBy', 'String', NULL, 11, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950322397185', '1765199949147992065', 'status', 'varchar', '启用状态', 'status', 'String', NULL, 12, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950389506050', '1765199949147992065', 'is_deleted', 'varchar', '删除状态', 'isDeleted', 'String', NULL, 13, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950435643393', '1765199949147992065', 'remark', 'varchar', '备注信息', 'remark', 'String', NULL, 14, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950649552897', '1765199950553083905', 'id', 'varchar', '任务日志ID', 'id', 'String', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950695690241', '1765199950553083905', 'job_name', 'varchar', '任务名称', 'jobName', 'String', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950741827586', '1765199950553083905', 'job_group', 'varchar', '任务组名', 'jobGroup', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950800547841', '1765199950553083905', 'invoke_target', 'varchar', '调用目标字符串', 'invokeTarget', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950846685186', '1765199950553083905', 'job_message', 'varchar', '日志信息', 'jobMessage', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199950943154178', '1765199950553083905', 'status', 'varchar', '执行状态（0正常 1失败）', 'status', 'String', NULL, 5, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199951001874434', '1765199950553083905', 'exception_info', 'varchar', '异常信息', 'exceptionInfo', 'String', NULL, 6, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');
INSERT INTO `gen_table_field` VALUES ('1765199951077371905', '1765199950553083905', 'create_time', 'datetime', '创建时间', 'createTime', 'LocalDateTime', 'java.time.LocalDateTime', 7, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text');

SET FOREIGN_KEY_CHECKS = 1;
