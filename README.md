# aster-boot

### 项目介绍

- aster-boot 是采用SpringBoot3.x、SpringSecurity、Mybatis-Plus、vue3&ElementPlus等框架，开发的一套前后端分离权限管理系统。
- 前端工程：https://gitee.com/lp1791803611/aster-admin
- 在线体验：http://admin.aster.vip/#/  admin/123456

### 组织结构

``` lua
aster-boot
├── aster-common -- 工具类及通用代码
├── aster-framework -- 框架模块
├── aster-module-generator -- 代码生成模块
├── aster-module-monitor -- 监控模块
├── aster-module-quartz -- 任务调度模块
├── aster-module-sensitive -- 数据脱敏模块
├── aster-module-tenant -- 租户模块
├── aster-module-websocket -- 即时通讯模块
├── aster-module-license -- 证书生成模块
├── aster-system -- 后台管理模块
├── aster-api -- 对外接口模块
└── aster-admin -- 后台管理启动器
```

### 技术选型

| 选型                 | 介绍    |      |
|--------------------|-------|------|
| SpringBoot         | 主框架   | 3.2.1 |
| SpringSecurity     | 认证授权  |      |
| Mybatis-plus       | ORM框架 | 3.5.5 |
| Dynamic-datasource | 多数据源  | 4.2.x |
| Redis              | 缓存    | 两者选其一 |
| Ehcache3.x         | 缓存    |      |
| Quartz             | 任务调度  |      |
| MinIO              | 对象存储  | 8.5.x |
| Hutool             | 工具类库  | 5.7  |
| EasyExcel          | 导入导出  | 3.3.x |
| Jasypt             | 加解密   | 3.x  |
| Snakeyaml          | YML解析 |      |
| Knife4j            | API文档 | 4.3.x |
| Easy-Captcha       | 验证码生成 | 1.6.x |
| WebSocket          | 实时通信  |      |
| Logback            | 日志    |    |
| Truelicense        | 证书许可  | 1.33 |

### 基础功能

| 功能   | 说明                          |   |
|------|-----------------------------|---|
| 用户管理 | 用户信息，角色、部门、岗位配置，密码重置等       | √ |
| 角色管理 | 角色信息，菜单权限、数据权限配置            | √ |
| 部门管理 | 组织机构树管理                     | √ |
| 岗位管理 | 用户职位                        | √ |
| 菜单管理 | 系统菜单、按钮权限等                  | √ |
| 字典管理 | 字典类型、字典数据管理                 | √ |
| 参数配置 | 系统参数配置                      | √ |
| 访问日志 | 登录日志、登录异常信息查询               | √ |
| 操作日志 | 用户操作记录、操作异常记录               | √ |
| 在线用户 | 在线用户监控，强退用户                 | √ |
| 服务监控 | 监控系统CPU、内存、JVM、磁盘等信息        | √ |
| 缓存监控 | 缓存查询，删除、清空等操作               | √ |
| 通知公告 | 通知公告信息维护                    | √ |
| 系统接口 | API接口文档                     | √ |
| 多数据源 | dynamic-datasource          | √ |
| 乐观锁  | 基于mybatis-plus              | √ |
| 代码生成 | 前后端代码一键生成                   | √ |
| 定时任务 | 基于quartz的任务调度               | √ |
| 国际化  | i18n国际化                     | √ |
| 实时通信 | websocket                   | √ |
| 数据权限 | 全部数据、本机构及子机构数据、<br/>本机构数据、本人数据、自定义数据 | √ |
| 租户管理 | 租户套餐、租户用户数限制等               | √ |
| 证书许可 | 给付费用户提供的访问许可证明              | √ |

### 开发环境

| 工具            | 版本号   |
|---------------|-------| 
| JDK           | 17+   | 
| MySQL         | 5.7+  | 
| Redis或ehcache |       | 
| Nginx         | 1.22+ | 

### 项目启动

因配置文件中对密码做了加密，所以启动时需要添加密钥：--jasypt.password=xxxx
<br />

- idea配置

    ![输入图片说明](images/img_09.png)
- 启动jar包

```lua
  java -jar aster-boot.jar --jasypt.password=xxxx
``` 

### 演示图

![img-1](/images/img_01.png)

![img-2](/images/img_02.png)

![img-3](/images/img_03.png)

![img-4](/images/img_04.png)

![img-5](/images/img_05.png)

![img-6](/images/img_06.png)

![img-7](/images/img_07.png)

![img-8](/images/img_08.png)